
package bases;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

/**
 *
 * @author Legion
 */
public class GeneradorClave {
   public String Generarclaves(){
       String password="";
         String[] symbols = {"C", "O", "L", "E", "G", "I", "O", "m", "o","ll","e","t","u","r","o","-","2","0","2","1"};
        int length = 21;
        Random random;
        try {
            random = SecureRandom.getInstanceStrong();
            StringBuilder sb = new StringBuilder(length);
           
            for (int i = 0; i <length; i++) {
                int indexRandom = random.nextInt ( symbols.length );
                 sb.append( symbols[indexRandom] );  
            }
             password = sb.toString();
          } catch (NoSuchAlgorithmException e){
              System.out.println(e.toString());
          } 
       return password;
   
   } 
}
