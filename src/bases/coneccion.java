
package bases;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class coneccion {
//forma local 
 public String ip="192.168.0.128"; //es la IP donde está la base de datos MySQL, 
public String puerto="3306";// es el puerto por donde escucha el servidor de base de datos.
public String database="colegiomolleturo1";//es el nombre de la base de datos a la cual nos queremos conectar.
public String usuario="root"; //es el usuario de la base de datos 
public String password="";//es la clave con que el usuario puede ingresar a la base de datos.
public String JDBC="jdbc:mysql:";
public String Url;
    
Connection conectar;


public Connection getConnection() {
        Url=""+JDBC+"//"+ip+":"+puerto+"/"+database+"";
        try { 
             Class.forName("com.mysql.jdbc.Driver");
            conectar = DriverManager.getConnection(Url,usuario,password);
	         //  System.out.println("correcta coneccion");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,e.getMessage());        
            
       }
        return conectar;
    }

 public void desconectar(){
        try {
            conectar.close();
        } catch (SQLException ex) {
            Logger.getLogger(coneccion.class.getName()).log(Level.SEVERE, null, ex);
        }
}
}
