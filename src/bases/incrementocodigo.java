
package bases;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Mesias
 */
public class incrementocodigo {
coneccion db;  
PreparedStatement ps=null;
ResultSet rs= null;
     
 public incrementocodigo(){
 this.db=new coneccion();
}      
    
    public int auto_incremento(String sql ){//autoincrementode de secuencial SGBD
 int id=1;
     try {
         ps=db.getConnection().prepareStatement(sql);
         rs=ps.executeQuery();
         while(rs.next()){
         id=rs.getInt(1)+1;
         }
     } catch (SQLException ex) {
       System.out.println("idmaximo"+ex.getMessage());
     }finally{
         try {
             ps.close();
             rs.close();
           db.desconectar();
         } catch (Exception e) {
         }   
     }
     return id;    
 } 
   public String devolverundato(String sql ){// debuelve una consulta de  forma  rapida
 String maximo=null;
     try {
         ps=db.getConnection().prepareStatement(sql);
         rs=ps.executeQuery();
         while(rs.next()){
         maximo=rs.getString(1);
         }
         db.desconectar();
     } catch (SQLException ex) {
       System.out.println("idmaximo"+ex.getMessage());
     }finally{
         try {
             ps.close();
             rs.close();
            db.desconectar();
         } catch (Exception e) {
         }   
     }
     return maximo;    
 }    
}
