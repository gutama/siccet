package bases;

import java.awt.event.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.*;
import java.security.MessageDigest;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.JOptionPane;
import org.apache.commons.codec.binary.Base64;
public class validaciones {

    public boolean validacioncedula(String ce) {
        if (ce.length() < 10) {
            JOptionPane.showMessageDialog(null, "cedula incompleta");
        }
        int par, impar, sum, cont = 0;
        int dig1, dig2, dig3, dig4, dig5, dig6, dig7, dig8, dig9, dig10;
        dig1 = Integer.parseInt(ce.substring(0, 1));
        dig2 = Integer.parseInt(ce.substring(1, 2));
        dig3 = Integer.parseInt(ce.substring(2, 3));
        dig4 = Integer.parseInt(ce.substring(3, 4));
        dig5 = Integer.parseInt(ce.substring(4, 5));
        dig6 = Integer.parseInt(ce.substring(5, 6));
        dig7 = Integer.parseInt(ce.substring(6, 7));
        dig8 = Integer.parseInt(ce.substring(7, 8));
        dig9 = Integer.parseInt(ce.substring(8, 9));
        dig10 = Integer.parseInt(ce.substring(9, 10));
        dig1 *= 2;
        if (dig1 >= 10) {
            dig1 -= 9;
        }
        dig3 *= 2;
        if (dig3 >= 10) {
            dig3 -= 9;
        }
        dig5 *= 2;
        if (dig5 >= 10) {
            dig5 -= 9;
        }
        dig7 *= 2;
        if (dig7 >= 10) {
            dig7 -= 9;
        }
        dig9 *= 2;
        if (dig9 >= 10) {
            dig9 -= 9;
        }

        par = dig2 + dig4 + dig6 + dig8;
        impar = dig1 + dig3 + dig5 + dig7 + dig9;
        sum = par + impar;
        int t = 10 - (sum % 10);
        if (t == 10) {
            t = 0;
        }
        if (dig10 == t) {//condicion en la cual me afirma  que la cedula es correcta

            return true;
        } else {
            JOptionPane.showMessageDialog(null, "Cedula incorrecta", "!Advertencia!", JOptionPane.WARNING_MESSAGE);
        }
        return false;
    }

    public void limitarcaracteres(JTextField campo, int cantidad) {
        campo.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                int tamaño = campo.getText().length();
                if (tamaño >= cantidad) {
                    e.consume();
                }
            }
        });
    }

    public void validarnumerosdecimales(JTextField campo) {
        campo.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (!Character.isDigit(e.getKeyChar()) && e.getKeyChar() != '.') {
                    e.consume();
                }
                if (e.getKeyChar() == '.' && campo.getText().contains(".")) {
                    e.consume();
                }
            }
        });
    }

    public void validarnumeros(JTextField campo) {
        campo.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!Character.isDigit(c)) {
                    e.consume();
                }
            }
        });
    }

    public void ingresarsolomayusculas(JTextField campo) {
        campo.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (Character.isLowerCase(c)) {
                    String cad = ("" + c).toUpperCase();
                    c = cad.charAt(0);
                    e.setKeyChar(c);
                }
                if (Character.isDigit(c)) {
                    e.consume();
                }
            }
        });

    }
    public static boolean validar_correo(String email){  
        String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email); 
        return matcher.matches();
    }
      public String encriptar(String secretKey, String cadena) {
        String encriptacion = "";
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] llavePassword = md5.digest(secretKey.getBytes("utf-8"));
            byte[] BytesKey = Arrays.copyOf(llavePassword, 24);
            SecretKey key = new SecretKeySpec(BytesKey, "DESede");
            Cipher cifrado = Cipher.getInstance("DESede");
            cifrado.init(Cipher.ENCRYPT_MODE, key);
            byte[] plainTextBytes = cadena.getBytes("utf-8");
            byte[] buf = cifrado.doFinal(plainTextBytes);
            byte[] base64Bytes = Base64.encodeBase64(buf);
            encriptacion = new String(base64Bytes);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Algo salió mal");
        }
        return encriptacion;
    }

    public String desencriptar(String secretKey, String cadenaEncriptada) {
        String desencriptacion = "";
        try {
            byte[] message = Base64.decodeBase64(cadenaEncriptada.getBytes("utf-8"));
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] digestOfPassword = md5.digest(secretKey.getBytes("utf-8"));
            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
            SecretKey key = new SecretKeySpec(keyBytes, "DESede");
            Cipher decipher = Cipher.getInstance("DESede");
            decipher.init(Cipher.DECRYPT_MODE, key);
            byte[] plainText = decipher.doFinal(message);
            desencriptacion = new String(plainText, "UTF-8");

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Algo salió mal");
        }
        return desencriptacion;
    }  
    
}
