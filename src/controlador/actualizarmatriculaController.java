package controlador;

import bases.incrementocodigo;
import bases.mimagenes;
import bases.validaciones;
import dao.estudianteDao;
import dao.gradoDao;
import dao.matriculaDao;
import dao.periodoacademicoDao;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import modelo.estudiante;
import modelo.matricula;
import vista.VactualizarMatricula;

/**
 *
 * @author Legion
 */
public class actualizarmatriculaController implements ActionListener {

    VactualizarMatricula vista;
    gradoDao da = new gradoDao();
    estudiante esmodelo = new estudiante();
    estudianteDao esdao = new estudianteDao();
    validaciones v = new validaciones();
    incrementocodigo in = new incrementocodigo();
    matricula mmodelo = new matricula();
    matriculaDao mdao = new matriculaDao();
    mimagenes i = new mimagenes();

    public actualizarmatriculaController(VactualizarMatricula vista) {
        this.vista = vista;
        //cargar daos a la base 
        da.nuevamatricula(vista.tbl_listacursos);
        vista.txt_periodo.setText(in.devolverundato("SELECT max(periodo)  as periodo from periodoacademico"));
        //poner en modo escucha los botones
        vista.btn_buscar.addActionListener(this);
        vista.btn_guardar.addActionListener(this);
    }

    public void iniciarvista(String fondos, mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
        i.insertarimagenesetiquetas(vista.fondo, fondos);
    }

    public int numerodecuposporaula(int codigocurso) {
        int respuesta = Integer.parseInt(in.devolverundato("SELECT cupos from aula,aulacurso,curso where aulacurso.codigoaula=aula.codigoAula and aulacurso.codigocurso=curso.codigocurso AND curso.codigocurso=" + codigocurso + ""));
        return respuesta;
    }

    public int numerodeestudiantesmatriculados(int codigocurso) {
        int respuesta = Integer.parseInt(in.devolverundato("SELECT COUNT(codigocurso) FROM matricula,periodoacademico  WHERE  matricula.codigocurso=" + codigocurso + " and matricula.codigoperiodo in (SELECT MAX(codigoperiodo) from periodoacademico)"));
        return respuesta;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Object o = e.getSource();
            if (o.equals(vista.btn_buscar)) {
                esmodelo.setCedula(vista.txt_cedula.getText());
                if (v.validacioncedula(esmodelo.getCedula()) == true && esdao.buscar(esmodelo) == true) {
                    vista.txt_nombrescompletos.setText(esmodelo.getNombre() + " " + esmodelo.getApellido());
                }
            }
            if (o.equals(vista.btn_guardar)) {
                if (vista.txt_cedula.getText().length() != 0) {
                    int sele = vista.tbl_listacursos.getSelectedRow();
                    mmodelo.setCodigocursoFk(Integer.parseInt(vista.tbl_listacursos.getValueAt(sele, 0).toString())); //obtengo el codigo del curso
                    mmodelo.setCedulaestudianteFK(vista.txt_cedula.getText());
                    mmodelo.setCodigoperiodoFK(Integer.parseInt(in.devolverundato("select codigoperiodo from periodoacademico where  periodo='" + vista.txt_periodo.getText()+ "'")));
                    int codigocusro = Integer.parseInt(in.devolverundato("select codigocurso from matricula"));
                    String cedulamatricula = in.devolverundato("select cedulaestudiante from matricula");
                    if (mmodelo.getCodigocursoFk() != codigocusro && cedulamatricula != mmodelo.getCedulaestudianteFK()) {
                        if (numerodeestudiantesmatriculados(mmodelo.getCodigocursoFk()) <= numerodecuposporaula(mmodelo.getCodigocursoFk())) {
                            mdao.guardar(mmodelo);
                        } else {
                            JOptionPane.showMessageDialog(null, "Cupo Lleno", "¡Avertencia¡", JOptionPane.WARNING_MESSAGE);
                        }

                    } else {
                        JOptionPane.showMessageDialog(null, "No puede matricular en este curso", "¡Avertencia¡", JOptionPane.WARNING_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Rellene el campo cedula", "¡Avertencia¡", JOptionPane.WARNING_MESSAGE);
                }
            }
        } catch (Exception ece) {
        }
    }
}
