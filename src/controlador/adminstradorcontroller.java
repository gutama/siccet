
package controlador;

import bases.mimagenes;
import bases.validaciones;
import dao.personaDao;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Action;
import javax.swing.JOptionPane;
import modelo.persona;
import modelo.personarol;
import modelo.rol;
import modelo.usuario;
import vista.viniciosecion;
import vista.vregistroadministrador;

/**
 *
 * @author Legion
 */
public class adminstradorcontroller implements ActionListener{
    vregistroadministrador vista;
    persona p=new persona();
    usuario u=new usuario();
    rol r=new  rol();
    personaDao pda=new personaDao();
    validaciones v=new validaciones();
    personarol pr=new personarol();
    public adminstradorcontroller(vregistroadministrador vista) {
        this.vista = vista;
        vista.btn_guadar.addActionListener(this);
    }
  
   
      public void iniciarvista(String fondos,mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
  i.insertarimagenesetiquetas(vista.fondo,fondos);
    }
      public int guadarrol(String cedula){
        r.setRol(cedula);
          return pda.guadarol(r);
      }
    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            String secretKey=" PROGRAMADORES GUTAMA Y TADAY";
            Object o=e.getSource();
            if(o.equals(vista.btn_guadar)){
            p.setCedula(vista.txt_cedula.getText());
            p.setNombre(vista.txt_nombres.getText());
            p.setAñonacimiento(vista.txt_añonacimiento.getText());
            if(vista.txt_contraseña.getText().equals(vista.txt_conficontraseña.getText())){
            pda.guardar(p);    
            u.setClave(v.encriptar(secretKey,vista.txt_contraseña.getText()));
            u.setCedulaFk(vista.txt_cedula.getText());
            r.setRol(vista.txt_cargo.getText());
            pr.setCedulaFK(vista.txt_cedula.getText());
            pr.setCodigorolFK(guadarrol(r.getRol()));// guarda el rol y devuelbe el codigo generado
            pda.guardarpersonarol(pr);//guarda datos intermedios
            pda.guardarusuario(u);
            }else{
            JOptionPane.showMessageDialog(null, "No coinsiden las contraseñas", "", JOptionPane.ERROR_MESSAGE);
                vista.txt_contraseña.setText("");
                vista.txt_conficontraseña.setText("");
            }   
            }
        } catch (Exception ec) {
        }
        
    
    }
    
}
