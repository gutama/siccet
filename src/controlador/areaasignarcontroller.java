package controlador;

import bases.incrementocodigo;
import bases.mimagenes;
import dao.areadministrativaDao;
import dao.docenteDao;
import dao.periodoacademicoDao;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import modelo.areaAdministrativa;
import modelo.docenteArea;
import vista.vasignarareaadministrativa;

/**
 *
 * @author Legion
 */
public class areaasignarcontroller implements ActionListener {

    vasignarareaadministrativa vista;
    docenteArea dmodelo = new docenteArea();
    incrementocodigo in = new incrementocodigo();
    areadministrativaDao area = new areadministrativaDao();
    docenteDao ddao = new docenteDao();
    periodoacademicoDao pedao = new periodoacademicoDao();
    DefaultTableModel columna = new DefaultTableModel();

    public areaasignarcontroller(vasignarareaadministrativa vista) {
        this.vista = vista;
        //cargar datos de la base  de datos
        area.cargarareas(vista.tbl_listaareas);
        ddao.listardocentes(vista.tbl_docentes);
        pedao.listarperiodos(vista.jc_añolectivo);
        //metodos
        cabeceradatos(vista.tbl_agregados);
        //poner en modo escucha los  botones
        vista.btn_agregar.addActionListener(this);
        vista.btn_quitar.addActionListener(this);
        vista.btn_grabar.addActionListener(this);
    }

   public void iniciarvista(String fondos,mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
  i.insertarimagenesetiquetas(vista.fondo,fondos);
    }

    public void cabeceradatos(JTable tb) {
        columna.addColumn("CODIGO");
        columna.addColumn("CEDULA");
        columna.addColumn("NOMBRES Y APELLIDOS");
        columna.addColumn("AREA");
        tb.setModel(columna);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Object o = e.getSource();
            if (o.equals(vista.btn_agregar)) {
                int selec = vista.tbl_listaareas.getSelectedRow();
                int sele = vista.tbl_docentes.getSelectedRow();
                String da[] = new String[4];
                da[0] = vista.tbl_listaareas.getValueAt(selec, 0).toString();
                da[1] = vista.tbl_docentes.getValueAt(sele, 0).toString();
                da[2] = vista.tbl_docentes.getValueAt(sele, 1).toString();
                da[3] = vista.tbl_listaareas.getValueAt(selec, 1).toString();
                columna.addRow(da);
                vista.tbl_agregados.setModel(columna);
            }
            if (o.equals(vista.btn_quitar)) {
                int selec = vista.tbl_agregados.getSelectedRow();
                ((DefaultTableModel) vista.tbl_agregados.getModel()).removeRow(selec);
            }
            if (o.equals(vista.btn_grabar)) {
                dmodelo.setCodigoperiodo(Integer.parseInt(in.devolverundato("select codigoperiodo from periodoacademico  where periodo='" + vista.jc_añolectivo.getSelectedItem() + "'")));
                for (int i = 0; i < vista.tbl_agregados.getRowCount(); i++) {
                    dmodelo.setCodigoarea(Integer.parseInt(vista.tbl_agregados.getValueAt(i, 0).toString()));
                    dmodelo.setCeduladocente(vista.tbl_agregados.getValueAt(i, 1).toString());
                   area.guardardocente_area(dmodelo);
                }
                JOptionPane.showMessageDialog(null, "guardado exitosamente", "", JOptionPane.INFORMATION_MESSAGE);
                
               
            }

        } catch (Exception ex) {
        }

    }

}
