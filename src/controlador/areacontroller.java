package controlador;

import bases.mimagenes;
import bases.validaciones;
import dao.areadministrativaDao;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JOptionPane;
import modelo.areaAdministrativa;
import vista.vareaadministrativa;

/**
 *
 * @author Legion
 */
public class areacontroller implements ActionListener, MouseListener {

    vareaadministrativa vista;
    areaAdministrativa amodelo = new areaAdministrativa();
    areadministrativaDao adao = new areadministrativaDao();
    validaciones v = new validaciones();

    public areacontroller(vareaadministrativa vista) {
        this.vista = vista;
        //cargar datos de la base 
        adao.cargarareas(vista.tbl_listarareas);
        //poner en modo escucha los botones
        vista.btn_grabar.addActionListener(this);
        vista.btn_modificar.addActionListener(this);
        vista.tbl_listarareas.addMouseListener(this);
        //metodos
        validarcampos();
    }

   public void iniciarvista(String fondos,mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
  i.insertarimagenesetiquetas(vista.fondo,fondos);
    }

    public void validarcampos() {
        v.ingresarsolomayusculas(vista.txt_nombre);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Object o = e.getSource();
            if (o.equals(vista.btn_grabar)) {
                amodelo.setNombre(vista.txt_nombre.getText());
                if (adao.guardar(amodelo) == true) {
                    JOptionPane.showMessageDialog(null, "guardado", "", JOptionPane.INFORMATION_MESSAGE);
                    adao.cargarareas(vista.tbl_listarareas);
                    vista.txt_nombre.setText("");
                }
            }
            if (o.equals(vista.btn_modificar)) {
                int sele = vista.tbl_listarareas.getSelectedRow();
                amodelo.setCodigoarea(Integer.parseInt(vista.tbl_listarareas.getValueAt(sele, 0).toString()));
                amodelo.setNombre(vista.txt_nombre.getText());
                if (adao.modificar(amodelo) == true) {
                    JOptionPane.showMessageDialog(null, "Modificado", "", JOptionPane.INFORMATION_MESSAGE);
                    adao.cargarareas(vista.tbl_listarareas);
                    vista.txt_nombre.setText("");
                }
            }
        } catch (Exception ex) {
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        int sele = vista.tbl_listarareas.getSelectedRow();
        amodelo.setNombre(vista.tbl_listarareas.getValueAt(sele, 1).toString());
        vista.txt_nombre.setText(amodelo.getNombre());
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

}
