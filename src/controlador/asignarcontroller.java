package controlador;

import bases.incrementocodigo;
import bases.mimagenes;
import dao.docenteDao;
import dao.gradoDao;
import dao.materiaDao;
import dao.periodoacademicoDao;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.docentecurso;
import modelo.docenteMateria;
import vista.vasignar;

/**
 *
 * @author Legion
 */
public class asignarcontroller implements ActionListener, MouseListener {

    vasignar vista;
    docenteMateria doc = new docenteMateria();
    docentecurso dmo = new docentecurso();
    docenteDao dodao = new docenteDao();
    // grado gra=new grado();
    gradoDao gradao = new gradoDao();
    materiaDao madao = new materiaDao();
    DefaultTableModel columna = new DefaultTableModel();
    incrementocodigo in = new incrementocodigo();

    public asignarcontroller(vasignar vista) {
        this.vista = vista;
        ///cargar datos de  la  base de datos 
        gradao.cargargrados(vista.tbl_grados);
        dodao.listardocentes(vista.tbl_docentes);
        vista.txt_periodo.setText(in.devolverundato("SELECT max(periodo)  as periodo from periodoacademico"));
        //pponer en modo escucha los botones 
        vista.tbl_grados.addMouseListener(this);
        vista.btn_agregar.addActionListener(this);
        vista.btn_quitar.addActionListener(this);
        vista.btn_grabar.addActionListener(this);
        //
        cabeceradatos(vista.tbl_agregados);
    }

    public void iniciarvista(String fondos, mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
        i.insertarimagenesetiquetas(vista.fondo, fondos);
    }

    public void cabeceradatos(JTable tb) {
        columna.addColumn("CODIGO");
        columna.addColumn("GRADO");
        columna.addColumn(" MATERIA");
        columna.addColumn("CEDULA");
        columna.addColumn("NOMBRES Y APELLIDOS");
        columna.addColumn("CARGO");
        tb.setModel(columna);

    }

    public void eliminarfilas(DefaultTableModel model) {
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        try {
            if (o.equals(vista.btn_agregar)) {
                int selec = vista.tbl_grados.getSelectedRow();
                int sele = vista.tbl_docentes.getSelectedRow();
                String da[] = new String[7];
                da[0] = vista.tbl_grados.getValueAt(selec, 0).toString();
                da[1] = vista.tbl_grados.getValueAt(selec, 2).toString() + " " + vista.tbl_grados.getValueAt(selec, 3).toString() + " " + vista.tbl_grados.getValueAt(selec, 4).toString() + " " + vista.tbl_grados.getValueAt(selec, 5).toString();
                da[2] = (String) vista.jc_listmaterias.getSelectedItem();
                da[3] = vista.tbl_docentes.getValueAt(sele, 0).toString();
                da[4] = vista.tbl_docentes.getValueAt(sele, 1).toString();
                da[5] = (String) vista.jc_cargo.getSelectedItem();
                columna.addRow(da);
                vista.tbl_agregados.setModel(columna);
            }
            if (o.equals(vista.btn_quitar)) {
                int selec = vista.tbl_agregados.getSelectedRow();
                ((DefaultTableModel) vista.tbl_agregados.getModel()).removeRow(selec);
            }
            if (o.equals(vista.btn_grabar)) {
                doc.setCodigoperiodoFK(Integer.parseInt(in.devolverundato("select codigoperiodo from periodoacademico  where periodo='" + vista.txt_periodo + "'")));
                dmo.setCodigoperiodoFK(Integer.parseInt(in.devolverundato("select codigoperiodo from periodoacademico  where periodo='" + vista.txt_periodo + "'")));
                for (int i = 0; i < vista.tbl_agregados.getRowCount(); i++) {
                    dmo.setCodigogradoFK(Integer.parseInt(vista.tbl_agregados.getValueAt(i, 0).toString()));
                    dmo.setCeduladocenteFK(vista.tbl_agregados.getValueAt(i, 3).toString());
                    dmo.setTipo(vista.tbl_agregados.getValueAt(i, 5).toString());
                    dmo.setCodigomateriaFK(Integer.parseInt(in.devolverundato("select codigomateria from materia where nombre='" + vista.tbl_agregados.getValueAt(i, 2) + "'")));
                    doc.setCeduladocenteFK(vista.tbl_agregados.getValueAt(i, 3).toString());
                    doc.setCodigomateriaFK(Integer.parseInt(in.devolverundato("select codigomateria from materia where nombre='" + vista.tbl_agregados.getValueAt(i, 2) + "'")));
                    dodao.docenteCurso(dmo);
                    dodao.docente_materia(doc);
                }
                vista.jc_listmaterias.setToolTipText(" ");
                eliminarfilas(columna);
                JOptionPane.showMessageDialog(null, "guardado exitosamente", "", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (Exception ex) {
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        vista.jc_listmaterias.setSelectedItem("");
        int selec = vista.tbl_grados.getSelectedRow();
        int codigogrado = Integer.parseInt(vista.tbl_grados.getValueAt(selec, 1).toString());
        madao.listarmateriasporgrados(vista.jc_listmaterias, codigogrado);
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

}
