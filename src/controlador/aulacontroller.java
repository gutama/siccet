package controlador;

import bases.incrementocodigo;
import bases.mimagenes;
import bases.validaciones;
import dao.aulaDao;
import java.awt.event.*;
import javax.swing.*;
import modelo.aula;
import vista.vaula;

/**
 *
 * @author Mesias
 */
public class aulacontroller implements ActionListener{
    aulaDao sg=new aulaDao();
    aula mo=new aula();
    vaula vista;
    incrementocodigo in=new incrementocodigo();
    public aulacontroller(vaula vista) {
        this.vista = vista;
                         sg.listarcupos(vista.jc_listaulas);
         vista.btn_guardar.setEnabled(false);
          vista.btn_modificar.setEnabled(false);
          vista.txt_cupo.setText("");
        //poner en  modo escucha los botones
        this.vista.btn_guardar.addActionListener(this);
        this.vista.btn_modificar.addActionListener(this);
        vista.jc_listaulas.addActionListener(this);
        vista.btn_nuevo.addActionListener(this);
        //cargar los datos de la  base
        validarcampos();
    }

    
    public void iniciarvista(String fondos,mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
  i.insertarimagenesetiquetas(vista.fondo,fondos);
    }

    public void limpliarcajas() {
        vista.txt_cupo.setText("");
        vista.txt_numeroaula.setText("");
    }
    
    public void validarcampos(){
        validaciones v=new validaciones();
        //v.ingresarsolomayusculas(vista.txt_nombreareapeda);
        v.validarnumeros(vista.txt_cupo);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Object o = e.getSource();
            if (o.equals(vista.btn_modificar)) {
                mo.setNumerodeaula(vista.txt_numeroaula.getText());
                mo.setCupo(Integer.parseInt(vista.txt_cupo.getText()));
                if(sg.modificar(mo)==true){
                sg.listarcupos(vista.jc_listaulas);
                JOptionPane.showMessageDialog(null,"Modificado","",JOptionPane.INFORMATION_MESSAGE);
                }  
            }      
            if (o.equals(vista.btn_guardar)) {// boton para guardar l las  materias
                if (vista.txt_cupo.getText().length() > 0 && vista.txt_numeroaula.getText().length() > 0) {
                    mo.setNumerodeaula(vista.txt_numeroaula.getText());
                    mo.setCupo(Integer.parseInt(vista.txt_cupo.getText()));
                    sg.guardar(mo);
                    sg.listarcupos(vista.jc_listaulas);
                    this.limpliarcajas();
                    vista.btn_guardar.setEnabled(false);
                    vista.btn_nuevo.setEnabled(true);
                } else {
                    JOptionPane.showMessageDialog(null, "Ingrese el nombre de la maeria", "!Advertencia!", JOptionPane.WARNING_MESSAGE);
                }
            }
            if(o.equals(vista.jc_listaulas)){
                mo.setNumerodeaula(in.devolverundato("select numeroaula from  aula where cupos="+vista.jc_listaulas.getSelectedItem()+"")); 
             vista.txt_numeroaula.setEditable(false);
             vista.txt_numeroaula.setText(String.valueOf(mo.getNumerodeaula()));
             vista.txt_cupo.setText(String.valueOf(vista.jc_listaulas.getSelectedItem()));
             vista.btn_modificar.setEnabled(true);
            }
            if(o.equals(vista.btn_nuevo)){
                limpliarcajas();
               vista.txt_numeroaula.setEditable(true);
               vista.btn_guardar.setEnabled(true);
               vista.btn_modificar.setEnabled(false);
               vista.btn_nuevo.setEnabled(false);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    }

}
