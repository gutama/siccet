
package controlador;

import bases.incrementocodigo;
import bases.mimagenes;
import dao.aulaDao;
import dao.gradoDao;
import dao.periodoacademicoDao;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import modelo.aulacurso;
import modelo.periodoAcademico;
import vista.vaulacurso;

/**
 *
 * @author Legion
 */
public class aulacursocontroller implements ActionListener{
    vaulacurso vista;
    aulacurso amodelo=new aulacurso();
    aulaDao adao=new aulaDao();
    gradoDao gdao=new gradoDao();
    incrementocodigo in=new incrementocodigo();
    public aulacursocontroller(vaulacurso vista) {
        this.vista = vista;
              adao.listaraulas(vista.jc_listaaulas);
               vista.txt_periodo.setText(in.devolverundato("SELECT max(periodo)  as periodo from periodoacademico"));
              gdao.cargargrados(vista.tb_listagrados);
              //poner en modo escucha el boton
              vista.btn_grabar.addActionListener(this);
    }
  public void iniciarvista(String fondos,mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
  i.insertarimagenesetiquetas(vista.fondo,fondos);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        Object o=e.getSource();
        if(o.equals(vista.btn_grabar)){
            int sele=vista.tb_listagrados.getSelectedRow();
           amodelo.setCodigocursofk(Integer.parseInt(vista.tb_listagrados.getValueAt(sele,0).toString()));
           amodelo.setCodigoaulafk(Integer.parseInt(in.devolverundato("select codigoaula from aula where numeroaula='"+vista.jc_listaaulas.getSelectedItem()+"'")));
           amodelo.setCodigoperiodofk(Integer.parseInt(in.devolverundato("select codigoperiodo  from periodoacademico where periodo='"+vista.txt_periodo.getText()+"'")));
        adao.guardarasignacionaulaycurso(amodelo);
        }
    }
    
}
