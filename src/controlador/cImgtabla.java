package controlador;


import java.awt.Component;
import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Mesias
 */
public class cImgtabla extends DefaultTableCellRenderer{
   public Component getTableCellRendererComponent(JTable tabla,Object value,boolean isSelected, boolean hasfocus,int row, int colum ){
     if(value instanceof JLabel){
     JLabel lb1=(JLabel) value;
     return lb1;
     } 
       if(value instanceof JButton){
       JButton bt1= (JButton) value;
       if(isSelected){
       bt1.setForeground(tabla.getSelectionForeground());
       bt1.setBackground(tabla.getSelectionBackground());
       }else
          bt1.setForeground(tabla.getForeground()); 
       bt1.setBackground(UIManager.getColor("Button.background"));
       return bt1;
       }
       return super.getTableCellRendererComponent(tabla, value, isSelected, hasfocus, row, colum);
   }
    
}
