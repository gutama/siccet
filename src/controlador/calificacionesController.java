package controlador;

import bases.incrementocodigo;
import bases.mimagenes;
import bases.validaciones;
import com.placeholder.PlaceHolder;
import dao.calificacionesDao;
import dao.docenteDao;
import dao.gradoDao;
import dao.materiaDao;
import dao.matriculaDao;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.caliCualitativa;
import modelo.caliCuantitativa;
import modelo.docente;
import modelo.docentecurso;
import modelo.matricula;
import vista.vcalificaciones;

/**
 *
 * @author Legion
 */
public class calificacionesController implements ActionListener,MouseListener {

    vcalificaciones vista;
    caliCuantitativa modelo = new caliCuantitativa();
    materiaDao mdao = new materiaDao();
    gradoDao gdao = new gradoDao();
    docenteDao ddao = new docenteDao();
    docente dmo = new docente();
    docentecurso dc = new docentecurso();
    validaciones v = new validaciones();
    incrementocodigo in = new incrementocodigo();
    DefaultTableModel columnas = new DefaultTableModel();
    DefaultTableModel columna = new DefaultTableModel();
    DefaultTableModel colum = new DefaultTableModel();
    matriculaDao madao = new matriculaDao();
    matricula matri=new matricula();
    caliCualitativa ca = new caliCualitativa();
    calificacionesDao cadao = new calificacionesDao();

    public calificacionesController(vcalificaciones vista) {
        this.vista = vista;
        //poner en modo escucha los  botones
        vista.btn_buscar.addActionListener(this);
        vista.jc_listamaterias.addActionListener(this);
        vista.btn_guardar.addActionListener(this);
        vista.btn_agregar.addActionListener(this);
        vista.tbl_listagrados.addMouseListener(this);
        vista.tbl_listaes.addMouseListener(this);
        vista.btn_modificar.addActionListener(this);
        validarcampos();
        cabececera(vista.tbl_listaes);
        cabececeracalificaciones(vista.tbl_calificaciones);
        cabececeragrado(vista.tbl_listagrados);
        // uso de la libreria placeholder
        PlaceHolder holder = new PlaceHolder(vista.txt_quimestre1, "0.0");
        PlaceHolder holders = new PlaceHolder(vista.txt_quimestre2, "0.0");
        PlaceHolder hold = new PlaceHolder(vista.txt_comportamiento, "A");
    }

    public void iniciarvista(String fondos, mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
        i.insertarimagenesetiquetas(vista.fondo, fondos);
    }

    public void validarcampos() {
        v.validarnumeros(vista.txt_cedula);
        v.limitarcaracteres(vista.txt_cedula, 10);
        v.validarnumerosdecimales(vista.txt_quimestre1);
        v.validarnumerosdecimales(vista.txt_quimestre2);
        v.ingresarsolomayusculas(vista.txt_comportamiento);
    }

    public void cabececera(JTable tabla) {
        columnas.addColumn("COD.MATRICULA");
        columnas.addColumn("CEDULA");
        columnas.addColumn("ESTUDIANTES");
        tabla.setModel(columnas);
    }

    public void cabececeracalificaciones(JTable tabla) {
        columna.addColumn("COD.MATRICULA");
        columna.addColumn("ESTUDIANTES");
        columna.addColumn("QUIMESTRE 1");
        columna.addColumn("QUIMESTRE 2");
        columna.addColumn("PROMEDIO");
        columna.addColumn("COMPORTAMIENTO");
        tabla.setModel(columna);
    }
     public void cabececeragrado(JTable tabla) {
        colum.addColumn("COD.CURSO");
        colum.addColumn("NOMBRE");
        colum.addColumn("TIPO");
        colum.addColumn("PARALELO");
        tabla.setModel(colum);
    }


    public void eliminarfilas(DefaultTableModel model) {
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        try {
            if (o.equals(vista.btn_buscar)) {
                dmo.setCedula(vista.txt_cedula.getText());
                if (v.validacioncedula(vista.txt_cedula.getText()) == true && ddao.buscar(dmo) == true) {
                    vista.txt_nombres.setText(dmo.getNombre() + " " + dmo.getApellido());
                    int codigoperiodo = Integer.parseInt(in.devolverundato("SELECT MAX(codigoperiodo) from periodoacademico"));
                    mdao.listarmateriaspordocente(vista.jc_listamaterias, vista.txt_cedula.getText(), codigoperiodo);

                }
            }
            if (o.equals(vista.jc_listamaterias)) {
                dc.setCeduladocenteFK(vista.txt_cedula.getText());
                dc.setCodigomateriaFK(Integer.parseInt(in.devolverundato("select codigomateria from materia where nombre='" + vista.jc_listamaterias.getSelectedItem() + "'")));
                ddao.listarcursosdocente(vista.tbl_listagrados, dc,colum);   
            }  
            if (o.equals(vista.btn_guardar)) {
                modelo.setCodigomateriaFK(dc.getCodigomateriaFK());
                modelo.setCodigonotacualitativaFK(Integer.parseInt(in.devolverundato("select codigonotacualitativa from notacualitativa where equivalente=' " + ca.calificacioncualitativa(modelo.promedio()) + " '")));
                for (int j = 0; j < vista.tbl_calificaciones.getRowCount(); j++) {
                    modelo.setCodigomatriculaFK(Integer.parseInt(vista.tbl_calificaciones.getValueAt(j, 0).toString()));
                    modelo.setQuimestre1(Double.parseDouble(vista.tbl_calificaciones.getValueAt(j, 2).toString()));
                    modelo.setQuimestre2(Double.parseDouble(vista.tbl_calificaciones.getValueAt(j, 3).toString()));
                    modelo.setPromedioanual(Double.parseDouble(vista.tbl_calificaciones.getValueAt(j, 4).toString()));
                    modelo.setComportamiento(vista.tbl_calificaciones.getValueAt(j, 5).toString());
                    cadao.guardar(modelo);
                }
                JOptionPane.showMessageDialog(null, "Guardado exitosamente");
                eliminarfilas(columna);
                eliminarfilas(colum);
                vista.btn_modificar.setEnabled(true);
            }
            if(o.equals(vista.btn_modificar)){
            modelo.setCodigomateriaFK(dc.getCodigomateriaFK());
           int sele = vista.tbl_listaes.getSelectedRow();
          matri.setCedulaestudianteFK(vista.tbl_listaes.getValueAt(sele,1).toString());
          modelo.setQuimestre1(Double.parseDouble(vista.txt_quimestre1.getText()));
          modelo.setQuimestre2(Double.parseDouble(vista.txt_quimestre2.getText()));
          modelo.setComportamiento(vista.txt_comportamiento.getText());
          modelo.setCodigonotacualitativaFK(Integer.parseInt(in.devolverundato("select codigonotacualitativa from notacualitativa where equivalente=' " + ca.calificacioncualitativa(modelo.promedio()) + " '")));
          modelo.setPromedioanual(modelo.promedio());
          if(cadao.modificar(modelo, matri)==true){
          JOptionPane.showMessageDialog(null, "Calificación modificado");
            vista.btn_agregar.setEnabled(true);
            vista.btn_guardar.setEnabled(true);
            vista.txt_quimestre1.setText(" ");
            vista.txt_quimestre2.setText(" ");
            vista.txt_comportamiento.setText("");
          }
            }
            if (vista.btn_agregar.equals(o)) {
                int sele = vista.tbl_listaes.getSelectedRow();
                String datos[] = new String[6];
                datos[0] = vista.tbl_listaes.getValueAt(sele, 0).toString();
                datos[1] = vista.tbl_listaes.getValueAt(sele, 2).toString();
                datos[2] = vista.txt_quimestre1.getText();
                datos[3] = vista.txt_quimestre2.getText();
                modelo.setQuimestre1(Double.parseDouble(vista.txt_quimestre1.getText()));
                modelo.setQuimestre2(Double.parseDouble(vista.txt_quimestre2.getText()));
                datos[4] = String.valueOf(modelo.promedio());
                datos[5] = vista.txt_comportamiento.getText();
                columna.addRow(datos);
                vista.tbl_calificaciones.setModel(columna);
                vista.txt_quimestre1.setText("");
                vista.txt_quimestre2.setText("");
                vista.txt_comportamiento.setText("");
                 int selec = vista.tbl_listaes.getSelectedRow();
                ((DefaultTableModel) vista.tbl_listaes.getModel()).removeRow(selec);
            }
        } catch (Exception ex) {
        }
    }

    @Override
    public void mouseClicked(MouseEvent arg0) {
        if(arg0.getSource().equals(vista.tbl_listagrados)){
         int sele = vista.tbl_listagrados.getSelectedRow();
      int codigocurso=Integer.parseInt(vista.tbl_listagrados.getValueAt(sele,0).toString());
      int ma=Integer.parseInt(in.devolverundato("select count(codigomatricula) from matricula  where codigocurso="+codigocurso+" and matricula.codigoperiodo in  (SELECT max(codigoperiodo) from periodoacademico)"));
      if(ma>0){
             madao.cargarestudiantesporcursomatriculado(vista.tbl_listaes, columnas,codigocurso);
             }else
                 if(ma==0){
                                eliminarfilas(columnas);
                 JOptionPane.showMessageDialog(null,"No hay estudiantes matriculados","",JOptionPane.INFORMATION_MESSAGE);
             }
        }
        if(arg0.getSource().equals(vista.tbl_listaes)){
          int sele = vista.tbl_listaes.getSelectedRow();
          matri.setCedulaestudianteFK(vista.tbl_listaes.getValueAt(sele,1).toString());
          modelo.setCodigomateriaFK(dc.getCodigomateriaFK());
          if(cadao.buscar(modelo, matri)==true){
          vista.txt_quimestre1.setText(String.valueOf(modelo.getQuimestre1()));
          vista.txt_quimestre2.setText(String.valueOf(modelo.getQuimestre2()));
          vista.txt_comportamiento.setText(modelo.getComportamiento());
          if(modelo.getQuimestre1()>0){
          vista.btn_agregar.setEnabled(false);
            vista.btn_guardar.setEnabled(false);
          }else
              vista.btn_modificar.setEnabled(false);
              
          }
        }
    }

    @Override
    public void mousePressed(MouseEvent arg0) {
        
    }

    @Override
    public void mouseReleased(MouseEvent arg0) {
      
    }

    @Override
    public void mouseEntered(MouseEvent arg0) {
        
    }

    @Override
    public void mouseExited(MouseEvent arg0) {
        }

}
