
package controlador;

import bases.incrementocodigo;
import bases.mimagenes;
import bases.validaciones;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import dao.estudianteDao;
import dao.matriculaDao;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import modelo.estudiante;
import modelo.institucion;
import modelo.matricula;
import modelo.periodoAcademico;
import vista.vcertificadomatricula;

/**
 *
 * @author Legion
 */
public class certificadomatriculacontroller implements ActionListener{
    vcertificadomatricula vista;
    institucion mo=new institucion();
    estudianteDao edao=new estudianteDao();
    incrementocodigo in=new incrementocodigo();
      matricula modelo = new matricula();
    estudiante emode=new estudiante();
    validaciones v= new validaciones();
      matriculaDao mdao = new matriculaDao();
       Document documento = new Document();
      
    public certificadomatriculacontroller(vcertificadomatricula vista) {
        this.vista = vista;
        //
        vista.txt_cedula.addActionListener(this);
        vista.jc_listaaños.addActionListener(this);
        vista.btn_imprimir.addActionListener(this);
    }
  
    
    public void iniciarvista(String fondos, mimagenes i) {
        vista.setLocationRelativeTo(null);
//        i.insertarimagenesetiquetas(vista.fondo, fondos);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");

        
       
        
    }
      public void abrirpdf(String cetificado) {
        try {
            File path = new File(cetificado + ".pdf");
            Desktop.getDesktop().open(path);
        } catch (Exception e) {
        }
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        Object o=e.getSource();
                Font fuente = new Font();
        fuente.setStyle(Font.BOLD);
        fuente.setSize(14);
         Font parrafo = new Font();
        parrafo.setStyle(Font.NORMAL);
        parrafo.setSize(14);

        Font tipografia = new Font();
        tipografia.setSize(10);
        Font TITULO = new Font();
        TITULO.setSize(18);
        TITULO.setStyle(Font.BOLD);
        Font enca = new Font();
        enca.setSize(8);
        enca.setStyle(Font.NORMAL);
        try {
             if (o.equals(vista.txt_cedula)) {
                emode.setCedula(vista.txt_cedula.getText());
                if (v.validacioncedula(emode.getCedula()) == true) {
                    edao.buscar(emode);
                     modelo.setCedulaestudianteFK(vista.txt_cedula.getText());
                     mdao.listargradoscursadosporelestudiante(vista.jc_listaaños,modelo);
                    vista.txt_nombreestudiante.setText(emode.getNombre() + " " + emode.getApellido());       
                }

            }
             if(o.equals(vista.jc_listaaños)){
                 periodoAcademico p=new periodoAcademico();
             String grado = (String) vista.jc_listaaños.getSelectedItem();
        modelo.setCedulaestudianteFK(vista.txt_cedula.getText());
                String tipo = grado.substring(8);     
                if (tipo.equals("EGB")) {
                    modelo.setCodigocursoFk(Integer.parseInt(in.devolverundato("select codigogrado  from grado  where nombre='" + grado.substring(0, 8) + "' and tipo='" + tipo + "'")));               
                } else {
                    tipo = grado.substring(9);
                    modelo.setCodigocursoFk(Integer.parseInt(in.devolverundato("select codigogrado  from grado  where nombre='" + grado.substring(0, 8) + "' and tipo='" + tipo + "'")));
                    
                }
               if(mdao.cargardatosparacertificado(modelo, p)){
               vista.txt_añolectivo.setText(p.getPeriodo());
               vista.txt_fechamatriculacion.setText(modelo.getFecha());
               vista.txt_codigo.setText(String.valueOf(modelo.getCodigomatricula()));
               } 
             }
             if(o.equals(vista.btn_imprimir)){
                  // creacion del documento pdf
                FileOutputStream archivo;
                //encabesado del documento
                Image imagen = Image.getInstance("escudocolegio.jpg");
                imagen.setAlignment(Element.ALIGN_CENTER);
                imagen.scaleAbsolute(80, 40);
                imagen.setAbsolutePosition(250, 800);
                Paragraph titulo1 = new Paragraph("UNIDAD EDUCATIVA MOLLETURO", TITULO);
                titulo1.setAlignment(1);
                Paragraph direccion = new Paragraph("Molleturo - Cuenca - Azuay", enca);
                direccion.setAlignment(Element.ALIGN_CENTER);
                Paragraph datos = new Paragraph("Tlfno: 074045599 - E-mail: colegiomolleturo1981@hotmail.com ", enca);
                datos.setAlignment(Element.ALIGN_CENTER);
                Paragraph linea = new Paragraph("_____________________________________________________________________________________________________________________", enca);
                // cuerpo del documento
                Paragraph titulo = new Paragraph("CERTIFICADO DE MATRICULA", fuente);

                try {
                    archivo = new FileOutputStream(vista.lb_nombrelis.getText() + ".pdf");
                    PdfWriter.getInstance(documento, archivo);
                    documento.open();
                    titulo.setAlignment(1);
                    documento.add(imagen);
                    documento.add(titulo1);
                    documento.add(direccion);
                    documento.add(datos);
                    documento.add(linea);
                    documento.add(Chunk.NEWLINE);
                    documento.add(titulo);
                       documento.add(Chunk.NEWLINE);
                    Paragraph tex = new Paragraph("QUE:De acuerdo al Art. 164, del Reglamento General "
                            + "a la ley Organica de educación intercultural y Bilingue, el/la Sr/Srta: "+vista.txt_nombreestudiante.getText()+" con cédula de ciudadanía numero: "+vista.txt_cedula.getText()+
                            ". Se encuentra legalmente matriculado/a en el "+vista.jc_listaaños.getSelectedItem()+" desde el "+vista.txt_fechamatriculacion.getText()+
                            " durante el año lectivo: "+vista.txt_añolectivo.getText()+" con codigo de matricula numero: "+vista.txt_codigo.getText(),parrafo);
                    Paragraph fina=new Paragraph("Esto es todo en  cuanto puedo certificar en honor a la verdad.");
                    Paragraph fechafir=new Paragraph("Molleturo,"+vista.txt_fechacertificacion.getText());
                    fechafir.setAlignment(Element.ALIGN_RIGHT);
                    Paragraph secre=new Paragraph(vista.txt_nombresecretario.getText());
                    secre.setAlignment(Element.ALIGN_CENTER);
                    Paragraph secr=new Paragraph("Secretaria");
                    secr.setAlignment(Element.ALIGN_CENTER);
                    documento.add(tex);
                    documento.add(Chunk.NEWLINE);
                    documento.add(fina);
                     documento.add(Chunk.NEWLINE);
                    documento.add(fechafir);
                     documento.add(Chunk.NEWLINE);
                       documento.add(Chunk.NEWLINE);
                       documento.add(Chunk.NEWLINE);
                       documento.add(secre);
                       documento.add(secr);
                    documento.close();
                } catch (Exception ex) {

                }
                abrirpdf(vista.lb_nombrelis.getText());
                vista.txt_añolectivo.setText(" ");
                vista.txt_codigo.setText(" ");
                vista.txt_fechamatriculacion.setText(" ");
             }
        } catch (Exception ex) {
        }
    }
    
}
