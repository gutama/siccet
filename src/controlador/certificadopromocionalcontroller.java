package controlador;

import bases.incrementocodigo;
import bases.mimagenes;
import bases.validaciones;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import dao.calificacionesDao;
import dao.estudianteDao;
import dao.institucionDao;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import impresiones.PlantillaCertificado;
import impresiones.cuadrocalificaciones;
import modelo.estudiante;
import modelo.grado;
import modelo.institucion;
import vista.vcertificado;

/**
 *
 * @author Legion
 */
public class certificadopromocionalcontroller implements ActionListener {

    vcertificado vista;
    institucionDao idao = new institucionDao();
    institucion imodelo = new institucion();
    incrementocodigo in = new incrementocodigo();
    estudiante emode = new estudiante();
    estudianteDao edao = new estudianteDao();
    grado gmodelo = new grado();
    validaciones v = new validaciones();
    calificacionesDao cda = new calificacionesDao();
    cuadrocalificaciones c = new cuadrocalificaciones();
    PlantillaCertificado pl = new PlantillaCertificado();
    Document documento = new Document();

    public certificadopromocionalcontroller(vcertificado vista) {
        this.vista = vista;
        //
        cargardatosdelainstitucion();
        validarcampos();
        //poner en modo escucha 
        vista.txt_cedulaestudiante.addActionListener(this);
        vista.btn_imprimir.addActionListener(this);

    }

    public void iniciarvista(String fondos, mimagenes i) {
        vista.setLocationRelativeTo(null);
        i.insertarimagenesetiquetas(vista.escudo1, "/imagenes/escudo.png");
        i.insertarimagenesetiquetas(vista.escudoministerio, "/imagenes/ministerio.png");
        //i.insertarimagenesetiquetas(vista.fondo, fondos);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");

    }

    public void cargardatosdelainstitucion() {
        idao.datosdelainstitucion(imodelo);
        vista.txt_codigoamie.setText(imodelo.getCodigo());
        vista.txt_nombreinstitucion.setText(imodelo.getNombre());
        vista.txt_regimen.setText(imodelo.getRegimen());
        vista.txt_añolectivo.setText(in.devolverundato("Select max(periodo) from periodoacademico"));
    }

    public void validarcampos() {
        v.validarnumeros(vista.txt_cedulaestudiante);
        v.limitarcaracteres(vista.txt_cedulaestudiante, 10);
        v.limitarcaracteres(vista.txt_promedio, 4);
    }

    public void abrirpdf(String cetificado) {
        try {
            File path = new File(cetificado + ".pdf");
            Desktop.getDesktop().open(path);
        } catch (Exception e) {
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Font fuente= new Font();
        fuente.setStyle(Font.BOLD);
        fuente.setSize(14);
        Font tipografia= new Font();
         tipografia.setSize(10);
         Font fuentes= new Font();
        fuentes.setStyle(Font.BOLD);
        fuentes.setSize(12);
         
        try {
            Object o = e.getSource();
            if (o.equals(vista.btn_imprimir)) {
                pl.setCodigoAMIE(vista.txt_codigoamie.getText());
                pl.setAñolectivo(vista.txt_añolectivo.getText());
                pl.setRegimen(vista.txt_regimen.getText());
                pl.setNombreinstitucion(vista.txt_nombreinstitucion.getText());
                pl.setEstudiante(vista.txt_nombreestudiante.getText());
                pl.setGrado(vista.txt_curso.getText());
                pl.setImagen1("escudo.png");
                pl.setImagen2("ministerio.png");
                pl.setPromedio(vista.txt_promedio.getText());
                pl.setCertificado(vista.lb_certificado.getText());
                pl.setCedula(vista.txt_cedulaestudiante.getText());
                pl.setLugarfirma(vista.txt_lugarfirma.getText());
                // creacion del documento pdf
                FileOutputStream archivo;
                Paragraph titulo = new Paragraph("CERTIFICADO DE PROMOCIÓN",fuente);
                try {
                    archivo = new FileOutputStream(pl.getCertificado() + ".pdf");
                    PdfWriter.getInstance(documento, archivo);
                    documento.open();
                    titulo.setAlignment(1);
                    Image imagen=null,imagen1=null;
                    try {
                 imagen = Image.getInstance(pl.getImagen1());
                        imagen.scaleAbsolute(80,40);
                        imagen.setAbsolutePosition(30,800);
                         imagen1 = Image.getInstance(pl.getImagen2());
                        imagen1.scaleAbsolute(80,40);
                        imagen1.setAbsolutePosition(500,800);
                           
                    } catch (Exception ex) {
                    } 
                    documento.add(imagen);
                    documento.add(imagen1);
                    documento.add(new Paragraph("CÓDIGO AMIE: " + pl.getCodigoAMIE() + "      AÑO LECTIVO: " + pl.getAñolectivo() + "        REGIMEN: "+ pl.getRegimen(),fuente));
                    documento.add(Chunk.NEWLINE);
                    documento.add(titulo);
                    documento.add(Chunk.NEWLINE);
                    Paragraph texto = new Paragraph("El Rector (a) / Director (a)  de la institucion educativa:");
                    Paragraph texto1 = new Paragraph("De conformidad con lo  prescrito en el Art. 197  del Reglamento  General  de la Ley Orgánica  de Educación Intercultural y demas normativas vigentes, certifica que el / la estudiante");
                    Paragraph texto3 = new Paragraph("del " + pl.getGrado() + " CODIGO ESTUDIANTE No " + pl.getCedula() + ", obtuvo las siguietes calificaciones durante el presente  año lectivo.");
                    documento.add(texto);
                    documento.add(Chunk.NEWLINE);
                    Paragraph ins = new Paragraph(pl.getNombreinstitucion(),fuente);
                    ins.setAlignment(1);
                    documento.add(ins);
                    documento.add(Chunk.NEWLINE);
                    documento.add(texto1);
                    documento.add(Chunk.NEWLINE);
                    Paragraph es = new Paragraph(pl.getEstudiante(),fuente);
                    es.setAlignment(1);
                    documento.add(es);
                    documento.add(Chunk.NEWLINE);
                    documento.add(texto3);
                    documento.add(Chunk.NEWLINE);
                    // creo la tabla  en el  pdf
                    PdfPTable tabla = new PdfPTable(4);
                    tabla.setWidthPercentage(100);
                    PdfPCell area = new PdfPCell(new Phrase("ÁREA",fuentes));
                    area.setBackgroundColor(BaseColor.WHITE);
                    area.setHorizontalAlignment(Element.ALIGN_CENTER);
                   PdfPCell materia = new PdfPCell(new Phrase("ASIGNATURA",fuentes));
                    materia.setBackgroundColor(BaseColor.WHITE);
                    materia.setHorizontalAlignment(Element.ALIGN_CENTER);
                    PdfPCell calificacion = new PdfPCell(new Phrase("CALIFICACIÓN CUANTITATIVA",fuentes));
                    calificacion.setBackgroundColor(BaseColor.WHITE);
                    calificacion.setHorizontalAlignment(Element.ALIGN_CENTER);
                    PdfPCell equivalente = new PdfPCell(new Phrase("CALIFICACIÓN CUALITATIVA",fuentes));
                    equivalente.setBackgroundColor(BaseColor.WHITE);
                   equivalente.setHorizontalAlignment(Element.ALIGN_CENTER);
                    PdfPCell prome = new PdfPCell(new Phrase("PROMEDIO:"+pl.getPromedio()));
                   prome.setBackgroundColor(BaseColor.WHITE);
                    prome.setHorizontalAlignment(Element.ALIGN_CENTER);
                    prome.setColspan(3);
                    tabla.addCell(materia);
                    tabla.addCell(area);
                    tabla.addCell(calificacion);
                    tabla.addCell(equivalente);
                    // agrego datos a la  tabla del pdf
                    for (int i = 0; i < vista.tbl_cuadrocalificacionbes.getRowCount(); i++) {
                        c.setArea(vista.tbl_cuadrocalificacionbes.getValueAt(i, 0).toString());
                        c.setMateria(vista.tbl_cuadrocalificacionbes.getValueAt(i, 1).toString());
                        c.setCalificacioncuantitativa(vista.tbl_cuadrocalificacionbes.getValueAt(i, 2).toString());
                        c.setCalicualitativa(vista.tbl_cuadrocalificacionbes.getValueAt(i, 3).toString());
                        Paragraph areape=new Paragraph(c.getArea(),tipografia);
                         Paragraph materias=new Paragraph(c.getMateria(),tipografia);
                          Paragraph calificacione=new Paragraph(c.getCalificacioncuantitativa(),tipografia);
                          calificacione.setAlignment(1);                        
                           Paragraph equiva=new Paragraph(c.getCalicualitativa(),tipografia);
                        tabla.addCell(areape);
                        tabla.addCell(materias);
                        tabla.addCell(calificacione);
                        tabla.addCell(equiva);     
                    }
                    c.setComportamiento(vista.txt_comportamiento.getText());
                    documento.add(tabla);
                    documento.add(new Paragraph("PROMEDIO: " + pl.getPromedio()+"   "+"COMPORTAMIENTO: "+c.getComportamiento(),fuente));
                     documento.add(Chunk.NEWLINE);
                    documento.add(new Paragraph("Para certificar sucribe el/la director/a - Rector/a del Plantel"));
                    documento.add(new Paragraph("Dado y firmado en: " + pl.getLugarfirma()));
                    documento.add(Chunk.NEWLINE);
                    documento.add(Chunk.NEWLINE);
                    documento.add(Chunk.NEWLINE);
                    documento.add(new Paragraph("     Rector(a)/Director(a)             " + "                      secretario(a)/Tutor(a)", fuente));
                    documento.close();
                } catch (Exception ex) {

                }
                abrirpdf(vista.lb_certificado.getText());
            }
            if (o.equals(vista.txt_cedulaestudiante)) {
                emode.setCedula(vista.txt_cedulaestudiante.getText());
                if (v.validacioncedula(emode.getCedula()) == true) {
                    edao.buscarestudiantecurso(emode, gmodelo);
                    vista.txt_nombreestudiante.setText(emode.getNombre() + " " + emode.getApellido());
                    vista.txt_curso.setText(gmodelo.getNombre() + " " + gmodelo.getTipo());
                    cda.cargarCALIFICACIONES(vista.tbl_cuadrocalificacionbes, emode);// saca  la notas  por materias
                    String promedio=(in.devolverundato("SELECT AVG(promedioanual) from  notacuantitativa, matricula WHERE matricula.codigomatricula=notacuantitativa.codigomatricula and  matricula.cedulaestudiante='" + emode.getCedula() + "' and matricula.codigoperiodo in  (SELECT max(codigoperiodo) from periodoacademico)"));
                    vista.txt_promedio.setText(promedio.substring(0,4));
                           
                    vista.txt_comportamiento.setText((in.devolverundato("SELECT comportamiento from  notacuantitativa, matricula WHERE matricula.codigomatricula=notacuantitativa.codigomatricula and  matricula.cedulaestudiante='" + emode.getCedula() + "' and matricula.codigoperiodo in  (SELECT max(codigoperiodo) from periodoacademico)")));
                ;
                }

            }

        } catch (Exception ex) {
            System.out.println(" error encontrado" + ex.getMessage());
        }

    }

}
