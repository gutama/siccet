package controlador;

import bases.incrementocodigo;
import bases.mimagenes;
import bases.validaciones;
import dao.gradoDao;
import dao.materiaDao;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import modelo.diseñotablas.Render;
import modelo.diseñotablas.centrarcolumnas;
import modelo.grado;
import modelo.gradoMateria;
import modelo.materia;
import vista.vcrearmallacademica;

/**
 *
 * @author Legion
 */
public class crearmallacontroller implements ActionListener {

    vcrearmallacademica vista;
    gradoDao gdao = new gradoDao();
    materiaDao mdao = new materiaDao();
    materia ma = new materia();
    gradoMateria m = new gradoMateria();
    incrementocodigo in = new incrementocodigo();
    grado gmodel = new grado();
    validaciones v = new validaciones();
    centrarcolumnas cetrar = new centrarcolumnas();

    public crearmallacontroller(vcrearmallacademica vista) {
        this.vista = vista;
        vista.btn_guardar.addActionListener(this);
        vista.jc_listagrados.addActionListener(this);
        vista.cbSeleccionar.addActionListener(this);
        //cagar datos de la base
        cargarTabla();
        gdao.listargradosconcatenados(vista.jc_listagrados);

    }

    private void cargarTabla() {
        vista.tbl_materias.setDefaultRenderer(Object.class, new Render());

        String[] columnas = new String[]{"CODIGO", "MATERIAS", " NUM.HORAS", "SELECCIONE"};
        boolean[] editable = {false, false, true, true};
        Class[] types = new Class[]{java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class};
        DefaultTableModel mModeloTablaProd = new DefaultTableModel(columnas, 0) {

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int row, int column) {
                return editable[column];
            }
        };
        ArrayList<materia> lis = mdao.listar_materias();
        if (lis.size() > 0) {
            Object filas[] = new Object[columnas.length];
            for (int j = 0; j < lis.size(); j++) {
                ma = lis.get(j);
                filas[0] = ma.getCodigomateria();
                filas[1] = ma.getNombre();
                filas[2] =0;
                filas[3] = vista.cbSeleccionar.isSelected();
                mModeloTablaProd.addRow(filas);
            }
        }
        vista.tbl_materias.setModel(mModeloTablaProd);
    }

    public void iniciarvista(String fondos, mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
        i.insertarimagenesetiquetas(vista.fondo, fondos);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {

            Object o = e.getSource();
            if (o.equals(vista.cbSeleccionar)) {
                if (vista.cbSeleccionar.isSelected()) {
                    vista.cbSeleccionar.setText("Desseleccionar todos");
                } else {
                    vista.cbSeleccionar.setText("Seleccionar todos");
                }
                cargarTabla();

            }
            if (o.equals(vista.btn_guardar)) {
                String grado = (String) vista.jc_listagrados.getSelectedItem();
                String tipo = grado.substring(8);
                if (tipo.equals("EGB")) {
                    m.setCodigogrado(Integer.parseInt(in.devolverundato("select codigogrado  from grado  where nombre='" + grado.substring(0, 8) + "' and tipo='" + tipo + "'")));
                } else {
                    tipo = grado.substring(9);
                    m.setCodigogrado(Integer.parseInt(in.devolverundato("select codigogrado  from grado  where nombre='" + grado.substring(0, 8) + "' and tipo='" + tipo + "'")));
                }
                 for (int i = 0; i < vista.tbl_materias.getRowCount(); i++) {
            m.setCodigomateria(Integer.parseInt(vista.tbl_materias.getValueAt(i,0).toString()));//codigo de la materia
             m.setNumerohoras(Integer.parseInt(vista.tbl_materias.getValueAt(i,2).toString()));//numero de horas
              String seleccion = "" + vista.tbl_materias.getValueAt(i,3); // Seleccionar
            if(seleccion.equals("true")){  
            gdao.guardarmallaacademica(m);
            }
     
            }
                System.out.println("codigo" + m.getCodigogrado());
            }
        } catch (Exception ex) {
            System.out.println("problema " + ex.getMessage());
        }

    }

}
