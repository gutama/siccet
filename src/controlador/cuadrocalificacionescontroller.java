package controlador;

import bases.incrementocodigo;
import bases.mimagenes;
import bases.validaciones;
import dao.calificacionesDao;
import dao.estudianteDao;
import dao.matriculaDao;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.estudiante;
import modelo.matricula;
import vista.vvercalificaciones;

/**
 *
 * @author Legion
 */
public class cuadrocalificacionescontroller implements ActionListener {

    vvercalificaciones vista;
    estudiante es = new estudiante();
    estudianteDao edao = new estudianteDao();
    calificacionesDao cdao = new calificacionesDao();
    matricula modelo = new matricula();
    matriculaDao mdao = new matriculaDao();
    validaciones v = new validaciones();
    incrementocodigo in = new incrementocodigo();
    DefaultTableModel columna = new DefaultTableModel();

    public cuadrocalificacionescontroller(vvercalificaciones vista) {
        this.vista = vista;
        validarcampos();
        cabecera(vista.tbl_lista);
        // poner en  modo escucha  los  botones
        vista.btn_buscar.addActionListener(this);
        vista.jc_listagra.addActionListener(this);
    }

    public void validarcampos() {
        v.limitarcaracteres(vista.txt_cedula, 10);
        v.validarnumeros(vista.txt_cedula);
    }

    public void iniciarvista(String fondos, mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
        i.insertarimagenesetiquetas(vista.fondo, fondos);
    }

    public void cabecera(JTable tabla) {
        columna.addColumn("AREA");
        columna.addColumn("ASIGNATURA");
        columna.addColumn("QUIMESTRE 1");
        columna.addColumn("QUIMESTRE 2");
        columna.addColumn("PROMEDIO");
        columna.addColumn("CALIFICACION" + "CUALITATIVA");
        tabla.setModel(columna);
    }

    public void eliminarfilas(DefaultTableModel model) {
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
             Object o = e.getSource();
        if (o.equals(vista.btn_buscar)) {
            if (vista.txt_cedula.getText().trim().length() != 0) {
                es.setCedula(vista.txt_cedula.getText());
                if (edao.buscar(es) == true && v.validacioncedula(es.getCedula())) {
                    vista.txt_nombrecom.setText(es.getNombre() + " " + es.getApellido());
                    modelo.setCedulaestudianteFK(vista.txt_cedula.getText());
                    mdao.listargradoscursadosporelestudiante(vista.jc_listagra, modelo);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Rellene el campo cédula", "!Advertencia!", JOptionPane.WARNING_MESSAGE);
            }
        }
        if (o.equals(vista.jc_listagra)) {
            String grado = (String) vista.jc_listagra.getSelectedItem();
            String tipo = grado.substring(8);
            if (tipo.equals("EGB")) {
                modelo.setCodigocursoFk(Integer.parseInt(in.devolverundato("select codigocurso  from vista_cursos  where nombre='" + grado.substring(0, 8) + "' and tipo='" + tipo + "'")));
                int codigomatricula = Integer.parseInt(in.devolverundato("select count(notacuantitativa.codigomatricula) from notacuantitativa,matricula where matricula.codigocurso=" + modelo.getCodigocursoFk() + " and matricula.codigomatricula=notacuantitativa.codigomatricula"));
                if (codigomatricula > 0) {
                    cdao.vercalificaciones(vista.tbl_lista, modelo, columna);
                } else if (codigomatricula == 0) {
                    eliminarfilas(columna);
                    JOptionPane.showMessageDialog(null, "No existe calificaciones", "", JOptionPane.INFORMATION_MESSAGE);
                }
            } else {
                tipo = grado.substring(9);
                modelo.setCodigocursoFk(Integer.parseInt(in.devolverundato("select codigocurso  from vista_cursos  where nombre='" + grado.substring(0, 8) + "' and tipo='" + tipo + "'")));
                int codigomatricula = Integer.parseInt(in.devolverundato("select count(notacuantitativa.codigomatricula) from notacuantitativa,matricula where matricula.codigocurso=" + modelo.getCodigocursoFk() + " and matricula.codigomatricula=notacuantitativa.codigomatricula"));
                if (codigomatricula > 0) {
                    cdao.vercalificaciones(vista.tbl_lista, modelo, columna);
                } else 
                    if (codigomatricula == 0) {
                    eliminarfilas(columna);
                    JOptionPane.showMessageDialog(null, "No existe calificaciones", "", JOptionPane.INFORMATION_MESSAGE);

                }

            }
        }
        } catch (Exception ex) {
            System.out.println(" error"+ex.getMessage());
        }
       

    }

}
