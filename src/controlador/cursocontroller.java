package controlador;

import bases.incrementocodigo;
import bases.mimagenes;
import java.awt.event.*;
import javax.swing.*;
import dao.*;
import javax.swing.table.DefaultTableModel;
import modelo.*;
import vista.vcurso;

/**
 *
 * @author Mesias
 */
public class cursocontroller implements ActionListener {

    vcurso vista;
    grado g = new grado();
    curso au = new curso();
    gradoDao sg = new gradoDao();
    aulaDao sb = new aulaDao();
    paraleloDao pdao = new paraleloDao();
    jornadaDao jdaDao = new jornadaDao();
    incrementocodigo in = new incrementocodigo();
    DefaultTableModel columna;

    public cursocontroller(vcurso vista) {
        this.vista = vista;
        //poner en  modo escucha los botones
        this.vista.btn_guardar.addActionListener(this);
        //cargar los datos de la  base

        pdao.listarparalelo(vista.jc_paralelo);
        jdaDao.listarjornadas(vista.jc_jornada);
        sg.listargradosconcatenados(vista.jc_nombre);
        sg.matricularenuncurso(vista.tb_listagrados);
    }

 public void iniciarvista(String fondos,mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
  i.insertarimagenesetiquetas(vista.fondo,fondos);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Object o = e.getSource();

            if (o.equals(vista.btn_guardar)) {
                String grado = (String) vista.jc_nombre.getSelectedItem();
                String tipo = grado.substring(8);
                au.setCodigoparaleloFK(Integer.parseInt(in.devolverundato("select codigoparalelo from paralelo where paralelo='" + vista.jc_paralelo.getSelectedItem() + "'")));
                au.setCodigojornadaFK(Integer.parseInt(in.devolverundato("select codigojornada from jornada  where jornada='" + vista.jc_jornada.getSelectedItem() + "'")));
                if (tipo.equals("EGB")) {
                    au.setCodigogradoFK(Integer.parseInt(in.devolverundato("select codigogrado  from grado  where nombre='" + grado.substring(0, 8) + "' and tipo='" + tipo + "'")));

                } else {
                    tipo = grado.substring(9);
                    au.setCodigogradoFK(Integer.parseInt(in.devolverundato("select codigogrado  from grado  where nombre='" + grado.substring(0, 8) + "' and tipo='" + tipo + "'")));
                }
                if (sg.guadarCursos(au) == true);
                JOptionPane.showMessageDialog(null, "guardado exitosamente", "", JOptionPane.INFORMATION_MESSAGE);
                sg.guardar(g);
                sg.cargargrados(vista.tb_listagrados);
            }

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    }
}
