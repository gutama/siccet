package controlador;

import bases.incrementocodigo;
import bases.mimagenes;
import bases.validaciones;
import dao.docenteDao;
import dao.documentoDao;
import java.awt.Desktop;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import modelo.*;
import vista.vcalificaciones;
import vista.vdocente;
import vista.vinventario;
import vista.vlistarinventario;

/**
 *
 * @author Legion
 */
public class docentecontroller implements ActionListener {

    docente modelo = new docente();
    docenteDao bs = new docenteDao();
    documentoDao dob = new documentoDao();
    docenteDocumento docedo = new docenteDocumento();
    documento model = new documento();
    vdocente vista;
    validaciones v = new validaciones();
    incrementocodigo in = new incrementocodigo();
    String rutaarcivo = "";
    ImageIcon icono = null;
       mimagenes i=new mimagenes();
       String fondo = "/imagenes/fondo 3.jpg";
    public docentecontroller(vdocente vista) {
        this.vista = vista;
        validarcampos();
        vista.btn_guardar.addActionListener(this);
        vista.btn_modificar.addActionListener(this);
        vista.btn_cancelar.addActionListener(this);
        vista.btn_eliminar.addActionListener(this);
        vista.seleccionararchivo.addActionListener(this);
        vista.btn_mihojadevida.addActionListener(this);

    }

   public void iniciarvista() {
        vista.setLocationRelativeTo(null);
       
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
 
    }

    public void validarcampos() {
        //validar campos de texto.
        v.limitarcaracteres(vista.txt_cedula, 10);
        v.validarnumeros(vista.txt_cedula);
        v.ingresarsolomayusculas(vista.txt_nombred);
        v.ingresarsolomayusculas(vista.txt_apellidod);
        v.limitarcaracteres(vista.txt_telefonod, 10);
        v.validarnumeros(vista.txt_telefonod);
    }

    public void limpiarcajas() {
        vista.txt_cedula.setText("");
        vista.txt_nombred.setText("");
        vista.txt_apellidod.setText("");
        vista.txt_correod.setText("");
        vista.txt_telefonod.setText("");
    }

    public int guardar(String nombre, File ruta) throws IOException {
        try {
            model.setNombrepdf(nombre);
            byte[] docu = new byte[(int) ruta.length()];
            InputStream z = new FileInputStream(ruta);
            z.read(docu);
            model.setArchivopdf(docu);
        } catch (FileNotFoundException ex) {
            model.setArchivopdf(null);
        }
        return dob.agregar_pdfVO(model);
    }




    public void seleccionararchivo() {
        JFileChooser j = new JFileChooser();
        FileNameExtensionFilter fi = new FileNameExtensionFilter("Archivos pdf", "pdf");
        j.setFileFilter(fi);
        int se = j.showOpenDialog(vista);
        if (se == 0) {
            vista.seleccionararchivo.setText("" + j.getSelectedFile().getName());
            rutaarcivo = j.getSelectedFile().getAbsolutePath();
        }
    }


    public Image get_Image(String ruta) {
        ImageIcon a = new ImageIcon(getClass().getResource(ruta));
        Image bo = a.getImage();
        return bo;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        try {
        
            if (o.equals(vista.btn_guardar)) {
                //datos para guardar en  la  tabla  docente
                modelo.setCedula(vista.txt_cedula.getText());
                modelo.setNombre(vista.txt_nombred.getText());
                modelo.setApellido(vista.txt_apellidod.getText());
                modelo.setTelefono(vista.txt_telefonod.getText());
                modelo.setCorreo(vista.txt_correod.getText());
                //datos  para guardar en la  tabla documento;
                File ruta = new File(rutaarcivo);
                model.setNombrepdf("hoja de vida");
                if (modelo.getCedula().trim().length() != 0 && v.validacioncedula(modelo.getCedula()) == true && rutaarcivo.trim().length() != 0 && bs.guardar(modelo) == true) {
                    // datos para  giardar  en  la tabla docentedocumento
                    docedo.setCeduladocente(vista.txt_cedula.getText());
                    docedo.setCodigodocumento(guardar(model.getNombrepdf(), ruta));
                    bs.guardardocumentodocente(docedo);
                    JOptionPane.showMessageDialog(null, "guardado", "!Mensaje!", JOptionPane.INFORMATION_MESSAGE);
                    rutaarcivo = "";
                    this.limpiarcajas();//metodo que limpia  las cajas de texto
                } else {
                    JOptionPane.showMessageDialog(null, "Rellene todos los campos", "!Advertencia!", JOptionPane.WARNING_MESSAGE);
                }
            }
            if (o.equals(vista.seleccionararchivo)) {
                seleccionararchivo();
            }
            if (o.equals(vista.btn_modificar)) {//boton  modificar datos del codente
                if (vista.txt_cedula.getText().trim().length() != 0) {
                    modelo.setCedula(vista.txt_cedula.getText());
                    modelo.setNombre(vista.txt_nombred.getText());
                    modelo.setApellido(vista.txt_apellidod.getText());
                    modelo.setTelefono(vista.txt_telefonod.getText());
                    modelo.setCorreo(vista.txt_correod.getText());
                    if(bs.modificar(modelo)==true){
                    JOptionPane.showMessageDialog(null, "Modificado exitosamente", "", JOptionPane.INFORMATION_MESSAGE);
                    this.limpiarcajas();
                    }
                    
                } else {
                    JOptionPane.showMessageDialog(null, "Rellene el campo cédula", "!Advertencia!", JOptionPane.WARNING_MESSAGE);
                }
            }
            if (o.equals(vista.btn_cancelar)) {
                if (vista.txt_cedula.getText().trim().length() != 0) {
                    modelo.setCedula(vista.txt_cedula.getText());
                    bs.buscar(modelo);
                    dob.recuperararchivo(vista.txt_cedula.getText(),model);
                    if (get_Image("/imagenes/pdf.png") != null) {
                        icono = new ImageIcon(get_Image("/imagenes/pdf.png"));
                    }
                    if (model.getArchivopdf() != null) {
                        vista.btn_mihojadevida.setIcon(icono);
                    }               
                    vista.txt_cedula.setText(modelo.getCedula());
                    vista.txt_nombred.setText(modelo.getNombre());
                    vista.txt_apellidod.setText(modelo.getApellido());
                    vista.txt_telefonod.setText(modelo.getTelefono());
                    vista.txt_correod.setText(modelo.getCorreo());
                } else {
                    JOptionPane.showMessageDialog(null, "Rellene el campo cédula", "!Advertencia!", JOptionPane.WARNING_MESSAGE);
                }
            }
             if (o.equals(vista.btn_eliminar)) {
                if (vista.txt_cedula.getText().trim().length() != 0) {
                    modelo.setCedula(vista.txt_cedula.getText());
                    if (bs.eliminar(modelo.getCedula())) {
                        JOptionPane.showMessageDialog(null, "Docente eliminado");
                        limpiarcajas();
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Rellene el campo cédula", "!Advertencia!", JOptionPane.WARNING_MESSAGE);
                }
            }
            if (o.equals(vista.btn_mihojadevida)) {
                try {
                    dob.recuperararchivo(vista.txt_cedula.getText(),model);
                    model.setArchivopdf(model.getArchivopdf());
                    dob.ejecutar_archivo(model);
                    Desktop.getDesktop().open(new File("new.pdf"));// abrir documento  tipo pdf en la  pagina de  escritorio
                } catch (Exception ex) {
                }
            }
        } catch (Exception ex) {
            System.out.println(" el error es " + ex.getMessage());
        }

    }

}
