package controlador;

import bases.mimagenes;
import bases.validaciones;
import com.placeholder.PlaceHolder;
import dao.estudianteDao;
import dao.representanteDao;
import java.awt.event.*;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import modelo.estudiante;
import modelo.familiarcercano;
import modelo.representante;
import vista.vrestudiante;

/**
 *
 * @author Legion
 */
public class estudiantecontroller implements ActionListener {

    estudiante modelo = new estudiante();
    estudianteDao bs = new estudianteDao();
    vrestudiante vista;
    validaciones v = new validaciones();
    DefaultTableModel columnas = new DefaultTableModel();
    familiarcercano fmodelo = new familiarcercano();
    representante rmodelo = new representante();
    representanteDao rdao = new representanteDao();

    public estudiantecontroller(vrestudiante vista) {
        this.vista = vista;
        vista.btn_guardar.addActionListener(this);
        vista.btn_modificar.addActionListener(this);
        vista.btn_eliminar.addActionListener(this);
        vista.btn_buscar.addActionListener(this);
        //validar campos
        validarcampos();
        //funcionalidad de botones
        vista.btn_eliminar.setEnabled(false);
        vista.btn_modificar.setEnabled(false);
        vista.btn_guardar.setEnabled(true);

    }

    public void iniciarvista(String fondos, mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
        i.insertarimagenesetiquetas(vista.fondo, fondos);
    }

    public void validarcampos() {
        //validar campos de texto.
        v.limitarcaracteres(vista.txt_cedula, 10);
        v.limitarcaracteres(vista.txt_nombre, 20);
        v.limitarcaracteres(vista.txt_apellido, 20);
        v.validarnumeros(vista.txt_cedula);
        v.ingresarsolomayusculas(vista.txt_nombre);
        v.ingresarsolomayusculas(vista.txt_apellido);
        v.ingresarsolomayusculas(vista.txt_apellidorepre);
        v.ingresarsolomayusculas(vista.txt_nombrerepre);
        v.validarnumeros(vista.txt_cedularepre);
        v.limitarcaracteres(vista.txt_cedularepre,10);
    }

    public void limpiarcajas() {
        vista.txt_cedula.setText("");
        vista.txt_nombre.setText("");
        vista.txt_apellido.setText("");
        vista.txt_correo.setText("");
        vista.txt_telefono.setText("");
        vista.txt_fechanacimiento.setText("");
        vista.txt_apellidorepre.setText(" ");
        vista.txt_cedularepre.setText("");
        vista.txt_correorepre.setText("");
        vista.txt_direccion1repre.setText("");
        vista.txt_direccionfami.setText(" ");
        vista.txt_nombrerepre.setText("");
        vista.txt_telefonofami.setText("");
    }

    public void cabeceratabla() {
        columnas.addColumn("CEDULA");
        columnas.addColumn("NOMBRES ");
        columnas.addColumn("APELLIDOS");

    }

    private void filtrodatos(String consulta, JTable jtableBuscar) {
        columnas = (DefaultTableModel) jtableBuscar.getModel();
        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<>(columnas);
        jtableBuscar.setRowSorter(tr);
        tr.setRowFilter(RowFilter.regexFilter(consulta));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        try {
            if (o.equals(vista.btn_modificar)) {
                modelo.setCedula(vista.txt_cedula.getText());
                modelo.setNombre(vista.txt_nombre.getText());
                modelo.setApellido(vista.txt_apellido.getText());
                modelo.setTelefono(vista.txt_telefono.getText());
                modelo.setCorreo(vista.txt_correo.getText());
                modelo.setFechanacimiento(vista.txt_fechanacimiento.getText());
                rmodelo.setNombre(vista.txt_nombrerepre.getText());
                rmodelo.setApellido(vista.txt_apellidorepre.getText());
                rmodelo.setDireccion(vista.txt_direccion1repre.getText());
                rmodelo.setCorreo(vista.txt_correorepre.getText());
                rmodelo.setCedula(vista.txt_cedularepre.getText());
                 fmodelo.setTelefono(vista.txt_telefonofami.getText());
                    fmodelo.setDireccion(vista.txt_direccionfami.getText());
                    fmodelo.setCodigofamiliar(fmodelo.getCodigofamiliar());
                if (bs.modificar(modelo) == true && rdao.modificar(rmodelo)==true && rdao.modificarfamilar(fmodelo)==true) {
                    JOptionPane.showMessageDialog(null, "Modificado Exitosamente");
                    this.limpiarcajas();
                }
                //funcionalidad de botones
                vista.btn_eliminar.setEnabled(false);
                vista.btn_modificar.setEnabled(false);
                vista.btn_guardar.setEnabled(true);
            }

            if (o.equals(vista.btn_guardar)) {
                modelo.setCedularepresentanteFK(vista.txt_cedularepre.getText());
                modelo.setNombre(vista.txt_nombre.getText());
                modelo.setApellido(vista.txt_apellido.getText());
                modelo.setTelefono(vista.txt_telefono.getText());
                modelo.setCorreo(vista.txt_correo.getText());
                modelo.setFechanacimiento(vista.txt_fechanacimiento.getText());
                rmodelo.setNombre(vista.txt_nombrerepre.getText());
                rmodelo.setApellido(vista.txt_apellidorepre.getText());
                rmodelo.setDireccion(vista.txt_direccion1repre.getText());
                rmodelo.setCorreo(vista.txt_correorepre.getText());
                modelo.setCedula(vista.txt_cedula.getText());
                rmodelo.setCedula(vista.txt_cedularepre.getText());
                if (modelo.getCedula().trim().length() != 0 && v.validacioncedula(modelo.getCedula()) == true && v.validacioncedula(rmodelo.getCedula()) == true) {
                    fmodelo.setTelefono(vista.txt_telefonofami.getText());
                    fmodelo.setDireccion(vista.txt_direccionfami.getText());
                    rmodelo.setCodigofamiliarFK(rdao.guardarparientefamiliar(fmodelo));
                    if (rmodelo.getCodigofamiliarFK() > 0) {
                        if (rdao.guardar(rmodelo) == true) {
                            if (bs.guardar(modelo) == true) {
                                JOptionPane.showMessageDialog(null, "guardado", "!Mensaje!", JOptionPane.INFORMATION_MESSAGE);
                                this.limpiarcajas();//metodo que limpia  las cajas de texto
                            }
                        }
                    }

                } else {
                    JOptionPane.showMessageDialog(null, "Rellene todos los campos", "!Advertencia!", JOptionPane.WARNING_MESSAGE);
                }
            }
            if (o.equals(vista.btn_eliminar)) {//botn para eliminar los datos del estudiante
                if (vista.txt_cedula.getText().trim().length() != 0) {
                    modelo.setCedula(vista.txt_cedula.getText());
                    if (bs.eliminar(modelo) == true) {
                        JOptionPane.showMessageDialog(null, "Eliminado Exitosamente");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Rellene el campo cédula", "!Advertencia!", JOptionPane.WARNING_MESSAGE);
                }
            }
            if (o.equals(vista.btn_buscar)) {
                if (vista.txt_cedula.getText().trim().length() != 0) {
                    modelo.setCedula(vista.txt_cedula.getText());
                    if (bs.buscarestudiante(modelo, rmodelo,fmodelo) == true) {
                        vista.txt_nombre.setText(modelo.getNombre());
                        vista.txt_apellido.setText(modelo.getApellido());
                        vista.txt_fechanacimiento.setText(modelo.getFechanacimiento());
                        vista.txt_telefono.setText(modelo.getTelefono());
                        vista.txt_correo.setText(modelo.getCorreo());
                        vista.txt_cedularepre.setText(rmodelo.getCedula());
                        vista.txt_nombrerepre.setText(rmodelo.getNombre());
                        vista.txt_apellidorepre.setText(rmodelo.getApellido());
                        vista.txt_correorepre.setText(rmodelo.getCorreo());
                        vista.txt_direccion1repre.setText(rmodelo.getDireccion());
                        vista.txt_direccionfami.setText(fmodelo.getDireccion());
                        vista.txt_telefonofami.setText(fmodelo.getTelefono());
                        int codigofa=fmodelo.getCodigofamiliar();
                        fmodelo.setCodigofamiliar(codigofa);
                        vista.btn_modificar.setEnabled(true);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Rellene el campo cédula", "!Advertencia!", JOptionPane.WARNING_MESSAGE);
                }
            }

        } catch (Exception ex) {
            System.out.println(" el error es" + ex.getMessage());
        }

    }

}
