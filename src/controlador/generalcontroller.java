package controlador;

import bases.incrementocodigo;
import bases.mimagenes;
import bases.validaciones;
import dao.calicualitativaDao;
import dao.periodoacademicoDao;
import java.awt.event.*;
import javax.swing.*;
import modelo.caliCualitativa;
import modelo.periodoAcademico;
import vista.vrgeneral;

/**
 *
 * @author Mesias
 */
public class generalcontroller implements ActionListener, MouseListener {

    vrgeneral vista;
    caliCualitativa model=new caliCualitativa();
    periodoAcademico modelo=new periodoAcademico();
    periodoacademicoDao bs=new periodoacademicoDao();
    calicualitativaDao sb=new calicualitativaDao();
    incrementocodigo in=new incrementocodigo();
    public generalcontroller(vrgeneral vista) {
        this.vista = vista;
        vista.btn_guardaraño.addActionListener(this);
        vista.btn_guardarnota.addActionListener(this);
        bs.listarperiodos(vista.jc_listaperiodosacademicos);
        sb.cargarequvalentes(vista.tbl_listarequvalentes);
    }

  public void iniciarvista(String fondos,mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
  i.insertarimagenesetiquetas(vista.fondo,fondos);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        Object o=e.getSource();
        if(o.equals(vista.btn_guardaraño)){
        modelo.setPeriodo(vista.txt_periodo.getText());
        if(bs.guardar(modelo)==true){
          JOptionPane.showMessageDialog(null,"guardado exitosamente","",JOptionPane.INFORMATION_MESSAGE);
          bs.listarperiodos(vista.jc_listaperiodosacademicos);
        }
        }
         if(o.equals(vista.btn_guardarnota)){
        model.setEquivalente((String) vista.jc_notacualitativa.getSelectedItem());
        if(sb.guardar(model)==true){
          JOptionPane.showMessageDialog(null,"guardado exitosamente","",JOptionPane.INFORMATION_MESSAGE);
        }
        }
          if(o.equals(vista.btn_modificaraño)){
            modelo.setPeriodo(vista.txt_periodo.getSelectedText());
        }
           if(o.equals(vista.btn_modificarnotacualitativa)){
        
        }
        
        
    }
    @Override
    public void mouseClicked(MouseEvent e) {
//        int sele = vista.tbl_listamaterias.getSelectedRow();
//        vista.txt_nommateria.setText(vista.tbl_listamaterias.getValueAt(sele, 1).toString()); //eviar datos a un jtextfield     
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

   
}
