package controlador;

import bases.incrementocodigo;
import bases.mimagenes;
import java.awt.event.*;
import javax.swing.*;
import dao.*;
import javax.swing.table.DefaultTableModel;
import modelo.*;
import vista.vcurso;
import vista.vgrado;

/**
 *
 * @author Mesias
 */
public class gradocontroller implements ActionListener{

    vgrado vista;
    grado g = new grado();
    jornada j=new jornada();
    paralelo p=new paralelo();
    gradoDao sg = new gradoDao();
    jornadaDao jdao=new jornadaDao();
    paraleloDao pdao=new paraleloDao();
    public gradocontroller(vgrado vista) {
        this.vista = vista;
        //poner en  modo escucha los botones
        this.vista.btn_guardar.addActionListener(this);
           this.vista.btn_guardarjornada.addActionListener(this);
              this.vista.btn_guadarparalelo.addActionListener(this);
    }

 public void iniciarvista(String fondos,mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
  i.insertarimagenesetiquetas(vista.fondo,fondos);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Object o = e.getSource();
       
            if (o.equals(vista.btn_guardar)) {// boton para guardar l las  materias
                g.setNombre((String) vista.jc_nombre.getSelectedItem());
                g.setTipo((String) vista.jc_tipo.getSelectedItem());
                sg.guardar(g);
            }
            if(o.equals(vista.btn_guardarjornada)){
            j.setJornada((String) vista.jc_jornada.getSelectedItem());
            jdao.guardar(j);
            }
            if(o.equals(vista.btn_guadarparalelo)){
            p.setParalelo((String) vista.jc_paralelo.getSelectedItem());
            pdao.guardar(p);
            }
               
            
            
               
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    }
}
