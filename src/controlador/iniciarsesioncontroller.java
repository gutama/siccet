package controlador;

import bases.incrementocodigo;
import bases.mimagenes;
import bases.validaciones;
import dao.personaDao;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Action;
import javax.swing.JOptionPane;
import modelo.rol;
import modelo.usuario;
import vista.menuprincipal;
import vista.menuprincipaldocente;
import vista.menuprincipalestudiante;
import vista.registrarroles;
import vista.vdocente;
import vista.viniciosecion;
import vista.vregistrocuenta;

/**
 *
 * @author Legion
 */
public class iniciarsesioncontroller implements ActionListener {

    viniciosecion vista;
    mimagenes i = new mimagenes();
    usuario u = new usuario();
    rol r = new rol();
    personaDao pda = new personaDao();
    validaciones v = new validaciones();
    String fondo = "/imagenes/fondo 3.jpg";

    public iniciarsesioncontroller(viniciosecion vista) {
        this.vista = vista;
        vista.btn_iniciarsecion.addActionListener(this);
        vista.txt_registrar.addActionListener(this);
    }

    public void iniciarvista(String fondos, mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
     i.logoventanas(vista, "./imagenes/escudocolegio.jpg");
        i.insertarimagenesetiquetas(vista.fondo, fondos);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Object o = e.getSource();
            String secretKey = " PROGRAMADORES GUTAMA Y TADAY";
            if (o.equals(vista.btn_iniciarsecion)) {
                u.setCedulaFk(vista.jTextField1.getText());
                if (pda.buscar(u, r) == true) {
                    if (v.desencriptar(secretKey, u.getClave()).equals(vista.txt_contraseña.getText()) && u.getCedulaFk().equals(vista.jTextField1.getText())) {
                        if (r.getRol().equals("ADMINISTRADOR")) {
                            registrarroles vi = new registrarroles(vista, true);
                            registrarRolController controlador = new registrarRolController(vi);
                            controlador.iniciarvista(fondo, i);
                        }
                        if (r.getRol().equals(" RECTOR")) {
                            menuprincipal v = new menuprincipal();
                            menucontroller controlador = new menucontroller(v);
                            controlador.iniciarvista();
                        }
                        if (r.getRol().equals("SECRETARIA")) {
                             menuprincipal v = new menuprincipal();
                            menucontroller controlador = new menucontroller(v);
                            controlador.iniciarvista();
                        }
                        if (r.getRol().equals(" INSPECTOR")) {
                             menuprincipal v = new menuprincipal();
                            menucontroller controlador = new menucontroller(v);
                            controlador.iniciarvista();
                        }
                        if (r.getRol().equals(" DOCENTE")) {
                            menuprincipaldocente v= new menuprincipaldocente();
                            menudocentecontroller controlador=new menudocentecontroller(v);
                            controlador.iniciarvista();
                        }
                         if (r.getRol().equals(" ESTUDIANTE")) {
                             menuprincipalestudiante v = new menuprincipalestudiante();
                            menuestudiantecontroller controlador = new menuestudiantecontroller(v);
                            controlador.iniciarvista();
                        }
                        
                    } else {
                        JOptionPane.showMessageDialog(null, "contraseña incorrecta", "", JOptionPane.ERROR_MESSAGE);
                        vista.txt_contraseña.setText("");
                        vista.jTextField1.setText("");
                    }
                }
            }
            if (o.equals(vista.txt_registrar)) {
                vregistrocuenta v = new vregistrocuenta(vista, true);
                registrarcuentacontroller controlador = new registrarcuentacontroller(v);
                controlador.iniciarvista(fondo, i);
            }
        } catch (Exception ec) {
        }

    }

}
