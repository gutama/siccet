package controlador;

import java.awt.event.*;
import bases.mimagenes;
import bases.validaciones;
import dao.institucionDao;
import javax.swing.JOptionPane;
import modelo.institucion;
import vista.vinstitucion;

public class institucionController implements ActionListener {

    vinstitucion vista;
    institucion  modelo= new institucion();
    institucionDao sgb= new institucionDao();
   
    validaciones v = new validaciones();

    public institucionController(vinstitucion vista) {
        this.vista = vista;
        this.modelo = modelo;
        this.sgb = sgb;
        vista.guardar.addActionListener(this);
        vista.modificar.addActionListener(this);
        vista.eliminar.addActionListener(this);
        vista.buscar.addActionListener(this);
        validarcampos();
    }

   public void iniciarvista(String fondos,mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
  i.insertarimagenesetiquetas(vista.fondo,fondos);
    }
    public  void  validarcampos(){
    //validacioon de campos
        v.ingresarsolomayusculas(vista.nombreE);
        v.ingresarsolomayusculas(vista.sectorE);
        v.ingresarsolomayusculas(vista.sectorE);
        v.limitarcaracteres(vista.telefonoE, 10);
        v.validarnumeros(vista.telefonoE);
    
    }

    public void vaciar() {
        vista.CodigoE.setText("");
        vista.nombreE.setText("");
        vista.telefonoE.setText("");
        vista.sectorE.setText("");
        vista.correo.setText("");
        vista.regimenE.setText("");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        if (o.equals(vista.modificar)) {
            modelo.setCodigo(vista.CodigoE.getText());
            if(modelo.getCodigo().length()>0){
            modelo.setNombre(vista.nombreE.getText());
            modelo.setTelefono(vista.telefonoE.getText());
            modelo.setSector(vista.sectorE.getText());
            modelo.setCorreo(vista.correo.getText());
            modelo.setRegimen(vista.regimenE.getText());
            sgb.modificar(modelo);
            vaciar();
            }else
             JOptionPane.showMessageDialog(null,"Ingrese el codigo AMIE","!Advertencia!",JOptionPane.WARNING_MESSAGE);    
        } 
        if (o.equals(vista.buscar)) {
            modelo.setCodigo(vista.CodigoE.getText());
            if (modelo.getCodigo().length()>0 && sgb.buscar(modelo) == true) {
                vista.CodigoE.setText(modelo.getCodigo());
                vista.nombreE.setText(modelo.getNombre());
                vista.telefonoE.setText(modelo.getTelefono());
                vista.sectorE.setText(modelo.getSector());
                vista.correo.setText(modelo.getCorreo());
                vista.regimenE.setText(modelo.getRegimen());
            }else
            JOptionPane.showMessageDialog(null,"Ingrese el codigo AMIE","!Advertencia!",JOptionPane.WARNING_MESSAGE);
        } 
        if (o.equals(vista.guardar)) {
            modelo.setCodigo(vista.CodigoE.getText());
            if(modelo.getCodigo().length()>0){
            modelo.setNombre(vista.nombreE.getText());
            modelo.setTelefono(vista.telefonoE.getText());
            modelo.setSector(vista.sectorE.getText());
            modelo.setCorreo(vista.correo.getText());
           modelo.setRegimen(vista.regimenE.getText());
            sgb.guardar(modelo);
            vaciar();
            }else
             JOptionPane.showMessageDialog(null,"Ingrese el codigo AMIE","!Advertencia!",JOptionPane.WARNING_MESSAGE);    
        } 
        if (o.equals(vista.eliminar)) {
            modelo.setCodigo(vista.CodigoE.getText());
            if(modelo.getCodigo().length()>0){
            sgb.eliminar(vista.CodigoE.getText());
            vaciar();
            }else
             JOptionPane.showMessageDialog(null,"Ingrese el codigo AMIE","!Advertencia!",JOptionPane.WARNING_MESSAGE);    
        }
    }
}
