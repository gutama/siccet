
package controlador;

import bases.incrementocodigo;
import bases.mimagenes;
import bases.validaciones;
import com.placeholder.PlaceHolder;
import dao.areadministrativaDao;
import dao.inventarioDao;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import modelo.areaAdministrativa;
import modelo.inventario;
import vista.vinventario;

/**
 *
 * @author Legion
 */
public class inventarioController implements ActionListener{
    vinventario vista;
    validaciones v=new validaciones();
    inventario imodelo=new inventario();
    areadministrativaDao adao=new areadministrativaDao();
    incrementocodigo in= new  incrementocodigo();
    inventarioDao idao=new inventarioDao();
   String ceduladocente;

    public inventarioController(vinventario vista, String ceduladocente) {
        this.vista = vista;
        this.ceduladocente = ceduladocente;
       // cargar datos de la base 
       adao.listarareas(vista.jc_area, ceduladocente);
       //poner en  modo escucha los  botones 
       vista.btn_guardar.addActionListener(this);
       //metodos para  validar campos 
       validarcampos();
       //uso del place horder
            PlaceHolder holde = new PlaceHolder(vista.txt_fechaingreso, "DD/MM/AAAA");
    }
   public  void validarcampos(){
   v.validarnumerosdecimales(vista.txt_costo);
   v.validarnumeros(vista.txt_cantidad);
   v.ingresarsolomayusculas(vista.txt_color);
   v.ingresarsolomayusculas(vista.txt_marca);
   v.ingresarsolomayusculas(vista.txt_material);
   v.ingresarsolomayusculas(vista.txt_tiporespaldo);
   v.ingresarsolomayusculas(vista.txt_descripcion);
   v.ingresarsolomayusculas(vista.txt_modelo);
   }
   
 public void iniciarvista(String fondos,mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
  i.insertarimagenesetiquetas(vista.fondo,fondos);
    }
      public void cargarareaasignada(String cedu){
      
      }
    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Object o=e.getSource();
            if(o.equals(vista.btn_guardar)){
            imodelo.setIdentificador(vista.txt_indentificador.getText());
            imodelo.setSerie(vista.txt_serie.getText());
            imodelo.setCodigoactual(vista.txt_codigoactual.getText());
            imodelo.setDescripcion(vista.txt_descripcion.getText());
            imodelo.setFechaingreso(vista.txt_fechaingreso.getText());
            imodelo.setCantidad(Integer.parseInt(vista.txt_cantidad.getText()));
            imodelo.setOrigeningreso(vista.txt_origeningreso.getText());
            imodelo.setTiporespaldo(vista.txt_tiporespaldo.getText());
            imodelo.setCosto(Double.parseDouble(vista.txt_costo.getText()));
            imodelo.setModelo(vista.txt_modelo.getText());
            imodelo.setMarca(vista.txt_marca.getText());
            imodelo.setColor(vista.txt_color.getText());
            imodelo.setMaterial(vista.txt_material.getText());
            imodelo.setDimenciones(vista.txt_dimencion.getText());
            imodelo.setEstado((String) vista.jc_estado.getSelectedItem());
            imodelo.setCodigoareaFK(Integer.parseInt(in.devolverundato("select codigoarea from area where area='"+vista.jc_area.getSelectedItem()+"'")));
            if(idao.guardar(imodelo)==true){
                JOptionPane.showMessageDialog(null,"guardado exitosamente","",JOptionPane.INFORMATION_MESSAGE);
            }
            }
            
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        }
    
}
