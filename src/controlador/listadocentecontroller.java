package controlador;

import bases.incrementocodigo;
import bases.mimagenes;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import dao.docenteDao;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.File;
import java.io.FileOutputStream;

import javax.swing.table.DefaultTableModel;

import vista.vlistadocentes;

/**
 *
 * @author Legion
 */
public class listadocentecontroller implements ActionListener {

    vlistadocentes vista;
    DefaultTableModel columnas = new DefaultTableModel();
    incrementocodigo in = new incrementocodigo();
    docenteDao dao = new docenteDao();
    Document documento = new Document();

    public listadocentecontroller(vlistadocentes vista) {
        this.vista = vista;
        vista.btn_imprimir.addActionListener(this);
        dao.listardocentes(vista.tbl_listadocentes);

    }

    public void iniciarvista(String fondos, mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
        i.logoventanas(vista, "./imagenes/escudocolegio.jpg");
        i.insertarimagenesetiquetas(vista.fondo, fondos);
    }

    public void abrirpdf(String cetificado) {
        try {
            File path = new File(cetificado + ".pdf");
            Desktop.getDesktop().open(path);
        } catch (Exception e) {
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        Font fuente = new Font();
        fuente.setStyle(Font.BOLD);
        fuente.setSize(14);
        Font tipografia = new Font();
        tipografia.setSize(10);
        Font TITULO = new Font();
        TITULO.setSize(18);
        TITULO.setStyle(Font.BOLD);
        Font enca = new Font();
        enca.setSize(8);
        enca.setStyle(Font.NORMAL);

        try {
            if (o.equals(vista.btn_imprimir)) {
                // creacion del documento pdf
                FileOutputStream archivo;
                //encabesado del documento
                Image imagen = Image.getInstance("escudocolegio.jpg");
                imagen.setAlignment(Element.ALIGN_CENTER);
                imagen.scaleAbsolute(80, 40);
                imagen.setAbsolutePosition(250, 800);
                Paragraph titulo1 = new Paragraph("UNIDAD EDUCATIVA MOLLETURO", TITULO);
                titulo1.setAlignment(1);
                Paragraph direccion = new Paragraph("Molleturo - Cuenca - Azuay", enca);
                direccion.setAlignment(Element.ALIGN_CENTER);
                Paragraph datos = new Paragraph("Tlfno: 074045599 - E-mail: colegiomolleturo1981@hotmail.com ", enca);
                datos.setAlignment(Element.ALIGN_CENTER);
                Paragraph linea = new Paragraph("_____________________________________________________________________________________________________________________", enca);
                // cuerpo del documento
                Paragraph titulo = new Paragraph("LISTA  DE DOCENTES DE LA UNIDAD EDUCATIVA", fuente);

                try {
                    archivo = new FileOutputStream(vista.lb_nombrelis.getText() + ".pdf");
                    PdfWriter.getInstance(documento, archivo);
                    documento.open();
                    titulo.setAlignment(1);
                    documento.add(imagen);
                    documento.add(titulo1);
                    documento.add(direccion);
                    documento.add(datos);
                    documento.add(linea);
                    documento.add(Chunk.NEWLINE);
                    documento.add(titulo);
                    Paragraph tex = new Paragraph("PERIODO LECTIVO: " + in.devolverundato("select max(periodo)from periodoacademico"));

                    documento.add(tex);
                    documento.add(Chunk.NEWLINE);
                    // creo la tabla  en el  pdf
                    PdfPTable tabla = new PdfPTable(2);
                    // establecer ancho de las columnas
                    float[] medidaCeldas = {0.55f, 1.55f};
                    tabla.setWidths(medidaCeldas);
                    tabla.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                    PdfPCell cedula = new PdfPCell(new Phrase("CEDULA", fuente));
                    cedula.setBackgroundColor(BaseColor.WHITE);
                    cedula.setHorizontalAlignment(Element.ALIGN_CENTER);
                    PdfPCell nombres = new PdfPCell(new Phrase("APELLIDOS Y NOMBRES", fuente));
                    nombres.setBackgroundColor(BaseColor.WHITE);
                    nombres.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tabla.addCell(cedula);
                    tabla.addCell(nombres);

                    // agrego datos a la  tabla del pdf
                    for (int i = 0; i < vista.tbl_listadocentes.getRowCount(); i++) {
                        Paragraph cod = new Paragraph(vista.tbl_listadocentes.getValueAt(i, 0).toString(), tipografia);
                        Paragraph ced = new Paragraph(vista.tbl_listadocentes.getValueAt(i, 1).toString(), tipografia);
                        tabla.addCell(cod);
                        tabla.addCell(ced);
                    }
                    documento.add(tabla);
                    documento.close();
                } catch (Exception ex) {

                }
                abrirpdf(vista.lb_nombrelis.getText());
            }
        } catch (Exception ex) {
        }
    }

    public void eliminarfilas(DefaultTableModel model) {
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

}
