package controlador;

import bases.incrementocodigo;
import bases.mimagenes;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import dao.gradoDao;
import dao.matriculaDao;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileOutputStream;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import vista.vlistarepresentantes;

/**
 *
 * @author Legion
 */
public class listarepresentantescontroller implements ActionListener, MouseListener {

    vlistarepresentantes vista;
    gradoDao g = new gradoDao();
    matriculaDao mda = new matriculaDao();
    DefaultTableModel columnas = new DefaultTableModel();
    incrementocodigo in = new incrementocodigo();
    Document documento = new Document();

    public listarepresentantescontroller(vlistarepresentantes vista) {
        this.vista = vista;
        vista.tbl_grado.addMouseListener(this);
        vista.btn_imprimir.addActionListener(this);
        cabeceratabla();
        g.matricularenuncurso(vista.tbl_grado);
    }

    public void iniciarvista(String fondos, mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
        i.insertarimagenesetiquetas(vista.fondo, fondos);
    }

    public void cabeceratabla() {
        columnas.addColumn("CEDULA");
        columnas.addColumn("NOMBRES COMPLETOS");
        vista.tbl_listarepresentante.setModel(columnas);
    }

    public void abrirpdf(String cetificado) {
        try {
            File path = new File(cetificado + ".pdf");
            Desktop.getDesktop().open(path);
        } catch (Exception e) {
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        Font fuente = new Font();
        fuente.setStyle(Font.BOLD);
        fuente.setSize(14);
        Font tipografia = new Font();
        tipografia.setSize(10);
        Font TITULO = new Font();
        TITULO.setSize(18);
        Font enca = new Font();
        enca.setSize(8);
        enca.setStyle(Font.NORMAL);
        try {
            if (o.equals(vista.btn_imprimir)) {

                int selec = vista.tbl_grado.getSelectedRow();
                int codigocurso = Integer.parseInt(vista.tbl_grado.getValueAt(selec, 0).toString());
                // creacion del documento pdf
                FileOutputStream archivo;
                //encabesado del documento
                Image imagen = Image.getInstance("escudocolegio.jpg");
                imagen.setAlignment(Element.ALIGN_CENTER);
                imagen.scaleAbsolute(80, 40);
                imagen.setAbsolutePosition(250, 800);
                Paragraph titulo1 = new Paragraph("UNIDAD EDUCATIVA MOLLETURO", TITULO);
                titulo1.setAlignment(1);
                Paragraph direccion = new Paragraph("Molleturo - Cuenca - Azuay", enca);
                direccion.setAlignment(Element.ALIGN_CENTER);
                Paragraph datos = new Paragraph("Tlfno: 074045599 - E-mail: colegiomolleturo1981@hotmail.com ", enca);
                datos.setAlignment(Element.ALIGN_CENTER);
                Paragraph linea = new Paragraph("_____________________________________________________________________________________________________________________", enca);
                // cuerpo del documento
                Paragraph titulo = new Paragraph("LISTA  DE REPRESENTANTES", fuente);
                titulo.setAlignment(1);
                try {
                    archivo = new FileOutputStream(vista.lb_nombrelis.getText() + ".pdf");
                    PdfWriter.getInstance(documento, archivo);
                    documento.open();
                    documento.add(imagen);
                    documento.add(titulo1);
                    documento.add(direccion);
                    documento.add(datos);
                    documento.add(linea);
                    documento.add(Chunk.NEWLINE);
                    documento.add(titulo);
                    Paragraph texto = new Paragraph("CURSO: " + vista.tbl_grado.getValueAt(selec, 1).toString() + " " + vista.tbl_grado.getValueAt(selec, 2).toString());
                    Paragraph text = new Paragraph("JORNADA: " + vista.tbl_grado.getValueAt(selec, 3).toString() + "                         PARALELO: " + vista.tbl_grado.getValueAt(selec, 4).toString());
                    Paragraph tex = new Paragraph("PERIODO LECTIVO: " + in.devolverundato("select max(periodo)from periodoacademico"));
                    documento.add(texto);
                    documento.add(text);
                    documento.add(tex);
                    documento.add(Chunk.NEWLINE);
                    // creo la tabla  en el  pdf
                    PdfPTable tabla = new PdfPTable(2);
                    tabla.setWidthPercentage(100);
                    // establecer ancho de las columnas
                    float[] medidaCeldas = {0.55f, 1.55f};
                    tabla.setWidths(medidaCeldas);
                    PdfPCell cedula = new PdfPCell(new Phrase("CEDULA", fuente));
                    cedula.setBackgroundColor(BaseColor.WHITE);
                    cedula.setHorizontalAlignment(Element.ALIGN_CENTER);
                    PdfPCell nombres = new PdfPCell(new Phrase("APELLIDOS Y NOMBRES", fuente));
                    nombres.setBackgroundColor(BaseColor.WHITE);
                    nombres.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tabla.addCell(cedula);
                    tabla.addCell(nombres);

                    // agrego datos a la  tabla del pdf
                    for (int i = 0; i < vista.tbl_listarepresentante.getRowCount(); i++) {
                        Paragraph ced = new Paragraph(vista.tbl_listarepresentante.getValueAt(i, 0).toString(), tipografia);
                        ced.setAlignment(1);
                        Paragraph nom = new Paragraph(vista.tbl_listarepresentante.getValueAt(i, 1).toString(), tipografia);
                        nom.setAlignment(1);
                        tabla.addCell(ced);
                        tabla.addCell(nom);
                    }
                    documento.add(tabla);
                    documento.close();
                } catch (Exception ex) {

                }
                abrirpdf(vista.lb_nombrelis.getText());
            }
        } catch (Exception ex) {
        }
    }

    public void eliminarfilas(DefaultTableModel model) {
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource().equals(vista.tbl_grado)) {
            int selec = vista.tbl_grado.getSelectedRow();
            int codigocurso = Integer.parseInt(vista.tbl_grado.getValueAt(selec, 0).toString());

            int ma = Integer.parseInt(in.devolverundato("select count(codigomatricula) from matricula  where codigocurso=" + codigocurso + " and matricula.codigoperiodo in  (SELECT max(codigoperiodo) from periodoacademico)"));
            if (ma > 0) {
                mda.cargarepresentatesporcursomatriculado(vista.tbl_listarepresentante, columnas, codigocurso);
            } else if (ma == 0) {
                eliminarfilas(columnas);
                JOptionPane.showMessageDialog(null, "No hay estudiantes matriculados", "", JOptionPane.INFORMATION_MESSAGE);
            }
        }

    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

}
