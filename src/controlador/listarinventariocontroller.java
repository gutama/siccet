
package controlador;

import bases.incrementocodigo;
import bases.mimagenes;
import dao.areadministrativaDao;
import dao.inventarioDao;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import modelo.inventario;
import vista.vlistarinventario;

/**
 *
 * @author Legion
 */
public class listarinventariocontroller implements ActionListener,KeyListener{
    vlistarinventario vista;
    inventario imodelo=new inventario();
    inventarioDao idao=new inventarioDao();
    DefaultTableModel columna=new DefaultTableModel();
    areadministrativaDao adado=new areadministrativaDao();
    incrementocodigo in=new incrementocodigo();
    public listarinventariocontroller(vlistarinventario vista) {
        this.vista = vista;
        adado.listartodaslasareas(vista.jc_listaareas);
        cabeceratabla(vista.tbl_inventario);
        //poner en  modo escucha los  botones
        vista.jc_listaareas.addActionListener(this);
        vista.txt_buscar.addKeyListener(this);
    }
    public void cabeceratabla(JTable tabla){
    columna.addColumn("CODIGO");
    columna.addColumn("IDENTIFICADOR");
    columna.addColumn("SERIE");
    columna.addColumn("CODIGO ACTUAL");
    columna.addColumn("DESCRIPCION");
    columna.addColumn("FECHA INGRESO");
    columna.addColumn("CANTIDAD");
    columna.addColumn("ORIGEN INGRESO");
    columna.addColumn("TIPO RESPALDO");
    columna.addColumn("COSTO");
    columna.addColumn("MODELO");
    columna.addColumn("MARCA");
    columna.addColumn("COLOR");
    columna.addColumn("MATERIAL");
    columna.addColumn("DIMENCION");
    columna.addColumn("ESTADO");
    tabla.setModel(columna);
    }
    public void iniciarvista(String fondos,mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
  i.insertarimagenesetiquetas(vista.fondo,fondos);
    }
          
    public void eliminarfilas(DefaultTableModel model) {
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }
      private void filtrodatos(String consulta, JTable jtableBuscar) {
        columna = (DefaultTableModel) jtableBuscar.getModel();
        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<>(columna);
        jtableBuscar.setRowSorter(tr);
        tr.setRowFilter(RowFilter.regexFilter(consulta));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
           Object o=e.getSource();
           if(o.equals(vista.jc_listaareas)){
               imodelo.setCodigoareaFK(Integer.parseInt(in.devolverundato("select codigoarea from area where area='"+vista.jc_listaareas.getSelectedItem()+"'")));
               int codigoinve=Integer.parseInt(in.devolverundato("select count(codigoinventario) from inventario where codigoarea="+imodelo.getCodigoareaFK()+""));
             if(codigoinve>0){
             idao.cargarareas(vista.tbl_inventario, columna, imodelo);
             }else
                 if(codigoinve==0){
                                eliminarfilas(columna);
                 JOptionPane.showMessageDialog(null,"No existe inventario en el area","",JOptionPane.INFORMATION_MESSAGE);
          
             }
               
               
           }
        } catch (Exception ex) {
        }
    
    }

    @Override
    public void keyTyped(KeyEvent e) {
       
    }

    @Override
    public void keyPressed(KeyEvent e) {
       
    }

    @Override
    public void keyReleased(KeyEvent e) {
        filtrodatos(vista.txt_buscar.getText(), vista.tbl_inventario);
    }
    
}
