package controlador;

import bases.incrementocodigo;
import bases.mimagenes;
import dao.gradoDao;
import dao.materiaDao;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import modelo.grado;
import modelo.gradoMateria;
import vista.vmallacademica;

/**
 *
 * @author Legion
 */
public class mallacontroller implements ActionListener {

    vmallacademica vista;
    gradoDao gdao = new gradoDao();
    materiaDao mdao = new materiaDao();
    gradoMateria m = new gradoMateria();
    incrementocodigo in = new incrementocodigo();
    grado gmodel=new grado();

    public mallacontroller(vmallacademica vista) {
        this.vista = vista;
        vista.jc_selecionargrado.addActionListener(this);
        //cagar datos de la base
        gdao.listargradosconcatenados(vista.jc_selecionargrado);
    }

  public void iniciarvista(String fondos,mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
  i.insertarimagenesetiquetas(vista.fondo,fondos);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Object o = e.getSource();
            if(o.equals(vista.jc_selecionargrado)){
            String grado = (String) vista.jc_selecionargrado.getSelectedItem();

                String tipo = grado.substring(8);     
                if (tipo.equals("EGB")) {
                    gmodel.setTipo(tipo);
                    gmodel.setCodigogrado(Integer.parseInt(in.devolverundato("select codigogrado  from grado  where nombre='" + grado.substring(0, 8) + "' and tipo='" + tipo + "'")));               
                } else {
                    tipo = grado.substring(9);
                    gmodel.setTipo(tipo);
                    gmodel.setCodigogrado(Integer.parseInt(in.devolverundato("select codigogrado  from grado  where nombre='" + grado.substring(0, 8) + "' and tipo='" + tipo + "'")));
                    gdao.visualizarmalla(vista.tbl_listargradosmaterias,gmodel);
                }
        gdao.visualizarmalla(vista.tbl_listargradosmaterias,gmodel);
            }
        } catch (Exception ex) {
            System.out.println("problema " + ex.getMessage());
        }

    }

}
