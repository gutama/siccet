package controlador;

import bases.incrementocodigo;
import bases.mimagenes;
import bases.validaciones;
import java.awt.event.*;
import javax.swing.*;
import dao.materiaDao;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import modelo.areaPedagogica;
import modelo.diseñotablas.Render;
import modelo.materia;
import vista.vmateria;
import vista.vrgeneral;

/**
 *
 * @author Mesias
 */
public class materiacontroller implements ActionListener, MouseListener {

    vmateria vista;
    materia m = new materia();
    areaPedagogica m1=new areaPedagogica();
    materiaDao sg = new materiaDao();
    incrementocodigo in=new incrementocodigo();
    public materiacontroller(vmateria vista) {
        this.vista = vista;
        //poner en  modo escucha los botones
        this.vista.btn_guardarmateria.addActionListener(this);
        this.vista.btn_guardarArea.addActionListener(this);
        this.vista.btn_modificarmateria.addActionListener(this);
        vista.jc_listarareapedagogica.addActionListener(this);
        vista.tbl_listamaterias.addMouseListener(this);
        //cargar los datos de la  base
        sg.listarareaspedagogicas(vista.jc_listarareapedagogica);
        sg.listarareaspedagogicas(vista.jc_seleccionarareapedagogica);
     cargarTabla();
        validarcampos();
    }

    public void iniciarvista(String fondos,mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
  i.insertarimagenesetiquetas(vista.fondo,fondos);
    }
    public void limpliarcajas() {
        vista.txt_nommateria.setText("");
    }
      public void limpliarcajasarea() {
        vista.txt_nombreareapeda.setText("");
    }
    public void validarcampos(){
        validaciones v=new validaciones();
        v.ingresarsolomayusculas(vista.txt_nombreareapeda);
        v.ingresarsolomayusculas(vista.txt_nommateria);
    }
    private void cargarTabla() {
        vista.tbl_listamaterias.setDefaultRenderer(Object.class, new Render());

        String[] columnas = new String[]{"CODIGO", "MATERIAS"};
        boolean[] editable = {false,true};
        Class[] types = new Class[]{java.lang.Object.class, java.lang.Object.class};
        DefaultTableModel mModeloTablaProd = new DefaultTableModel(columnas, 0) {

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int row, int column) {
                return editable[column];
            }
        };
        ArrayList<materia> lis = sg.listar_materias();
        if (lis.size() > 0) {
            Object filas[] = new Object[columnas.length];
            for (int j = 0; j < lis.size(); j++) {
                m = lis.get(j);
                filas[0] = m.getCodigomateria();
                filas[1] = m.getNombre();
                mModeloTablaProd.addRow(filas);
            }
        }
        vista.tbl_listamaterias.setModel(mModeloTablaProd);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Object o = e.getSource();
            if (o.equals(vista.btn_modificarmateria)) {
                int sele = vista.tbl_listamaterias.getSelectedRow();
                m.setCodigomateria(Integer.parseInt((String) vista.tbl_listamaterias.getValueAt(sele, 0)));
                m.setNombre(vista.txt_nommateria.getText());
                if(sg.modificar(m)==true){
                JOptionPane.showMessageDialog(null,"Modificado","",JOptionPane.INFORMATION_MESSAGE);
               cargarTabla();
                }  
            }
            
            if(o.equals(vista.btn_guardarArea)){// guarda  la  area pedagogica 
                m1.setNombre(vista.txt_nombreareapeda.getText());
                if (m1.getNombre().length() > 0) {
                    sg.guardar(m1);
                    sg.listarareaspedagogicas(vista.jc_listarareapedagogica);
                    limpliarcajasarea();
                } else {
                    JOptionPane.showMessageDialog(null, "Ingrese el nombre de la área pedagógica", "!Advertencia!", JOptionPane.WARNING_MESSAGE);
                }
            }
            if (o.equals(vista.btn_guardarmateria)) {// boton para guardar l las  materias
                if (vista.txt_nommateria.getText().length() > 0) {
                    m.setCodigoareapedagogica(Integer.parseInt(in.devolverundato("select idareapedagogica from  areapedagogica where "
                            + "nombre='"+vista.jc_seleccionarareapedagogica.getSelectedItem()+"'")));
                    m.setNombre(vista.txt_nommateria.getText());
                    sg.guardar(m);
                    this.limpliarcajas();
                   cargarTabla();
                } else {
                    JOptionPane.showMessageDialog(null, "Ingrese el nombre de la materia", "!Advertencia!", JOptionPane.WARNING_MESSAGE);
                }
            }
            if(o.equals(vista.jc_listarareapedagogica)){
                m1.setNombre((String) vista.jc_listarareapedagogica.getSelectedItem());
            vista.txt_nombreareapeda.setText(m1.getNombre());
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        int sele = vista.tbl_listamaterias.getSelectedRow();
        vista.txt_nommateria.setText(vista.tbl_listamaterias.getValueAt(sele, 1).toString()); //eviar datos a un jtextfield     
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
