package controlador;

import bases.mimagenes;
import bases.validaciones;
import dao.*;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URI;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.*;
import vista.*;

/**
 *
 * @author Legion
 */
public class menucontroller implements ActionListener {

    menuprincipal vista;
    validaciones v = new validaciones();
    mimagenes i = new mimagenes();
    String fondo = "/imagenes/fondo 3.jpg";

    public menucontroller(menuprincipal vista) {
        this.vista = vista;
        iniciarvista();
        vista.jm_general.addActionListener(this);
        vista.jm_materia.addActionListener(this);
        vista.jm_estudiante.addActionListener(this);
        vista.jm_docente.addActionListener(this);
        vista.jm_aula.addActionListener(this);
        vista.jm_grado.addActionListener(this);
        vista.jm_institucion.addActionListener(this);
        vista.jm_asigraymaterias.addActionListener(this);
        vista.jm_actualizarMatricula.addActionListener(this);
        vista.jm_crearcurso.addActionListener(this);
        vista.jm_nuevaMatricula.addActionListener(this);
        vista.jc_mallaacademica.addActionListener(this);
        vista.jm_asignarAula.addActionListener(this);
        vista.jm_subirnotas.addActionListener(this);
        vista.jm_nuevaarea.addActionListener(this);
        vista.jm_asigarea.addActionListener(this);
        vista.jm_nuevoinventario.addActionListener(this);
        vista.jm_verinventario.addActionListener(this);
        vista.jm_parestudiantil.addActionListener(this);
        vista.jm_plataformaeducativa.addActionListener(this);
        vista.jm_certificadopromocional.addActionListener(this);
        vista.jm_vercuadrocalificacioes.addActionListener(this);
        vista.jm_crearmalla.addActionListener(this);
        vista.jm_requisitos.addActionListener(this);
        vista.jm_certificadomatricula.addActionListener(this);
        vista.jm_listaestudiantes.addActionListener(this);
        vista.jm_listapadresdefamilia.addActionListener(this);
        vista.jm_listadocentes.addActionListener(this);
    }

    public void iniciarvista() {
        vista.setLocationRelativeTo(null);
        vista.setExtendedState(JFrame.MAXIMIZED_BOTH);
        vista.setTitle("BIENVENIDOS AL SISTEMA DE  INVENTARIO CALIFICACIONES,COMUNICACION EDUCATIVO TECNOLOGICO - UNIDAD  EDUCATIVA MOLLETURO");
        vista.setVisible(true);
        i.logoventanas(vista, "./imagenes/escudocolegio.jpg");
        i.insertarimagenesetiquetas(vista.fondo, "/imagenes/fondo2.jpg");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        try {
            if(o.equals(vista.jm_listadocentes)){
            vlistadocentes v= new vlistadocentes(new javax.swing.JFrame(), true);
            listadocentecontroller controlador=new listadocentecontroller(v);
             controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_listapadresdefamilia)) {
                vlistarepresentantes v = new vlistarepresentantes(new javax.swing.JFrame(), true);
                listarepresentantescontroller controlador = new listarepresentantescontroller(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_listaestudiantes)) {
                vlistaestudiantes v = new vlistaestudiantes(new javax.swing.JFrame(), true);
                listaestudiantescontroller controlador = new listaestudiantescontroller(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_certificadomatricula)) {
                vcertificadomatricula v = new vcertificadomatricula(new javax.swing.JFrame(), true);
                certificadomatriculacontroller controlador = new certificadomatriculacontroller(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_requisitos)) {
                vrequisito v = new vrequisito(new javax.swing.JFrame(), true);
                requisitocontroller controlador = new requisitocontroller(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_crearmalla)) {
                vcrearmallacademica v = new vcrearmallacademica(new javax.swing.JFrame(), true);
                crearmallacontroller controlador = new crearmallacontroller(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_vercuadrocalificacioes)) {
                vvercalificaciones v = new vvercalificaciones(new javax.swing.JFrame(), true);
                cuadrocalificacionescontroller controlador = new cuadrocalificacionescontroller(v);
                controlador.iniciarvista(fondo, i);
            }

            if (o.equals(vista.jm_materia)) {
                vmateria mate = new vmateria(new javax.swing.JFrame(), true);//vista de grado y materias
                materiacontroller controlador = new materiacontroller(mate);//agregar la vista de grado y matricula en el controlador
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_certificadopromocional)) {
                vcertificado v = new vcertificado(new javax.swing.JFrame(), true);
                certificadopromocionalcontroller controlador = new certificadopromocionalcontroller(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_estudiante)) {
                vrestudiante v = new vrestudiante(new javax.swing.JFrame(), true);
                estudiantecontroller controlador = new estudiantecontroller(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_docente)) {
                vdocente v = new vdocente(new javax.swing.JFrame(), true);
                docentecontroller controlador = new docentecontroller(v);
                controlador.iniciarvista();
            }
            if (o.equals(vista.jm_aula)) {
                vaula v = new vaula(new javax.swing.JFrame(), true);
                aulacontroller controlador = new aulacontroller(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_grado)) {
                vgrado v = new vgrado(new javax.swing.JFrame(), true);
                gradocontroller controlador = new gradocontroller(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_crearcurso)) {
                vcurso v = new vcurso(new javax.swing.JFrame(), true);
                cursocontroller controlador = new cursocontroller(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_general)) {
                vrgeneral v = new vrgeneral(new javax.swing.JFrame(), true);
                generalcontroller controlador = new generalcontroller(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_institucion)) {
                vinstitucion v = new vinstitucion(new javax.swing.JFrame(), true);
                institucionController controlador = new institucionController(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_asigraymaterias)) {
                vasignar v = new vasignar(new javax.swing.JFrame(), true);
                asignarcontroller controlador = new asignarcontroller(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_nuevaMatricula)) {
                Vnuevamatricula v = new Vnuevamatricula(new javax.swing.JFrame(), true);
                newmatriculacontroller controlador = new newmatriculacontroller(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jc_mallaacademica)) {
                vmallacademica v = new vmallacademica(new javax.swing.JFrame(), true);
                mallacontroller controlador = new mallacontroller(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_asignarAula)) {
                vaulacurso v = new vaulacurso(new javax.swing.JFrame(), true);
                aulacursocontroller controlador = new aulacursocontroller(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_subirnotas)) {
                vcalificaciones v = new vcalificaciones(new javax.swing.JFrame(), true);
                calificacionesController controlador = new calificacionesController(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_nuevaarea)) {
                vareaadministrativa v = new vareaadministrativa(new javax.swing.JFrame(), true);
                areacontroller controlador = new areacontroller(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_asigarea)) {
                vasignarareaadministrativa v = new vasignarareaadministrativa(new javax.swing.JFrame(), true);
                areaasignarcontroller controlador = new areaasignarcontroller(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_nuevoinventario)) {
                String respuesta = JOptionPane.showInputDialog(null, "Ingrese su cedula");

                if (v.validacioncedula(respuesta) == true && respuesta.length() == 10) {
                    vinventario vi = new vinventario(new javax.swing.JFrame(), true);
                    inventarioController controlador = new inventarioController(vi, respuesta);
                    controlador.iniciarvista(fondo, i);

                } else {
                    System.out.println("cedula incorrecta");
                }

            }
            if (o.equals(vista.jm_verinventario)) {
                vlistarinventario vl = new vlistarinventario(new javax.swing.JFrame(), true);
                listarinventariocontroller controlador = new listarinventariocontroller(vl);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_parestudiantil)) {
                vparticipacionestudiantil v = new vparticipacionestudiantil(new javax.swing.JFrame(), true);
                participacioncontroller controlador = new participacioncontroller(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_actualizarMatricula)) {
                VactualizarMatricula v = new VactualizarMatricula(new javax.swing.JFrame(), true);
                actualizarmatriculaController controlador = new actualizarmatriculaController(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_plataformaeducativa)) {

                try {
                    if (Desktop.isDesktopSupported()) {
                        Desktop desktop = Desktop.getDesktop();
                        if (desktop.isSupported(Desktop.Action.BROWSE)) {
                            desktop.browse(new URI("https://www.uemolleturo.com"));
                        }
                    }
                } catch (Exception ec) {
                    ec.printStackTrace();
                }
            }

        } catch (Exception ex) {
        }
    }
}
