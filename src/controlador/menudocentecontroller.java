package controlador;

import bases.mimagenes;
import bases.validaciones;
import dao.*;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URI;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.*;
import vista.*;

/**
 *
 * @author Legion
 */
public class menudocentecontroller implements ActionListener {

    menuprincipaldocente vista;
    validaciones v = new validaciones();
    mimagenes i = new mimagenes();
    String fondo = "/imagenes/fondo 3.jpg";

    public menudocentecontroller(menuprincipaldocente vista) {
        this.vista = vista;
        iniciarvista();

        vista.jm_docente.addActionListener(this);
       vista.jm_verinventario.addActionListener(this);
        vista.jm_subirnotas.addActionListener(this);
        vista.jm_nuevoinventario.addActionListener(this);
        vista.jm_parestudiantil.addActionListener(this);
        vista.jm_plataformaeducativa.addActionListener(this);
        vista.jm_certificadopromocional.addActionListener(this);
        vista.jm_vercuadrocalificacioes.addActionListener(this);
        vista.jm_listaestudiantes.addActionListener(this);
        vista.jm_listapadresdefamilia.addActionListener(this);
    }

    public void iniciarvista() {
        vista.setLocationRelativeTo(null);
        vista.setExtendedState(JFrame.MAXIMIZED_BOTH);
        vista.setTitle("BIENVENIDOS AL SISTEMA DE  INVENTARIO CALIFICACIONES,COMUNICACION EDUCATIVO TECNOLOGICO - UNIDAD  EDUCATIVA MOLLETURO");
        vista.setVisible(true);
        i.logoventanas(vista, "./imagenes/escudocolegio.jpg");
        i.insertarimagenesetiquetas(vista.fondo, "/imagenes/fondo2.jpg");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        try {

            if (o.equals(vista.jm_listapadresdefamilia)) {
                vlistarepresentantes v = new vlistarepresentantes(new javax.swing.JFrame(), true);
                listarepresentantescontroller controlador = new listarepresentantescontroller(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_listaestudiantes)) {
                vlistaestudiantes v = new vlistaestudiantes(new javax.swing.JFrame(), true);
                listaestudiantescontroller controlador = new listaestudiantescontroller(v);
                controlador.iniciarvista(fondo, i);
            }
            
            if (o.equals(vista.jm_vercuadrocalificacioes)) {
                vvercalificaciones v = new vvercalificaciones(new javax.swing.JFrame(), true);
                cuadrocalificacionescontroller controlador = new cuadrocalificacionescontroller(v);
                controlador.iniciarvista(fondo, i);
            }

      
            if (o.equals(vista.jm_docente)) {
                vdocente v = new vdocente(new javax.swing.JFrame(), true);
                docentecontroller controlador = new docentecontroller(v);
                controlador.iniciarvista();
            }
          
           
            if (o.equals(vista.jm_subirnotas)) {
                vcalificaciones v = new vcalificaciones(new javax.swing.JFrame(), true);
                calificacionesController controlador = new calificacionesController(v);
                controlador.iniciarvista(fondo, i);
            }
          
            if (o.equals(vista.jm_nuevoinventario)) {
                String respuesta = JOptionPane.showInputDialog(null, "Ingrese su cedula");

                if (v.validacioncedula(respuesta) == true && respuesta.length() == 10) {
                    vinventario vi = new vinventario(new javax.swing.JFrame(), true);
                    inventarioController controlador = new inventarioController(vi, respuesta);
                    controlador.iniciarvista(fondo, i);

                } else {
                    System.out.println("cedula incorrecta");
                }

            }
            if (o.equals(vista.jm_verinventario)) {
                vlistarinventario vl = new vlistarinventario(new javax.swing.JFrame(), true);
                listarinventariocontroller controlador = new listarinventariocontroller(vl);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_parestudiantil)) {
                vparticipacionestudiantil v = new vparticipacionestudiantil(new javax.swing.JFrame(), true);
                participacioncontroller controlador = new participacioncontroller(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_certificadopromocional)) {
                vcertificado v = new vcertificado(new javax.swing.JFrame(), true);
                certificadopromocionalcontroller controlador = new certificadopromocionalcontroller(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_plataformaeducativa)) {

                try {
                    if (Desktop.isDesktopSupported()) {
                        Desktop desktop = Desktop.getDesktop();
                        if (desktop.isSupported(Desktop.Action.BROWSE)) {
                            desktop.browse(new URI("https://www.uemolleturo.com"));
                        }
                    }
                } catch (Exception ec) {
                    ec.printStackTrace();
                }
            }

        } catch (Exception ex) {
        }
    }
}
