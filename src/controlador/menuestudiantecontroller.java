package controlador;

import bases.mimagenes;
import bases.validaciones;
import dao.*;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URI;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.*;
import vista.*;

/**
 *
 * @author Legion
 */
public class menuestudiantecontroller implements ActionListener {

    menuprincipalestudiante vista;
    validaciones v = new validaciones();
    mimagenes i = new mimagenes();
    String fondo = "/imagenes/fondo 3.jpg";

    public menuestudiantecontroller(menuprincipalestudiante vista) {
        this.vista = vista;
        iniciarvista();
        vista.jm_estudiante.addActionListener(this);
        vista.jm_actualizarMatricula.addActionListener(this);
        vista.jm_plataformaeducativa.addActionListener(this);
        vista.jm_vercuadrocalificacioes.addActionListener(this);

    }

    public void iniciarvista() {
        vista.setLocationRelativeTo(null);
        vista.setExtendedState(JFrame.MAXIMIZED_BOTH);
        vista.setTitle("BIENVENIDOS AL SISTEMA DE  INVENTARIO CALIFICACIONES,COMUNICACION EDUCATIVO TECNOLOGICO - UNIDAD  EDUCATIVA MOLLETURO");
        vista.setVisible(true);
        i.logoventanas(vista, "./imagenes/escudocolegio.jpg");
        i.insertarimagenesetiquetas(vista.fondo, "/imagenes/fondo2.jpg");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        try {
       
            if (o.equals(vista.jm_vercuadrocalificacioes)) {
                vvercalificaciones v = new vvercalificaciones(new javax.swing.JFrame(), true);
                cuadrocalificacionescontroller controlador = new cuadrocalificacionescontroller(v);
                controlador.iniciarvista(fondo, i);
            }

           
            if (o.equals(vista.jm_estudiante)) {
                vrestudiante v = new vrestudiante(new javax.swing.JFrame(), true);
                estudiantecontroller controlador = new estudiantecontroller(v);
                controlador.iniciarvista(fondo, i);
            }
           
            if (o.equals(vista.jm_actualizarMatricula)) {
                VactualizarMatricula v = new VactualizarMatricula(new javax.swing.JFrame(), true);
                actualizarmatriculaController controlador = new actualizarmatriculaController(v);
                controlador.iniciarvista(fondo, i);
            }
            if (o.equals(vista.jm_plataformaeducativa)) {

                try {
                    if (Desktop.isDesktopSupported()) {
                        Desktop desktop = Desktop.getDesktop();
                        if (desktop.isSupported(Desktop.Action.BROWSE)) {
                            desktop.browse(new URI("https://www.uemolleturo.com"));
                        }
                    }
                } catch (Exception ec) {
                    ec.printStackTrace();
                }
            }

        } catch (Exception ex) {
        }
    }
}
