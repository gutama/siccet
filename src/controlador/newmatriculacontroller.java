package controlador;

import bases.*;
import dao.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import modelo.*;
import modelo.diseñotablas.Render;
import vista.Vnuevamatricula;

/**
 *
 * @author Legion
 */
public class newmatriculacontroller implements ActionListener, MouseListener {

    Vnuevamatricula vista;
    estudiante emodelo = new estudiante();
    estudianteDao edao = new estudianteDao();
    estudianteDocumento esd = new estudianteDocumento();
    gradoDao gdao = new gradoDao();
    validaciones v = new validaciones();
    documento model = new documento();
    documentoDao dob = new documentoDao();
    periodoacademicoDao bs = new periodoacademicoDao();
    String rutaarcivo = "";
    ImageIcon icono = null;
    matricula mmodelo = new matricula();
    matriculaDao mdao = new matriculaDao();
    incrementocodigo in = new incrementocodigo();
    gradoRequisitos grmodelo = new gradoRequisitos();
    requisitoDao rdado = new requisitoDao();
    requsitosmatricula re = new requsitosmatricula();

    public newmatriculacontroller(Vnuevamatricula vista) {
        this.vista = vista;
        //cargardatos de la base
        gdao.nuevamatricula(vista.tbl_listacursos);//metodo que carga los cursos  como  octavo y primero ciencias o tecnico 
        validarcampos();
        //poner en modo escucha los botones
        vista.btn_buscar.addActionListener(this);
        vista.btn_guardar.addActionListener(this);
        vista.btn_seleccionacopiadeceduES.addActionListener(this);
        vista.tbl_listacursos.addMouseListener(this);
        vista.cbselecionar.addActionListener(this);
        //cargar  datos de la  base 
        vista.txt_periodo.setText(in.devolverundato("SELECT max(periodo)  as periodo from periodoacademico"));
        vista.cbselecionar.setText("Cumple con los Requisitos");
    }
public static String getFechaActual() {
        java.util.Date fecha = new java.util.Date();
        SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/YYYY");
        return formatoFecha.format(fecha);
    }
    public void validarcampos() {
        v.limitarcaracteres(vista.txt_cedula, 10);
        v.validarnumeros(vista.txt_cedula);

    }

    public void seleccionararchivo(JButton seleccionararchivo) {
        JFileChooser j = new JFileChooser();
        FileNameExtensionFilter fi = new FileNameExtensionFilter("Archivos pdf", "pdf");
        j.setFileFilter(fi);
        int se = j.showOpenDialog(vista);
        if (se == 0) {
            seleccionararchivo.setText("" + j.getSelectedFile().getName());
            rutaarcivo = j.getSelectedFile().getAbsolutePath();
        }
    }

    public int guardar(String nombre, File ruta) throws IOException {
        try {
            model.setNombrepdf(nombre);
            byte[] docu = new byte[(int) ruta.length()];
            InputStream z = new FileInputStream(ruta);
            z.read(docu);
            model.setArchivopdf(docu);
        } catch (FileNotFoundException ex) {
            model.setArchivopdf(null);
        }
        return dob.agregar_pdfVO(model);
    }

    public void iniciarvista(String fondos, mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
        i.insertarimagenesetiquetas(vista.fondo, fondos);

    }

    public int numerodecuposporaula(int codigocurso) {
        int respuesta = Integer.parseInt(in.devolverundato("SELECT cupos from aula,aulacurso,curso where aulacurso.codigoaula=aula.codigoAula and aulacurso.codigocurso=curso.codigocurso AND curso.codigocurso=" + codigocurso + ""));
        return respuesta;
    }

    public int numerodeestudiantesmatriculados(int codigocurso) {
        int respuesta = Integer.parseInt(in.devolverundato("SELECT COUNT(codigocurso) FROM matricula,periodoacademico  WHERE  matricula.codigocurso=" + codigocurso + " and matricula.codigoperiodo in (SELECT MAX(codigoperiodo) from periodoacademico)"));
        return respuesta;
    }

    private void cargarTabla(int codigogrado) {
        vista.tbl_requisitos.setDefaultRenderer(Object.class, new Render());

        String[] columnas = new String[]{"REQUISITOS", "SELECCIONE"};
        boolean[] editable = {false, true};
        Class[] types = new Class[]{java.lang.Object.class, java.lang.Boolean.class};
        DefaultTableModel mModeloTablaProd = new DefaultTableModel(columnas, 0) {

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int row, int column) {
                return editable[column];
            }
        };
        ArrayList<requsitosmatricula> lis = rdado.listar_requisitos(codigogrado);
        if (lis.size() > 0) {
            Object filas[] = new Object[columnas.length];
            for (int j = 0; j < lis.size(); j++) {
                re = lis.get(j);
                filas[0] = re.getNombre();
                filas[1] = vista.cbselecionar.isSelected();
                mModeloTablaProd.addRow(filas);
            }
        }
        vista.tbl_requisitos.setModel(mModeloTablaProd);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        try {
            if (o.equals(vista.cbselecionar)) {
                if (vista.cbselecionar.isSelected()) {
                    vista.cbselecionar.setText("Desseleccionar todos");
                } else {
                    vista.cbselecionar.setText("Cumple con los Requisitos");
                }
                int selec = vista.tbl_listacursos.getSelectedRow();
                grmodelo.setCodigogradoFK(Integer.parseInt(in.devolverundato("select codigogrado from grado where nombre='" + vista.tbl_listacursos.getValueAt(selec, 1).toString() + "'")));
                cargarTabla(grmodelo.getCodigogradoFK());
            }
            if (o.equals(vista.btn_guardar)) {
                if (vista.txt_cedula.getText().length() != 0) {
                    int sele = vista.tbl_listacursos.getSelectedRow();
                    mmodelo.setCodigocursoFk(Integer.parseInt(vista.tbl_listacursos.getValueAt(sele, 0).toString())); //obtengo el codigo del curso
                    mmodelo.setCedulaestudianteFK(vista.txt_cedula.getText());
                    mmodelo.setCodigoperiodoFK(Integer.parseInt(in.devolverundato("select codigoperiodo from periodoacademico where  periodo='" + vista.txt_periodo.getText() + "'")));
                    mmodelo.setFecha(getFechaActual());
                    int codigomat = Integer.parseInt(in.devolverundato("SELECT COUNT(codigomatricula) from matricula"));
                    if (codigomat == 0) {
                     if (vista.cbselecionar.equals("true"));
                                    mdao.guardar(mmodelo);
                                
                    } else
                    if(codigomat>0){
                        int codigocusro = Integer.parseInt(in.devolverundato("select codigocurso from matricula"));
                        String cedulamatricula = in.devolverundato("select cedulaestudiante from matricula");
                        if (mmodelo.getCodigocursoFk() != codigocusro && cedulamatricula != mmodelo.getCedulaestudianteFK()) {
                            if (numerodeestudiantesmatriculados(mmodelo.getCodigocursoFk()) <= numerodecuposporaula(mmodelo.getCodigocursoFk())) {
                                if (vista.cbselecionar.equals("true")) {
                                    mdao.guardar(mmodelo);
                                } else 
                                    JOptionPane.showMessageDialog(null, "no cumple con los requisitos", "¡Avertencia¡", JOptionPane.WARNING_MESSAGE);
                            } else {
                                JOptionPane.showMessageDialog(null, "Cupo Lleno", "¡Avertencia¡", JOptionPane.WARNING_MESSAGE);
                            }

                        } else {
                            JOptionPane.showMessageDialog(null, "No puede matricular en este curso", "¡Avertencia¡", JOptionPane.WARNING_MESSAGE);
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Rellene el campo cedula", "¡Avertencia¡", JOptionPane.WARNING_MESSAGE);
                }
            }
            if (o.equals(vista.btn_buscar)) {
                emodelo.setCedula(vista.txt_cedula.getText());
                if (v.validacioncedula(vista.txt_cedula.getText()) == true) {
                    edao.buscar(emodelo);
                    vista.txt_nombrescompletos.setText(emodelo.getNombre() + " " + emodelo.getApellido());
                }
            }
            if (o.equals(vista.btn_seleccionacopiadeceduES)) {
                seleccionararchivo(vista.btn_seleccionacopiadeceduES);
                File ruta = new File(rutaarcivo);
                model.setNombrepdf(vista.btn_seleccionacopiadeceduES.getText());
                esd.setCedulaestudiante(vista.txt_cedula.getText());
                esd.setCodigodocumento(guardar(model.getNombrepdf(), ruta));
                edao.guardarestudiantedocumento(esd);
            }

        } catch (Exception ex) {
        }

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource().equals(vista.tbl_listacursos)) {
            int selec = vista.tbl_listacursos.getSelectedRow();
            grmodelo.setCodigogradoFK(Integer.parseInt(in.devolverundato("select codigogrado from grado where nombre='" + vista.tbl_listacursos.getValueAt(selec, 1).toString() + "'")));
            cargarTabla(grmodelo.getCodigogradoFK());
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
