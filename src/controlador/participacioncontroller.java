package controlador;

import bases.incrementocodigo;
import bases.mimagenes;
import bases.validaciones;
import dao.gradoDao;
import dao.matriculaDao;
import dao.participacionDao;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.SQLException;
import java.util.logging.Level;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.participacionestudiantil;
import vista.vparticipacionestudiantil;

/**
 *
 * @author Legion
 */
public class participacioncontroller implements ActionListener, MouseListener {

    vparticipacionestudiantil vista;
    gradoDao gdao = new gradoDao();
    matriculaDao Mdao = new matriculaDao();
    DefaultTableModel columnas = new DefaultTableModel();
    participacionestudiantil p = new participacionestudiantil();
    participacionDao pdao = new participacionDao();
    validaciones v = new validaciones();
    incrementocodigo in = new incrementocodigo();

    public participacioncontroller(vparticipacionestudiantil vista) {
        this.vista = vista;
        //cargar datos de la tabla
        gdao.listarcursosparaparticipacion(vista.tbl_liscurso);
        vista.tbl_liscurso.addMouseListener(this);
        vista.btn_guardar.addActionListener(this);
        cabececera(vista.tbl_listestu);
    }

  public void iniciarvista(String fondos,mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
  i.insertarimagenesetiquetas(vista.fondo,fondos);
    }
    public int numerodeestudiantesmatriculados(int codigocurso) {
        int respuesta = Integer.parseInt(in.devolverundato("SELECT COUNT(codigocurso) FROM matricula,periodoacademico  WHERE  matricula.codigocurso=" + codigocurso + " and matricula.codigoperiodo in (SELECT MAX(codigoperiodo) from periodoacademico)"));
        return respuesta;
    }

    public void cabececera(JTable tabla) {
        columnas.addColumn("COD.MATRICULA");
        columnas.addColumn("CEDULA");
        columnas.addColumn("ESTUDIANTES");
        tabla.setModel(columnas);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Object o = e.getSource();
        if (o.equals(vista.btn_guardar)) {
            int sele = vista.tbl_listestu.getSelectedRow();
            p.setCodigomatriculaFK(Integer.parseInt(vista.tbl_listestu.getValueAt(sele,0).toString()));
            p.setCalificacion(Double.parseDouble(vista.txt_calificacion.getText()));
            pdao.guardar(p);
            ((DefaultTableModel) vista.tbl_listestu.getModel()).removeRow(sele);
        }
        } catch (Exception ex) {
        }
        
    }

    public void eliminarfilas(DefaultTableModel model) {
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        int selec = vista.tbl_liscurso.getSelectedRow();
        int codigocurso = Integer.parseInt(vista.tbl_liscurso.getValueAt(selec, 0).toString());
        try {
            if (numerodeestudiantesmatriculados(codigocurso) > 0) {
                Mdao.cargarestudiantesporcursomatriculado(vista.tbl_listestu, columnas, codigocurso);
            } else {
                eliminarfilas(columnas);
                JOptionPane.showMessageDialog(null, "Estudiantes no matriculados", "!Advertencia!", JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception ex) {
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

}
