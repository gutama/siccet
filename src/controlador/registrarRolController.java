package controlador;

import bases.*;
import dao.personaDao;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import modelo.*;
import vista.registrarroles;

/**
 *
 * @author Legion
 */
public class registrarRolController implements ActionListener {

    registrarroles vista;
    rol r = new rol();
    personaDao pda = new personaDao();
    validaciones v = new validaciones();
    incrementocodigo in=new incrementocodigo();
    persona p=new persona();
    personarol pr=new personarol();
    public registrarRolController(registrarroles vista) {
        this.vista = vista;
        //poner en modo escucha los botones 
        vista.btn_guardar.addActionListener(this);
        vista.jc_listararroles.addActionListener(this);
        vista.btn_modificar.addActionListener(this);
        vista.btn_buscar.addActionListener(this);
        vista.btn_buscarusuario.addActionListener(this);
        vista.btn_modificarrol.addActionListener(this);
        //cargar datos de la  base
        pda.Cargarroles(vista.jc_listararroles);
        //validar campos
        v.ingresarsolomayusculas(vista.txt_nombrerol);
        v.validarnumeros(vista.txt_cedularol);
        v.limitarcaracteres(vista.txt_cedularol,10);
        vista.txt_nombrerol.setText(" ");
    }

    public void iniciarvista(String fondos, mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
        i.insertarimagenesetiquetas(vista.fondo, fondos);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
     Object o=e.getSource();
        try {
            String secretKey = " PROGRAMADORES GUTAMA Y TADAY";
     if(o.equals(vista.btn_guardar)){
     r.setRol(vista.txt_nombrerol.getText());
     if(pda.guadarol(r)>0)
         JOptionPane.showMessageDialog(null, "Registrado", "", JOptionPane.INFORMATION_MESSAGE);
     pda.Cargarroles(vista.jc_listararroles);
      vista.txt_nombrerol.setText(" ");
     }
     if(o.equals(vista.jc_listararroles)){
        vista.txt_nombrerol.setText(vista.jc_listararroles.getSelectedItem().toString());
     }
     if (o.equals(vista.btn_modificar)){
        r.setRol(vista.txt_nombrerol.getText());
        r.setCodigorol(Integer.parseInt(in.devolverundato("select codigorol from rol  where cargo='"+vista.jc_listararroles.getSelectedItem()+"'")));
         if(pda.modificarrol(r)==true){
             JOptionPane.showMessageDialog(null, "Modificado", "", JOptionPane.INFORMATION_MESSAGE);
         vista.txt_nombrerol.setText("");
         }       
     }
     if(o.equals(vista.btn_buscar)){
         p.setCedula(vista.txt_cedula.getText());
      if(v.validacioncedula(p.getCedula())==true){  
        pda.buscarpersona(p);
      vista.txt_nombrescom.setText(p.getNombre());
      vista.txt_verclave.setText(v.desencriptar(secretKey,in.devolverundato("Select clave  from usuario where cedula='"+p.getCedula()+"'")));
      }
     }
     if(o.equals(vista.btn_buscarusuario)){
         p.setCedula(vista.txt_cedularol.getText());
      if(v.validacioncedula(p.getCedula())==true){  
        pda.buscarpersona(p);
      vista.txt_nombrecompleto.setText(p.getNombre());
      vista.txt_cargoactual.setText(in.devolverundato("SELECT cargo from rol,personarol WHERE rol.codigorol=personarol.codigorol and personarol.cedula='"+p.getCedula()+"'"));
      pda.rolesparadocentes(vista.jc_listanuevocargo);
      }
     }
     if(o.equals(vista.btn_modificarrol)){
     pr.setCedulaFK(vista.txt_cedularol.getText());
     pr.setCodigorolFK(Integer.parseInt(in.devolverundato("select codigorol from rol where cargo='"+vista.jc_listanuevocargo.getSelectedItem()+"'")));
     if(pda.modificargodocente(pr)==true){
     JOptionPane.showMessageDialog(null, "Modificado", "", JOptionPane.INFORMATION_MESSAGE);
     }
     }
        } catch (Exception ex) {
            System.out.println("hola"+ex.getMessage());
        }
   
    }

}
