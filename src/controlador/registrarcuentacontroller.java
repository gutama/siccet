
package controlador;

import bases.incrementocodigo;
import bases.mimagenes;
import bases.validaciones;
import dao.personaDao;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import javax.swing.Action;
import javax.swing.JOptionPane;
import modelo.persona;
import modelo.personarol;
import modelo.rol;
import modelo.usuario;
import vista.vregistrocuenta;

/**
 *
 * @author Legion
 */
public class registrarcuentacontroller  implements ActionListener{
vregistrocuenta vista;
     persona p=new persona();
    usuario u=new usuario();
    personarol pr=new personarol();
    rol r=new  rol();
    personaDao pda=new personaDao();
    validaciones v=new validaciones();
    incrementocodigo in=new incrementocodigo();
    public registrarcuentacontroller(vregistrocuenta vista) {
        this.vista = vista;
        //poner en modo escucha los botones
        vista.btn_guadar.addActionListener(this);
        vista.txt_añonacimiento.addActionListener(this);
        //metodos 
        validarcampos();
    }
 public String getFechaActual() {
        java.util.Date fecha = new java.util.Date();
        SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/YYYY");
        String año=formatoFecha.format(fecha).substring(6);
        return año;
    }
    public void iniciarvista(String fondos, mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
        i.insertarimagenesetiquetas(vista.fondo, fondos);
    }
    public  void  validarcampos(){
    v.ingresarsolomayusculas(vista.txt_nombres);
    v.validarnumeros(vista.txt_cedula);
    v.limitarcaracteres(vista.txt_cedula,10);
    v.limitarcaracteres(vista.txt_añonacimiento,4);
    v.validarnumeros(vista.txt_añonacimiento);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            String secretKey=" PROGRAMADORES GUTAMA Y TADAY";
            Object o=e.getSource();
            if(o.equals(vista.btn_guadar)){
            p.setCedula(vista.txt_cedula.getText());
            p.setNombre(vista.txt_nombres.getText());
            p.setAñonacimiento(vista.txt_añonacimiento.getText());
            if(vista.txt_contraseña.getText().equals(vista.txt_conficontraseña.getText())&& v.validacioncedula(p.getCedula())==true){
            u.setClave(v.encriptar(secretKey,vista.txt_contraseña.getText()));
            u.setCedulaFk(vista.txt_cedula.getText());
            r.setRol((String) vista.jc_listarroles.getSelectedItem());
            pda.guardar(p);
            pda.guardarusuario(u);
             pr.setCedulaFK(vista.txt_cedula.getText());
             pr.setCodigorolFK(Integer.parseInt(in.devolverundato("select codigorol from rol where cargo='"+vista.jc_listarroles.getSelectedItem()+"'")));
             pda.guardarpersonarol(pr);//guarda datos intermedios
            }else{
            JOptionPane.showMessageDialog(null, "No coinsiden las contraseñas", "", JOptionPane.ERROR_MESSAGE);
                vista.txt_contraseña.setText("");
                vista.txt_conficontraseña.setText("");
            }   
            }
            if(o.equals(vista.txt_añonacimiento)){
            int añonaciomiento=Integer.parseInt(vista.txt_añonacimiento.getText());
            int añoactual=Integer.parseInt(getFechaActual());
            int resUesta=añoactual-añonaciomiento;
               if(resUesta<21){
               vista.jc_listarroles.addItem("ESTUDIANTE");
               }else
                   pda.rolesparadocentes(vista.jc_listarroles);
            }
        } catch (Exception ex) {
        }
  
    }
    
}
