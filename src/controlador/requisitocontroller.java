
package controlador;

import bases.mimagenes;
import bases.validaciones;
import dao.gradoDao;
import dao.requisitoDao;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.gradoRequisitos;
import modelo.requsitosmatricula;
import vista.vrequisito;

/**
 *
 * @author Legion
 */
public class requisitocontroller implements ActionListener{
    gradoDao gdamo= new gradoDao();
    requsitosmatricula rmodelo=new requsitosmatricula();
    gradoRequisitos grmodelo=new gradoRequisitos();
    requisitoDao rdao=new requisitoDao();
    vrequisito vista;
    DefaultTableModel columna= new DefaultTableModel();
    validaciones v= new validaciones();
    public requisitocontroller(vrequisito vista) {
        this.vista = vista;
        gdamo.listargrado(vista.tbl_grados);
        vista.btn_agregar.addActionListener(this);
        vista.btn_quitar.addActionListener(this);
        vista.btn_guardar.addActionListener(this);
        cabecera();
        v.ingresarsolomayusculas(vista.txt_nombrerequisito);
    }

 public void eliminarfilas(DefaultTableModel model) {
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

  public void iniciarvista(String fondos,mimagenes i) {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
//        i.logoventanas(vista, "./imagenes/logoedu.jpg");
  i.insertarimagenesetiquetas(vista.fondo,fondos);
    }
  public  void cabecera(){
  columna.addColumn("CODIGO");
  columna.addColumn("GRADO");
  columna.addColumn("REQUISITOS");
  vista.tbl_asignar.setModel(columna);
  }
    
    @Override
    public void actionPerformed(ActionEvent ev) {
        Object o=ev.getSource();
        try {
            if(o.equals(vista.btn_agregar)){
                int selec=vista.tbl_grados.getSelectedRow();
                String dato[]=new String[6];
                 dato[0]=vista.tbl_grados.getValueAt(selec,0).toString();
                 dato[1]=vista.tbl_grados.getValueAt(selec,1).toString();
                 dato[2]=vista.txt_nombrerequisito.getText();
                columna.addRow(dato);
                vista.tbl_asignar.setModel(columna);
                vista.txt_nombrerequisito.setText("");
            }
            if(o.equals(vista.btn_guardar)){  
                for (int i = 0; i <vista.tbl_asignar.getRowCount(); i++) {
                      rmodelo.setNombre(vista.tbl_asignar.getValueAt(i,2).toString());
                      grmodelo.setCodigogradoFK(Integer.parseInt(vista.tbl_asignar.getValueAt(i,0).toString()));
                      grmodelo.setCodigorequisitoFK(rdao.guardarrequisito(rmodelo));    
                    rdao.guardarrequisitogrado(grmodelo);
                }
                 eliminarfilas(columna);
                JOptionPane.showMessageDialog(null, "guardado exitosamente", "", JOptionPane.INFORMATION_MESSAGE);
            }
            if(o.equals(vista.btn_quitar)){
                            int selec = vista.tbl_asignar.getSelectedRow();
                ((DefaultTableModel) vista.tbl_asignar.getModel()).removeRow(selec);
            }
        } catch (Exception e) {
            System.out.println("hola amigo el problema es"+e.getMessage());
        }
    
    }
    
}
