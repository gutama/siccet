/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import bases.coneccion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.areaAdministrativa;
import modelo.caliCualitativa;
import modelo.docenteArea;
import modelo.materia;
import modelo.periodoAcademico;

/**
 *
 * @author Legion
 */
public class areadministrativaDao {
    coneccion co;
    ResultSet re;
    PreparedStatement sq;
    public areadministrativaDao() {
          this.co = new coneccion();
    }
     public boolean guardar(areaAdministrativa ar){
       String z="INSERT INTO area(area) values(?)";
    try {
        sq =co.getConnection().prepareStatement(z);
       sq.setString(1,ar.getNombre());
         sq.executeUpdate();
                   
                 
co.desconectar();
return true;
    } catch (SQLException ex) {
  JOptionPane.showMessageDialog(null,ex);
    }
        return false;  
}
     public boolean guardardocente_area(docenteArea ar){
       String z="INSERT INTO docentearea(ceduladocente,codigoarea,codigoperiodo) values(?,?,?)";
    try {
        sq =co.getConnection().prepareStatement(z);
       sq.setString(1,ar.getCeduladocente());
       sq.setInt(2,ar.getCodigoarea());
       sq.setInt(3,ar.getCodigoperiodo());
         sq.executeUpdate();
                   
                 
co.desconectar();
return true;
    } catch (SQLException ex) {
  JOptionPane.showMessageDialog(null,ex);
    }
        return false;  
}
      public boolean modificar(areaAdministrativa m){
String z="update area set area=? where codigoarea=?";
try {
PreparedStatement sq =co.getConnection().prepareStatement(z);
sq.setString(1,m.getNombre());
sq.setInt(2,m.getCodigoarea());
    sq.executeUpdate();  
co.desconectar();
return  true;
    } catch (SQLException ex) {
    JOptionPane.showMessageDialog(null,ex);
    }
    return false;
} 
      
 public void cargarareas(JTable tabla){// metodo para cargar datos  de  la tabla materia
String da[]= new String[4];
DefaultTableModel modelo= new  DefaultTableModel();
modelo.addColumn("CODIGO");
modelo.addColumn("NOMBRE");
String a="select * from area";
        try {
            PreparedStatement sq =co.getConnection().prepareStatement(a);
            re=sq.executeQuery();
            while(re.next()){
            da[0]=String.valueOf(re.getInt("codigoarea"));
            da[1]=re.getString("area");
            modelo.addRow(da);
            }
            tabla.setModel(modelo);
            co.desconectar();
        } catch (SQLException ex) {
          
        }    
 }
                public void listarareas(JComboBox area,String ceduladocente) {// metodo para  cargar los  titulos  profecionales del docente
        String ca = "SELECT area  from area, docentearea where area.codigoarea=docentearea.codigoarea and docentearea.ceduladocente='"+ceduladocente+"'";
        Statement st;
        try {
            st = co.getConnection().createStatement();
            re = st.executeQuery(ca);
            while (re.next()) {
                area.addItem(re.getString("area"));
            }
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("error");
        }
    }
      public void listartodaslasareas(JComboBox area) {// metodo para cargar area.
        String ca = "SELECT area  from area";
                Statement st;
        try {
            st = co.getConnection().createStatement();
            re = st.executeQuery(ca);
            while (re.next()) {
                area.addItem(re.getString("area"));
            }
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("error");
        }
    }
}
