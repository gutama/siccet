package dao;

import bases.coneccion;
import java.sql.*;
import javax.swing.*;
import modelo.aula;
import modelo.aulacurso;

public class aulaDao {

    coneccion co;
    ResultSet re;

    public aulaDao() {
        this.co = new coneccion();
    }

    public void guardar(aula m) {
        String z = "INSERT INTO AULA(numeroaula,cupos) values(?,?)";
        try {
            PreparedStatement sq = co.getConnection().prepareStatement(z);
            sq.setString(1, m.getNumerodeaula());
            sq.setInt(2, m.getCupo());
            if (sq.executeUpdate() > 0) {
                JOptionPane.showMessageDialog(null, "guardado exitosamente", "", JOptionPane.INFORMATION_MESSAGE);
            }
            co.desconectar();
        } catch (SQLException ex) {
            System.out.println("error");
        }
    }
      public void guardarasignacionaulaycurso(aulacurso m) {
        String z = "INSERT INTO AULACURSO(codigocurso,codigoaula,codigoperiodo) values(?,?,?)";
        try {
            PreparedStatement sq = co.getConnection().prepareStatement(z);
            sq.setInt(1, m.getCodigocursofk());
            sq.setInt(2, m.getCodigoaulafk());
            sq.setInt(3,m.getCodigoperiodofk());
            if (sq.executeUpdate() > 0) {
                JOptionPane.showMessageDialog(null, "guardado exitosamente", "", JOptionPane.INFORMATION_MESSAGE);
            }
            co.desconectar();
        } catch (SQLException ex) {
            System.out.println("error");
        }
    }

    public void listarcupos(JComboBox titulos) {
        String ca = "select * from aula";
        Statement st;
        try {
            st = co.getConnection().createStatement();
            re = st.executeQuery(ca);
            while (re.next()) {
                titulos.addItem(re.getInt("cupos"));
            }
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("error");
        }
    }

      public void listaraulas(JComboBox aulas) {
        String ca = "select * from aula";
        Statement st;
        try {
            st = co.getConnection().createStatement();
            re = st.executeQuery(ca);
            while (re.next()) {
                aulas.addItem(re.getString("numeroaula"));
            }
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("error"+ex.getMessage());
        }
    }
    public boolean modificar(aula m) {
        String z = "update aula set cupos=? where numeroaula=?";
        System.out.println("query"+m.getCupo());
        try {
            PreparedStatement sq = co.getConnection().prepareStatement(z);
            sq.setInt(1, m.getCupo());
            sq.setString(2, m.getNumerodeaula());
            sq.executeUpdate();
            co.desconectar();
            return true;
        } catch (Exception ex) {
            System.out.println("error"+ex.getMessage());
        }
        return false;
    }

}
