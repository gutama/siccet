/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import bases.coneccion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.caliCualitativa;
import modelo.materia;
import modelo.periodoAcademico;

/**
 *
 * @author Legion
 */
public class calicualitativaDao {
    coneccion co;
    ResultSet re;
    PreparedStatement sq;
    public calicualitativaDao() {
          this.co = new coneccion();
    }
     public boolean guardar(caliCualitativa pa){
       String z="INSERT INTO NOTACUALITATIVA(equivalente) values(?)";
    try {
        sq =co.getConnection().prepareStatement(z);
       sq.setString(1,pa.getEquivalente());
         sq.executeUpdate();
                   
                 
co.desconectar();
return true;
    } catch (SQLException ex) {
  JOptionPane.showMessageDialog(null,ex);
    }
        return false;  
}
       public void cargarequvalentes(JTable tabla){// metodo para cargar datos  de  la tabla materia
String da[]= new String[4];
DefaultTableModel modelo= new  DefaultTableModel();
modelo.addColumn("CODIGO");
modelo.addColumn("NOMBRE");
String a="select * from notacualitativa order by codigonotacualitativa asc";
        try {
            PreparedStatement sq =co.getConnection().prepareStatement(a);
            re=sq.executeQuery();
            while(re.next()){
            da[0]=String.valueOf(re.getInt("codigonotacualitativa"));
            da[1]=re.getString("equivalente");
            modelo.addRow(da);
            }
            tabla.setModel(modelo);
            co.desconectar();
        } catch (SQLException ex) {
          
        }    
 }
}
