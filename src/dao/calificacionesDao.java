
package dao;

import bases.coneccion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.caliCuantitativa;
import modelo.docente;
import modelo.estudiante;
import modelo.matricula;

/**
 *
 * @author Legion
 */
public class calificacionesDao {
       coneccion co;
    ResultSet re;
    PreparedStatement sq;

    public calificacionesDao() {
        this.co = new coneccion();
    }
    
    public void guardar(caliCuantitativa m) {
        String z = "INSERT INTO NOTACUANTITATIVA(quimestre1,quimestre2,promedioanual,comportamiento,codigonotacualitativa,codigomatricula,codigomateria) values(?,?,?,?,?,?,?)";
        try {
             sq = co.getConnection().prepareStatement(z);
            sq.setDouble(1, m.getQuimestre1());
            sq.setDouble(2, m.getQuimestre2());
            sq.setDouble(3,m.getPromedioanual());
            sq.setString(4,m.getComportamiento());
            sq.setInt(5,m.getCodigonotacualitativaFK());
            sq.setInt(6,m.getCodigomatriculaFK());
            sq.setInt(7,m.getCodigomateriaFK());
            sq.executeUpdate();
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("error"+ex.getMessage());
        }
    }
    public boolean modificar(caliCuantitativa c,matricula m) {
        String z = "UPDATE NOTACUANTITATIVA,matricula SET quimestre1=?, quimestre2=?,promedioanual=?, comportamiento=?,codigonotacualitativa=? WHERE notacuantitativa.codigomatricula=matricula.codigomatricula and matricula.cedulaestudiante=? and  notacuantitativa.codigomateria=? and matricula.codigoperiodo in  (SELECT max(codigoperiodo) from periodoacademico)";
        try {
            sq = co.getConnection().prepareStatement(z);
            sq.setDouble(1,c.getQuimestre1());
            sq.setDouble(2,c.getQuimestre2());
            sq.setDouble(3,c.getPromedioanual());
            sq.setString(4,c.getComportamiento());
            sq.setInt(5,c.getCodigonotacualitativaFK());
            sq.setString(6,m.getCedulaestudianteFK());
            sq.setInt(7,c.getCodigomateriaFK());
            sq.executeUpdate();
            co.desconectar();
            return true;
        } catch (SQLException ex) {
            System.out.println("error"+ex.getMessage());
        }
        return false;
    }
    public boolean buscar(caliCuantitativa c,matricula m) {// metodo para buscar  los datos personales del docente
        String buscar = "select * from notacuantitativa,matricula where notacuantitativa.codigomatricula=matricula.codigomatricula and matricula.cedulaestudiante=? and  notacuantitativa.codigomateria=?";
        try {
            sq = co.getConnection().prepareStatement(buscar);
            sq.setString(1,m.getCedulaestudianteFK());
            sq.setInt(2,c.getCodigomateriaFK());
            re = sq.executeQuery();
            if (re.next()) {
                c.setQuimestre1(re.getDouble("quimestre1"));
                c.setQuimestre2(re.getDouble("quimestre2"));
                c.setComportamiento(re.getString("comportamiento"));
            } else {
                JOptionPane.showMessageDialog(null, "No hay calificaciones", "¡Adventecia!", JOptionPane.WARNING_MESSAGE);
            }
            co.desconectar();
            return true;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return false;
    }
     public void cargarCALIFICACIONES(JTable tabla, estudiante e) {// cargar datos para asignar  las materias  a cada docente y  cada  grado
        String da[] = new String[6];
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("AREA");
        modelo.addColumn("ASIGNATURA");
        modelo.addColumn("CALIFICACION CUANTITATIVA");
        modelo.addColumn("CALIFICACION"
                + "CUALITATIVA");
        String a = "SELECT m.nombre, ap.nombre, promedioanual, equivalente  from  matricula, notacuantitativa, notacualitativa, materia m, areapedagogica ap where matricula.codigomatricula=notacuantitativa.codigomatricula and ap.idareaPedagogica=m.idareapedagogica and notacuantitativa.codigonotacualitativa=notacualitativa.codigonotacualitativa and matricula.cedulaestudiante=? and notacuantitativa.codigomateria=m.codigomateria and matricula.codigoperiodo in (SELECT max(codigoperiodo) from periodoacademico)";
        try {
            PreparedStatement sq = co.getConnection().prepareStatement(a);
            sq.setString(1,e.getCedula());
            re = sq.executeQuery();
            while (re.next()) {
                da[0] = String.valueOf(re.getString("ap.nombre"));
                da[1] = String.valueOf(re.getString("m.nombre"));
                da[2] = re.getString("promedioanual");
                da[3] = re.getString("equivalente");
                modelo.addRow(da);
            }
            tabla.setModel(modelo);
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("no vale" + ex.getMessage());
        }
    }
      public void vercalificaciones(JTable tabla, matricula m,DefaultTableModel modelo ) {// cargar datos para asignar  las materias  a cada docente y  cada  grado
        String da[] = new String[6];
        String a = "SELECT m.nombre, ap.nombre,quimestre1,quimestre2, promedioanual, equivalente  from  matricula, notacuantitativa, notacualitativa, materia m, areapedagogica ap "
                + "where matricula.codigomatricula=notacuantitativa.codigomatricula and ap.idareaPedagogica=m.idareapedagogica and notacuantitativa.codigonotacualitativa=notacualitativa.codigonotacualitativa and matricula.cedulaestudiante=? and notacuantitativa.codigomateria=m.codigomateria and matricula.codigocurso=?";
        try {
            PreparedStatement sq = co.getConnection().prepareStatement(a);
            sq.setString(1,m.getCedulaestudianteFK());
            sq.setInt(2,m.getCodigocursoFk());
            re = sq.executeQuery();
            while (re.next()) {
                da[0] = String.valueOf(re.getString("ap.nombre"));
                da[1] = String.valueOf(re.getString("m.nombre"));
                da[2] = re.getString("quimestre1");
                da[3] = re.getString("quimestre2");
                da[4] = re.getString("promedioanual");
                da[5] = re.getString("equivalente");
                modelo.addRow(da);
            }
            tabla.setModel(modelo);
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("no vale" + ex.getMessage());
        }
    }
}
