package dao;

import bases.coneccion;
import java.sql.*;
import javax.swing.*;
import java.util.logging.*;
import javax.swing.table.DefaultTableModel;
import modelo.docente;
import modelo.docenteDocumento;
import modelo.docentecurso;
import modelo.docenteMateria;
import modelo.grado;

/**
 *
 * @author Mesias
 */
public class docenteDao {

    coneccion co;
    ResultSet re;
    PreparedStatement sq;

    public docenteDao() {//costructor
        this.co = new coneccion();
    }

    public boolean guardar(docente d) {
        try {
            String z = "INSERT INTO DOCENTE VALUES('" + d.getCedula() + "','" + d.getTelefono() + "','" + d.getCorreo() + "','" + d.getNombre() + "','" + d.getApellido() + "','activo')";
            Statement q = co.getConnection().createStatement();
            q.execute(z);
            co.desconectar();
            return true;
        } catch (Exception ex) {

        }
        return false;
    }

    public boolean guardardocumentodocente(docenteDocumento d) {
        try {
            String a = "INSERT INTO DOCENTEDOCUMENTO (ceduladocente,codigodocumento)VALUES(?,?)";
            sq = co.getConnection().prepareStatement(a);
            sq.setString(1, d.getCeduladocente());
            sq.setInt(2, d.getCodigodocumento());
            sq.executeUpdate();
            co.desconectar();
            return true;
        } catch (Exception ex) {
            System.out.println("error" + ex.getMessage());
        }
        return false;
    }

    public boolean buscar(docente d) {// metodo para buscar  los datos personales del docente
        String buscar = "select * from docente where ceduladocente=? and estado='activo'";
        try {
            sq = co.getConnection().prepareStatement(buscar);
            sq.setString(1, d.getCedula());
            re = sq.executeQuery();
            if (re.next()) {
                d.setNombre(re.getString("nombre"));
                d.setApellido(re.getString("apellido"));
                d.setTelefono(re.getString("telefono"));
                d.setCorreo(re.getString("correo"));
            } else {
                JOptionPane.showMessageDialog(null, "Docente no registrado", "¡Adventecia!", JOptionPane.WARNING_MESSAGE);
            }
            co.desconectar();
            return true;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return false;
    }

    public void listardocentes(JTable tabla) {// metodo para cargar datos  de  la tabla materia
        String da[] = new String[6];
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("CEDULA");
        modelo.addColumn("NOMBRES Y APELLIDOS");

        String a = "select * from docente where estado='activo'";
        try {
            PreparedStatement sq = co.getConnection().prepareStatement(a);
            re = sq.executeQuery();
            while (re.next()) {
                da[0] = re.getString("CEDULADOCENTE");
                da[1] = re.getString("NOMBRE") + " " + re.getString("APELLIDO");
                modelo.addRow(da);
            }
            tabla.setModel(modelo);
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("no vale" + ex.getMessage());
        }
    }

    public boolean modificar(docente doce) {
        String z = "UPDATE DOCENTE SET nombre=?, apellido=?,telefono=?, correo=? WHERE ceduladocente=?";
        try {
            sq = co.getConnection().prepareStatement(z);
            sq.setString(1, doce.getNombre());
            sq.setString(2, doce.getApellido());
            sq.setString(3, doce.getTelefono());
            sq.setString(4, doce.getCorreo());
            sq.setString(5, doce.getCedula());
            sq.executeUpdate();
            co.desconectar();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return false;
    }

    public boolean eliminar(String cedula) {
        String z = "UPDATE DOCENTE SET estado='eliminado' where ceduladocente='" + cedula + "'";
        try {
            Statement s = co.getConnection().createStatement();
            s.execute(z);
            co.desconectar();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return false;
    }

    public boolean docente_materia(docenteMateria dg) {
        String a = "INSERT INTO DOCENTEMATERIA(ceduladocente,codigomateria,codigoperiodo)VALUES(?,?,?)";
        try {
            sq = co.getConnection().prepareStatement(a);
            sq.setString(1,dg.getCeduladocenteFK());
            sq.setInt(2,dg.getCodigomateriaFK());
            sq.setInt(3,dg.getCodigoperiodoFK());
            sq.executeUpdate();
            co.desconectar();
            return true;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        return false;
    }
      public boolean docenteCurso(docentecurso dg) {
        String a = "INSERT INTO DOCENTECURSO(ceduladocente,tipo,codigocurso,codigomateria,codigoperiodo)VALUES(?,?,?,?,?)";
        try {
            sq = co.getConnection().prepareStatement(a);
            sq.setString(1,dg.getCeduladocenteFK());
            sq.setString(2, dg.getTipo());
            sq.setInt(3,dg.getCodigogradoFK());
            sq.setInt(4,dg.getCodigomateriaFK());
            sq.setInt(5,dg.getCodigoperiodoFK());
            sq.executeUpdate();
            co.desconectar();
            return true;
        } catch (SQLException ex) {
            System.out.println("error en"+ex.getMessage());
        }
        return false;
    }
             public void listarcursosdocente(JTable tabla,docentecurso dc,DefaultTableModel model) {// metodo para  cargar los  titulos  profecionales del docente
          Object datos[]=new Object[4];
        String ca = "SELECT vista_cursos.codigocurso,vista_cursos.nombre,vista_cursos.tipo,vista_cursos.paralelo from vista_cursos,docentecurso WHERE docentecurso.codigocurso=vista_cursos.codigocurso and docentecurso.ceduladocente='"+dc.getCeduladocenteFK()+"'and docentecurso.codigomateria="+dc.getCodigomateriaFK()+"  GROUP BY vista_cursos.nombre";
        Statement st;
        try {
            st = co.getConnection().createStatement();
            re = st.executeQuery(ca);
             while (re.next()) {
                 datos[0]=re.getInt("vista_cursos.codigocurso");
                 datos[1]=re.getString("vista_cursos.nombre");
                 datos[2]=re.getString("vista_cursos.tipo");
                 datos[3]=re.getString("vista_cursos.paralelo");
                 model.addRow(datos);
            } 
             tabla.setModel(model);
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("error");
        }
    }
}
