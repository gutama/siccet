
package dao;


import bases.coneccion;
import java.awt.Image;
import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import modelo.documento;
/**
 * 11 de  agosto del 2021
 * @author Mesias
 */
public class documentoDao {
    coneccion conec;
  PreparedStatement ps= null;
  ResultSet rs= null;

    public documentoDao() {
        this.conec =  new coneccion();
    }
  

  public ArrayList<documento> listar_pdf(){
     ArrayList<documento> lis= new ArrayList<documento>();
     String sql="select * from documento"; 
      try {
          ps=conec.getConnection().prepareStatement(sql);
          rs=ps.executeQuery(); 
          while(rs.next()){                   
          documento vo= new documento();
          vo.setCodigodocumento(rs.getString(1)); 
          vo.setNombrepdf(rs.getString(2));
          vo.setArchivopdf(rs.getBytes(3));
          lis.add(vo);
          }
          ps.close();
          ps.close();
          conec.desconectar();
      } catch (SQLException ex) {
          System.out.println(""+ex.getMessage());
      }
      return lis;
  } 
   public Image get_Image(String ruta) {
        ImageIcon a = new ImageIcon(getClass().getResource(ruta));
        Image bo = a.getImage();
        return bo;
    }
 
   public boolean recuperararchivo(String cedula, documento vo){
     String sql="select archivo from docente,documento,docentedocumento where documento.codigodocumento=docentedocumento.codigodocumento and "
             + "docente.ceduladocente=docentedocumento.ceduladocente and docente.ceduladocente='"+cedula+"' and documento.nombre='hoja de vida'"; 
     try {
          ps=conec.getConnection().prepareStatement(sql);
          rs=ps.executeQuery(); 
          while(rs.next()){                   
          vo.setArchivopdf(rs.getBytes("archivo"));
          }
          ps.close();
          conec.desconectar();
          return true;
      } catch (Exception ex) {
          System.out.println("hola que tal"+ex.getMessage());
      }
        return false;
  }  
    
  public int agregar_pdfVO(documento vo ){
      int codigo=0;
  String a="INSERT INTO documento(nombre,archivo) VALUES (?,?)";
      try {
          ps=conec.getConnection().prepareStatement(a);
          ps.setString(1,vo.getNombrepdf());
          ps.setBytes(2,vo.getArchivopdf());
          ps.executeUpdate();
         rs = ps.getGeneratedKeys();
         if (rs.next()) 
           codigo=rs.getInt(1);
 
          conec.desconectar();
          return codigo;
      } catch (Exception ex) {
      }
        return codigo;
  }
  public void modificar_doc2(documento vo ){
  String a="UPDATE documento SET nombre=? where codigodocumento=?";
      try {
          ps=conec.getConnection().prepareStatement(a);
          ps.setString(1,vo.getNombrepdf());
          ps.setString(2,vo.getCodigodocumento());     
          ps.executeUpdate();
          ps.close();
          conec.desconectar();
      } catch (SQLException ex) {
        
      }
  }
public void eliminar(documento vo, String cedula ){
  String a="DELETE a1,a2 FROM estudiantedocumento a1,documento a2\n" +
"WHERE a1.codigodocumento=a2.codigodocumento AND a1.cedulaestudiante=? and a1.codigodocumento=?";
      try {
         ps=conec.getConnection().prepareStatement(a);
         ps.setString(1,cedula);
         ps.setInt(2,Integer.parseInt(vo.getCodigodocumento()));
         ps.execute();
          conec.desconectar();
      } catch (SQLException ex) {
         
      }
  } 
public void modificar_do(documento vo ){
  String a="UPDATE documento SET nombre=?,archivo=? where codigodocumento=?";
      try {
          ps=conec.getConnection().prepareStatement(a);
          ps.setString(1,vo.getNombrepdf());
          ps.setBytes(2,vo.getArchivopdf());
          ps.setString(3,vo.getCodigodocumento());     
          ps.executeUpdate();
          ps.close();
          conec.desconectar();
      } catch (SQLException ex) {
          Logger.getLogger(documentoDao.class.getName()).log(Level.SEVERE, null, ex);
      }
  }  

public void ejecutar_archivo(documento vo){
byte [] b= null;
String a="select archivo from documento where archivo=?";
      try {
          ps=conec.getConnection().prepareStatement(a);
          ps.setBytes(1,vo.getArchivopdf());
          rs=ps.executeQuery();
          while(rs.next()){
          b=rs.getBytes(1);
          }
          InputStream bos= new ByteArrayInputStream(b);
          int tamaño=bos.available();
          byte [] datos= new byte[tamaño];
          bos.read(datos,0,tamaño);
          OutputStream ou= new FileOutputStream("new.pdf"); 
          ou.write(datos);
          ou.close();
          ps.close();
          rs.close();
          conec.desconectar();
      } catch (Exception ex) {
      } 
}
}
