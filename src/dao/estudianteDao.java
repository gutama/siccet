package dao;

import bases.coneccion;
import java.sql.*;
import javax.swing.*;
import java.net.URL;
import javax.swing.table.DefaultTableModel;
import modelo.estudiante;
import modelo.estudianteDocumento;
import modelo.familiarcercano;
import modelo.grado;
import modelo.representante;


/**
 *
 * @author Mesias
 */
public class estudianteDao {

    coneccion co;
    ResultSet re;

    public estudianteDao() {
        this.co = new coneccion();
    }

    public boolean guardar(estudiante es) {
        try {
            String z = "INSERT INTO ESTUDIANTE VALUES ('" + es.getCedula() + "','" + es.getNombre() + "','" + es.getApellido() + "','" + es.getFechanacimiento() + "',"
                    + "'" + es.getTelefono() + "','" + es.getCorreo() + "','" + es.getCedularepresentanteFK() + "','activo')";
            Statement sq = co.getConnection().createStatement();
            sq.execute(z);
            JOptionPane.showMessageDialog(null, "Guardado exitosamente", "", JOptionPane.INFORMATION_MESSAGE);
            co.desconectar();
            return true;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return false;

    }

    public boolean guardarestudiantedocumento(estudianteDocumento es) {
        try {
            String z = "INSERT INTO estudiantedocumento(codigodocumento,cedulaestudiante) values(?,?)";
            PreparedStatement sq = co.getConnection().prepareStatement(z);
            sq.setInt(1, es.getCodigodocumento());
            sq.setString(2, es.getCedulaestudiante());
            sq.executeUpdate();
            co.desconectar();
            return true;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        return false;

    }

    public boolean modificar(estudiante es) {
        try {
            String z = "update estudiante set nombre=?, apellido=?,fechanacimiento=?,telefono=?,correo=? where estado='activo' and cedulaestudiante=?";
            PreparedStatement sq = co.getConnection().prepareStatement(z);
            sq.setString(1, es.getNombre());
            sq.setString(2, es.getApellido());
            sq.setString(3, es.getFechanacimiento());
            sq.setString(4, es.getTelefono());
            sq.setString(5, es.getCorreo());
            sq.setString(6, es.getCedula());
            sq.executeUpdate();
            co.desconectar();
            return true;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return false;
    }

    public boolean listar(JTable tabla, DefaultTableModel model) {
        model.addColumn("CEDULA");
        model.addColumn("NOMBRES");
        model.addColumn("APELLIDOS");
        try {
            String[] x = new String[4];
            String z = "select * from estudiante where estado='activo'";
            PreparedStatement sq = co.getConnection().prepareStatement(z);
            re = sq.executeQuery();
            while (re.next()) {
                x[0] = re.getString("cedulaestudiante");
                x[1] = re.getString("nombre");
                x[2] = re.getString("apellido");
                model.addRow(x);
            }
            tabla.setModel(model);
            co.desconectar();
            return true;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return false;
    }

    public boolean eliminar(estudiante e) {
        String z = "UPDATE estudiante set estado='eliminado' where cedulaestudiante=?";
        try {
            PreparedStatement sq = co.getConnection().prepareStatement(z);
            sq.setString(1, e.getCedula());
            sq.executeUpdate();
            co.desconectar();
            return true;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return false;
    }


    public boolean buscar(estudiante e) {
        try {
            String z = "select * from estudiante  where estado='activo' and cedulaestudiante=?";
            PreparedStatement sq = co.getConnection().prepareStatement(z);
            sq.setString(1, e.getCedula());
            re = sq.executeQuery();
            if (re.next()) {
                e.setNombre(re.getString("nombre"));
                e.setApellido(re.getString("apellido"));
                e.setCorreo(re.getString("correo"));
                e.setFechanacimiento(re.getString("fechanacimiento"));
                e.setTelefono(re.getString("telefono"));
            } else {
                JOptionPane.showMessageDialog(null, "Estudiante no registrado", "¡Avertencia¡", JOptionPane.WARNING_MESSAGE);
            }
            co.desconectar();
            return true;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return false;
    }
        public boolean buscarestudiante(estudiante e,representante r,familiarcercano f) {
        try {
            String z = "select e.nombre,e.apellido,e.correo,e.fechanacimiento,e.telefono,r.cedularepresentante,r.nombre,r.apellido,r.email,r.direccion, f.direccion,f.telefono,f.codigofamiliar from estudiante e, representante r,familiarcercano f where  r.codigofamiliar=f.codigofamiliar and e.cedularepresentante=r.cedularepresentante and e.estado='activo' and e.cedulaestudiante=?";
            PreparedStatement sq = co.getConnection().prepareStatement(z);
            sq.setString(1, e.getCedula());
            re = sq.executeQuery();
            if (re.next()) {
                e.setNombre(re.getString("e.nombre"));
                e.setApellido(re.getString("e.apellido"));
                e.setCorreo(re.getString("e.correo"));
                e.setFechanacimiento(re.getString("e.fechanacimiento"));
                e.setTelefono(re.getString("e.telefono"));
                r.setCedula(re.getString("r.cedularepresentante"));
                r.setNombre(re.getString("r.nombre"));
                r.setApellido(re.getString("r.apellido"));
                r.setCorreo(re.getString("r.email"));
                r.setDireccion(re.getString("r.direccion")); 
                f.setDireccion(re.getString("f.direccion"));
                f.setTelefono(re.getString("f.telefono"));
                f.setCodigofamiliar(re.getInt("f.codigofamiliar"));
            } else {
                JOptionPane.showMessageDialog(null, "Estudiante no registrado", "¡Avertencia¡", JOptionPane.WARNING_MESSAGE);
            }
            co.desconectar();
            return true;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return false;
    }
    public boolean buscarestudiantecurso(estudiante e, grado g) {
        try {
            String z = "SELECT e.nombre,apellido,vc.nombre as grado, tipo FROM vista_cursos vc, estudiante e, matricula m WHERE e.cedulaestudiante=m.cedulaestudiante and m.codigocurso=vc.codigocurso and m.codigoperiodo in (SELECT max(codigoperiodo) from periodoacademico) and e.cedulaestudiante=?";
            PreparedStatement sq = co.getConnection().prepareStatement(z);
            sq.setString(1, e.getCedula());
            re = sq.executeQuery();
            if (re.next()) {
                e.setNombre(re.getString("e.nombre"));
                e.setApellido(re.getString("apellido"));
                g.setNombre(re.getString("grado"));
                g.setTipo(re.getString("tipo"));
            } else {
                JOptionPane.showMessageDialog(null, "Estudiante no registrado", "¡Avertencia¡", JOptionPane.WARNING_MESSAGE);
            }
            co.desconectar();
            return true;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return false;
    }
}

//select * from horario,persona p, usuario where p.cedula=horario.cedula and p.cedula=usuario.cedula and p.estado='A' or p.estado='i';
