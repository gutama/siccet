package dao;

import bases.coneccion;
import java.sql.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import modelo.curso;
import modelo.grado;
import modelo.gradoMateria;

public class gradoDao {

    coneccion co;
    ResultSet re;
PreparedStatement sq;
    public gradoDao() {
        this.co = new coneccion();
    }

    public void guardar(grado g) {
        String z = "INSERT INTO grado(nombre,tipo) values(?,?)";
        try {
           sq = co.getConnection().prepareStatement(z);
            sq.setString(1, g.getNombre());
            sq.setString(2, g.getTipo());
            if (sq.executeUpdate() > 0) {
                JOptionPane.showMessageDialog(null, "guardado exitosamente", "", JOptionPane.INFORMATION_MESSAGE);
            }
            co.desconectar();
        } catch (SQLException ex) {
            System.out.println("error");
        }
    }

    public void guardarmallaacademica(gradoMateria g) {
        String z = "INSERT INTO gradomateria(codigomateria,numerohoras,codigogrado) values(?,?,?)";
        try {
             sq = co.getConnection().prepareStatement(z);
            sq.setInt(1, g.getCodigomateria());
            sq.setInt(2, g.getNumerohoras());
            sq.setInt(3, g.getCodigogrado());
            if (sq.executeUpdate() > 0) {
                JOptionPane.showMessageDialog(null, "guardado exitosamente", "", JOptionPane.INFORMATION_MESSAGE);
            }
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("error");
        }
    }

    public boolean guadarCursos(curso a) {
        String z = "INSERT INTO curso(codigogrado,codigojornada,codigoparalelo) values(?,?,?)";
        try {
            sq = co.getConnection().prepareStatement(z);
            sq.setInt(1, a.getCodigogradoFK());
            sq.setInt(2, a.getCodigojornadaFK());
            sq.setInt(3, a.getCodigoparaleloFK());
            sq.executeUpdate();
            co.desconectar();
            return true;
        } catch (SQLException ex) {
            System.out.println("error" + ex.getMessage());
        }
        return false;
    }

    public void listargrados(JComboBox grado) {
        String ca = "select DISTINCT nombre from grado";
        Statement st;
        try {
            st = co.getConnection().createStatement();
            re = st.executeQuery(ca);
            while (re.next()) {
                grado.addItem(re.getString("nombre"));
            }
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("error");
        }
    }

    public void listargradosconcatenados(JComboBox grado) {
        String ca = "select * from grado";
        Statement st;
        try {
            st = co.getConnection().createStatement();
            re = st.executeQuery(ca);
            while (re.next()) {
                grado.addItem(re.getString("nombre") + " " + re.getString("tipo"));
            }
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("error");
        }
    }


    public void cargargrados(JTable tabla) {// cargar datos para asignar  las materias  a cada docente y  cada  grado
        String da[] = new String[6];
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("CODIGO CURSO");
        modelo.addColumn("CODIGO GRADO");
        modelo.addColumn("NOMBRE");
        modelo.addColumn("TIPO");
        modelo.addColumn("JORNADA");
        modelo.addColumn("PARALELO");
        String a = "select * from vista_cursos";
        try {
            PreparedStatement sq = co.getConnection().prepareStatement(a);
            re = sq.executeQuery();
            while (re.next()) {
                da[0] = String.valueOf(re.getInt("CODIGOCURSO"));
                da[1] = String.valueOf(re.getInt("CODIGOGRADO"));
                da[2] = re.getString("NOMBRE");
                da[3] = re.getString("TIPO");
                da[4] = re.getString("JORNADA");
                da[5] = re.getString("PARALELO");
                modelo.addRow(da);
            }
            tabla.setModel(modelo);
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("no vale" + ex.getMessage());
        }
    }

    public void nuevamatricula(JTable tabla) {// cargar los cursos para nueva matricula 
        String da[] = new String[6];
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("CODIGO CURSO");
        modelo.addColumn("NOMBRE");
        modelo.addColumn("TIPO");
        modelo.addColumn("JORNADA");
        modelo.addColumn("PARALELO");
        String a = "SELECT v.codigocurso, nombre,tipo,paralelo,jornada from vista_cursos v,aulacurso ac where v.codigocurso=ac.codigocurso AND ac.codigoperiodo IN (SELECT MAX(codigoperiodo)FROM periodoacademico) GROUP BY nombre";
        try {
            PreparedStatement sq = co.getConnection().prepareStatement(a);
            re = sq.executeQuery();
            while (re.next()) {
                da[0] = String.valueOf(re.getInt("v.CODIGOCURSO"));
                da[1] = re.getString("NOMBRE");
                da[2] = re.getString("TIPO");
                da[3] = re.getString("JORNADA");
                da[4] = re.getString("PARALELO");
                modelo.addRow(da);
            }
            tabla.setModel(modelo);
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("no vale" + ex.getMessage());
        }
    }
 public void listargrado(JTable tabla) {// cargar los cursos para nueva matricula 
        String da[] = new String[4];
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("CODIGO");
        modelo.addColumn("GRADO");
        modelo.addColumn("TIPO");
        String a = "SELECT * FROM GRADO";
        try {
            PreparedStatement sq = co.getConnection().prepareStatement(a);
            re = sq.executeQuery();
            while (re.next()) {
                da[0] = String.valueOf(re.getInt("CODIGOGRADO"));
                da[1] = re.getString("NOMBRE");
                da[2] = re.getString("TIPO");
                modelo.addRow(da);
            }
            tabla.setModel(modelo);
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("no vale" + ex.getMessage());
        }
    }
    public void visualizarmalla(JTable tabla, grado g) {// cargar la malla academica.
        String da[] = new String[4];
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("GRADOS");
        modelo.addColumn("MATERIAS");
        modelo.addColumn("NUMERO DE  HORAS");
        String a = "SELECT grado.nombre, materia.nombre,numerohoras FROM gradomateria,grado,materia WHERE gradomateria.codigogrado=grado.codigogrado AND grado.codigogrado=?  and grado.tipo=? and materia.codigomateria=gradomateria.codigomateria";
        try {
            PreparedStatement sq = co.getConnection().prepareStatement(a);
            sq.setInt(1, g.getCodigogrado());
            sq.setString(2, g.getTipo());
            re = sq.executeQuery();
            while (re.next()) {
                da[0] = re.getString("grado.nombre");
                da[1] = re.getString("materia.nombre");
                da[2] = re.getString("numerohoras");
                modelo.addRow(da);
            }
            tabla.setModel(modelo);
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("no vale" + ex.getMessage());
        }
    }

    public void matricularenuncurso(JTable tabla) {// cragar los datos  para matricular al estudiante en un curso
        String da[] = new String[6];
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("CODIGO CURSO");
        modelo.addColumn("NOMBRE");
        modelo.addColumn("TIPO");
        modelo.addColumn("JORNADA");
        modelo.addColumn("PARALELO");
        String a = "select * from vista_cursos";
        try {
            PreparedStatement sq = co.getConnection().prepareStatement(a);
            re = sq.executeQuery();
            while (re.next()) {
                da[0] = String.valueOf(re.getInt("CODIGOCURSO"));
                da[1] = re.getString("NOMBRE");
                da[2] = re.getString("TIPO");
                da[3] = re.getString("JORNADA");
                da[4] = re.getString("PARALELO");
                modelo.addRow(da);
            }
            tabla.setModel(modelo);
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("no vale" + ex.getMessage());
        }
    }

    public void listarcursosparaparticipacion(JTable tabla) {// cragar los datos  para matricular al estudiante en un curso
        String da[] = new String[6];
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("CODIGO CURSO");
        modelo.addColumn("NOMBRE");
        modelo.addColumn("TIPO");
        modelo.addColumn("JORNADA");
        modelo.addColumn("PARALELO");
        String a = "SELECT vista_cursos.codigocurso,nombre,tipo,jornada,paralelo FROM  vista_cursos,matricula m WHERE  m.codigocurso=vista_cursos.codigocurso and nombre=' PRIMERO ' OR nombre=' SEGUNDO 'and m.codigoperiodo in (SELECT\n"
                + "               MAX(codigoperiodo) as codigo\n"
                + "          FROM periodoacademico)GROUP BY tipo,jornada,paralelo";
        try {
            PreparedStatement sq = co.getConnection().prepareStatement(a);
            re = sq.executeQuery();
            while (re.next()) {
                da[0] = String.valueOf(re.getInt("vista_cursos.codigocurso"));
                da[1] = re.getString("NOMBRE");
                da[2] = re.getString("TIPO");
                da[3] = re.getString("JORNADA");
                da[4] = re.getString("PARALELO");
                modelo.addRow(da);
            }
            tabla.setModel(modelo);
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("no vale" + ex.getMessage());
        }
    }


    public boolean eliminar(grado m) {
        String z = "DELETE FROM grado WHERE codigogrado = ?";
        try {
            PreparedStatement sq = co.getConnection().prepareStatement(z);
            sq.setInt(1, m.getCodigogrado());
            sq.executeUpdate();
            co.desconectar();
            return true;
        } catch (Exception ex) {
            System.out.println("error" + ex.getMessage());
        }
        return false;
    }

}
