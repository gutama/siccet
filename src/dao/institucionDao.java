
package dao;

import modelo.*;
import bases.coneccion;
import java.sql.*;
import javax.swing.*;



/**
 *
 * @author Mesias
 */
public class institucionDao { 
coneccion co;
ResultSet re;
    public institucionDao() {
    co=new coneccion();
    }
public void guardar(institucion i){
String z="INSERT INTO INSTITUCION (codigoamie,nombre,direccion,regimen,telefono,correo,estado) VALUES(?,?,?,?,?,?,'activo')";            
try {
        PreparedStatement sq =co.getConnection().prepareStatement(z);
       sq.setString(1,i.getCodigo());
       sq.setString(2,i.getNombre());
       sq.setString(3,i.getSector());
       sq.setString(4,i.getRegimen());
       sq.setString(5,i.getTelefono());
       sq.setString(6,i.getCorreo());
       if(sq.executeUpdate()>0){
                     JOptionPane.showMessageDialog(null,"guardado exitosamente","",JOptionPane.INFORMATION_MESSAGE);
                  }
         co.desconectar();
    } catch (Exception ex) {
  JOptionPane.showMessageDialog(null,ex.getMessage());
    }
    }
 public void modificar(institucion i){
       String z="UPDATE INSTITUCION SET nombre=?,direccion=?, regimen=?,telefono=?,correo=? where codigoamie=? and  estado='activo'" ;
    try {
        PreparedStatement sq =co.getConnection().prepareStatement(z);  
        sq.setString(1,i.getNombre());
        sq.setString(2,i.getSector());
        sq.setString(3,i.getRegimen());
        sq.setString(4,i.getTelefono());
        sq.setString(5,i.getCorreo());
         sq.setString(6,i.getCodigo());
         if(sq.executeUpdate()>0){
                     JOptionPane.showMessageDialog(null,"Modificado exitosamente","",JOptionPane.INFORMATION_MESSAGE);
                  }
co.desconectar();
    } catch (SQLException ex) {
  JOptionPane.showMessageDialog(null,ex.getMessage());
    }
    }
   public boolean buscar(institucion e){
   String z="select * from institucion where codigoamie= ? and  estado='activo'" ;
    try {
        PreparedStatement sq =co.getConnection().prepareStatement(z); 
        sq.setString(1,e.getCodigo());
        re=sq.executeQuery();
        if(re.next()){
        e.setCodigo(re.getString("codigoamie"));
        e.setNombre(re.getString("nombre"));
        e.setTelefono(re.getString("telefono"));
        e.setCorreo(re.getString("correo"));
        e.setSector(re.getString("direccion"));
        e.setCorreo(re.getString("correo"));
        e.setRegimen(re.getString("regimen"));
        }else
           JOptionPane.showMessageDialog(null,"no se encuentra registrado");   
                co.desconectar();
                return true;
                  } catch (Exception ex) {
  JOptionPane.showMessageDialog(null,ex);
    }
    return false;
   }
 public boolean datosdelainstitucion(institucion e){
   String z="select * from institucion where estado='activo'" ;
    try {
        PreparedStatement sq =co.getConnection().prepareStatement(z); 
        re=sq.executeQuery();
        if(re.next()){
        e.setCodigo(re.getString("codigoamie"));
        e.setNombre(re.getString("nombre"));
        e.setRegimen(re.getString("regimen"));
        }else
           JOptionPane.showMessageDialog(null,"no se encuentra registrado");   
            co.desconectar();
                return true;
                  } catch (SQLException ex) {
  JOptionPane.showMessageDialog(null,ex);
    }
    return false;
   }   
   public void eliminar(String codigo){
   String z="UPDATE INSTITUCION SET estado='eliminado' where codigoamie='"+codigo+"'";
   try {
        
         Statement sq =co.getConnection().createStatement();
        sq.execute(z);
          JOptionPane.showMessageDialog(null,"Eliminado");
          co.desconectar();
    } catch (SQLException ex) {
  JOptionPane.showMessageDialog(null,ex.getMessage());
    }
   }
}
