package dao;

import bases.coneccion;
import java.sql.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import modelo.areaPedagogica;
import modelo.docenteMateria;
import modelo.inventario;
import modelo.materia;

public class inventarioDao {

    coneccion co;
    ResultSet re;
    PreparedStatement sq;

    public inventarioDao() {
        this.co = new coneccion();
    }

    public boolean guardar(inventario m) {
        String z = "INSERT INTO INVENTARIO (identificador,serie,codigoactual,descripcion,fechaingreso,cantidad,origeningreso,tiporespaldo,costo,modelo,marca,color,material,dimencion,estado,codigoarea) "
                + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            sq = co.getConnection().prepareStatement(z);
            sq.setString(1, m.getIdentificador());
            sq.setString(2, m.getSerie());
            sq.setString(3, m.getCodigoactual());
            sq.setString(4, m.getDescripcion());
            sq.setString(5, m.getFechaingreso());
            sq.setInt(6, m.getCantidad());
            sq.setString(7, m.getOrigeningreso());
            sq.setString(8, m.getTiporespaldo());
            sq.setDouble(9, m.getCosto());
            sq.setString(10, m.getModelo());
            sq.setString(11, m.getMarca());
            sq.setString(12, m.getColor());
            sq.setString(13, m.getMaterial());
            sq.setString(14, m.getDimenciones());
            sq.setString(15, m.getEstado());
            sq.setInt(16, m.getCodigoareaFK());
            if (sq.executeUpdate() > 0) {
                JOptionPane.showMessageDialog(null, "guardado exitosamente", "", JOptionPane.INFORMATION_MESSAGE);
            }
            co.desconectar();
            return true;
        } catch (Exception ex) {
            System.out.println("" + ex.getMessage());
        }
        return false;

    }

    public void cargarareas(JTable tabla, DefaultTableModel modelo, inventario in) {// metodo para cargar datos  de  la tabla materia
        String da[] = new String[18];
        String a = "select * from inventario where codigoarea=?";
        try {
            sq = co.getConnection().prepareStatement(a);
            sq.setInt(1, in.getCodigoareaFK());
            re = sq.executeQuery();
            while (re.next()) {
                da[0] = String.valueOf(re.getInt(1));
                da[1] = re.getString(2);
                da[2] = re.getString(3);
                da[3] = re.getString(4);
                da[4] = re.getString(5);
                da[5] = re.getString(6);
                da[6] = String.valueOf(re.getInt(7));
                da[7] = re.getString(8);
                da[8] = re.getString(9);
                da[9] = String.valueOf(re.getDouble(10));
                da[10] = re.getString(11);
                da[11] = re.getString(12);
                da[12] = re.getString(13);
                da[13] = re.getString(14);
                da[14] = re.getString(15);
                da[15] = re.getString(16);
                modelo.addRow(da);
            }
            tabla.setModel(modelo);
            co.desconectar();
        } catch (SQLException ex) {

        }
    }

}
