
package dao;

import bases.coneccion;
import java.awt.Color;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import modelo.areaPedagogica;
import modelo.diseñotablas.Render;
import modelo.docenteMateria;
import modelo.materia;

public class materiaDao {
coneccion co;
ResultSet re;
    public materiaDao() {
    this.co =new coneccion();      
    }
    public void guardar(materia m){
       String z="INSERT INTO MATERIA(nombre,idareaPedagogica) values(?,?)";
    try {
        PreparedStatement sq =co.getConnection().prepareStatement(z);
       sq.setString(1,m.getNombre());
       sq.setInt(2,m.getCodigoareapedagogica());
         if(sq.executeUpdate()>0){
                     JOptionPane.showMessageDialog(null,"guardado exitosamente","",JOptionPane.INFORMATION_MESSAGE);
                  }
co.desconectar();
    } catch (SQLException ex) {
  JOptionPane.showMessageDialog(null,ex);
    }
    }
       public void guardar(areaPedagogica m){
       String z="INSERT INTO AREAPEDAGOGICA(nombre) values(?)";
    try {
        PreparedStatement sq =co.getConnection().prepareStatement(z);
       sq.setString(1,m.getNombre());
         if(sq.executeUpdate()>0){
                     JOptionPane.showMessageDialog(null,"guardado exitosamente","",JOptionPane.INFORMATION_MESSAGE);
                  }
co.desconectar();
    } catch (SQLException ex) {
  JOptionPane.showMessageDialog(null,ex);
    }
    }
          public void listarareaspedagogicas(JComboBox titulos) {// metodo para  cargar los  titulos  profecionales del docente
        String ca = "select * from areapedagogica";
        Statement st;
        try {
            st = co.getConnection().createStatement();
            re = st.executeQuery(ca);
            while (re.next()) {
                titulos.addItem(re.getString("nombre"));
            }
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("error");
        }
    }
  public void listarmaterias(JComboBox materias) {// metodo para  cragar materias
        String ca = "select * from materia";
        Statement st;
        try {
            st = co.getConnection().createStatement();
            re = st.executeQuery(ca);
            while (re.next()) {
                materias.addItem(re.getString("nombre"));
            }
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("error");
        }
    }
    public void listarmateriaspordocente(JComboBox materias,String cedula,int codigoperiodo) {// metodo para  cragar materias
        String ca = "select materia.nombre FROM materia,docentemateria where docentemateria.codigomateria=materia.codigomateria && docentemateria.ceduladocente='"+cedula+"'&& docentemateria.codigoperiodo="+codigoperiodo+" GROUP by materia.nombre";
        Statement st;
        try {
            st = co.getConnection().createStatement();
            re = st.executeQuery(ca);
            while (re.next()) {
                materias.addItem(re.getString("nombre"));
            }
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("error");
        }
    }
    public void listarmateriasporgrados(JComboBox materias,int codigogrado) {// metodo para  cragar materias
        String ca = "select materia.nombre from gradomateria,materia,grado  where grado.codigogrado=gradomateria.codigogrado and gradomateria.codigogrado="+codigogrado+" and materia.codigomateria=gradomateria.codigomateria";
        Statement st;
        try {
            st = co.getConnection().createStatement();
            re = st.executeQuery(ca);
            while (re.next()) {
                materias.addItem(re.getString("nombre"));
            } 
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("error");
        }
    }
  public boolean modificar(materia m){
String z="update materia set nombre=? where codigomateria=?";
try {
PreparedStatement sq =co.getConnection().prepareStatement(z);
sq.setString(1,m.getNombre());
sq.setInt(2,m.getCodigomateria());
    sq.executeUpdate();  
co.desconectar();
return  true;
    } catch (SQLException ex) {
    JOptionPane.showMessageDialog(null,ex);
    }
    return false;
} 
  
      public ArrayList<materia> listar_materias(){
     ArrayList<materia> lis= new ArrayList<materia>();
     String sql="select * from materia"; 
      try {
         PreparedStatement sq =co.getConnection().prepareStatement(sql);
            re=sq.executeQuery();
          while(re.next()){                   
          materia m= new materia();
         m.setCodigomateria(re.getInt(1));
         m.setNombre(re.getString(2));
          lis.add(m);
          }
          sq.close();
          sq.close();
          co.desconectar();
      } catch (SQLException ex) {
          System.out.println(""+ex.getMessage());
      }
      return lis;
  }
    
 public boolean docente_materia(docenteMateria dm){
   String a="INSERT INTO DOCENTEMATERIA (ceduladocente,codigomateria,codigoperiodo)VALUES(?,?,?)";
    try {
PreparedStatement sq =co.getConnection().prepareStatement(a);
sq.setString(1,dm.getCeduladocenteFK());
sq.setInt(2,dm.getCodigomateriaFK());
sq.setInt(3,dm.getCodigoperiodoFK());
        sq.execute(a);
        co.desconectar();
        return true;
    } catch (SQLException ex) {
JOptionPane.showMessageDialog(null,ex);
    }   
    return false;
  }
   
}


