
package dao;

import bases.coneccion;
import com.placeholder.PlaceHolder;
import java.sql.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import modelo.inventario;
import modelo.jornada;
import modelo.matricula;
import modelo.periodoAcademico;

public class matriculaDao {
coneccion co;
ResultSet re;
PreparedStatement sq;
    public matriculaDao() {
    this.co =new coneccion();      
    }
    public void guardar(matricula m){
       String z="INSERT INTO matricula(codigoperiodo,cedulaestudiante,fecha,codigocurso) values(?,?,?,?)";
    try {
        sq =co.getConnection().prepareStatement(z);
       sq.setInt(1,m.getCodigoperiodoFK());
       sq.setString(2,m.getCedulaestudianteFK());
       sq.setString(3,m.getFecha());
       sq.setInt(4,m.getCodigocursoFk());
       
         if(sq.executeUpdate()>0){
                     JOptionPane.showMessageDialog(null,"guardado exitosamente","",JOptionPane.INFORMATION_MESSAGE);
                  }
co.desconectar();
    } catch (SQLException ex) {
  JOptionPane.showMessageDialog(null,ex);
    }
    } 
      public void cargarestudiantesporcursomatriculado(JTable tabla, DefaultTableModel modelo,int codigocuso){// metodo para cargar datos  de  la tabla materia
          String da[] = new String[8];
         int res=0;
        String a = "SELECT codigomatricula,e.cedulaestudiante,e.nombre, e.apellido from matricula m,estudiante e \n" +
"       where m.cedulaestudiante=e.cedulaestudiante and m.codigocurso=? and m.codigoperiodo in (SELECT\n" +
"               MAX(codigoperiodo) as codigo\n" +
"          FROM periodoacademico)";
        try {
            sq = co.getConnection().prepareStatement(a);
            sq.setInt(1,codigocuso);
            re = sq.executeQuery();
            while (re.next()) {
                da[0] = String.valueOf(re.getInt("codigomatricula"));
                da[1] = re.getString("e.cedulaestudiante");
                da[2] = re.getString("e.apellido")+" "+re.getString("e.nombre");
                modelo.addRow(da);
            }
            tabla.setModel(modelo);
           
            co.desconectar();
        } catch (Exception ex) {

        }
    }
           public void cargarepresentatesporcursomatriculado(JTable tabla, DefaultTableModel modelo,int codigocuso){// metodo para cargar datos  de  la tabla materia
          String da[] = new String[8];
         int res=0;
        String a = "SELECT r.cedularepresentante,r.nombre, r.apellido from matricula m,estudiante e, representante r\n" +
"      where m.cedulaestudiante=e.cedulaestudiante and e.cedularepresentante=r.cedularepresentante and m.codigocurso=? and m.codigoperiodo in (SELECT\n" +
"              MAX(codigoperiodo) as codigo\n" +
"      FROM periodoacademico)";
        try {
            sq = co.getConnection().prepareStatement(a);
            sq.setInt(1,codigocuso);
            re = sq.executeQuery();
            while (re.next()) {
                da[0] = re.getString("r.cedularepresentante");
                da[1] = re.getString("r.apellido")+" "+re.getString("r.nombre");
                modelo.addRow(da);
            }
            tabla.setModel(modelo);
           
            co.desconectar();
        } catch (Exception ex) {

        }
    }
     public void listargradoscursadosporelestudiante(JComboBox grados,matricula m) {// metodo para  cargar los grados  cursado  por el estudiante
        String ca = "select v.nombre,v.tipo from matricula m, vista_cursos v where m.codigocurso=v.codigocurso and m.cedulaestudiante='"+m.getCedulaestudianteFK()+"'";
        Statement st;
        try {
            st = co.getConnection().createStatement();
            re = st.executeQuery(ca);
            while (re.next()) {
                grados.addItem(re.getString("v.nombre")+" "+re.getString("v.tipo"));
            } 
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("error");
        }
    }
      public boolean cargardatosparacertificado(matricula m, periodoAcademico p) {// metodo para  cargar los grados  cursado  por el estudiante
        String ca = "SELECT matricula.codigomatricula,periodo,fecha from periodoacademico,matricula,vista_cursos,grado WHERE periodoacademico.codigoperiodo=matricula.codigoperiodo and matricula.codigocurso=vista_cursos.codigocurso and vista_cursos.codigogrado=grado.codigogrado and grado.codigogrado="+m.getCodigocursoFk()+" and matricula.cedulaestudiante='"+m.getCedulaestudianteFK()+"'";
        Statement st;
        try {
            st = co.getConnection().createStatement();
            re = st.executeQuery(ca);
            if (re.next()) {
                m.setCodigomatricula(re.getInt("matricula.codigomatricula"));
                m.setFecha(re.getString("fecha"));
                p.setPeriodo(re.getString("periodo"));
            } 
            co.desconectar();
            return true;
        } catch (Exception ex) {
            System.out.println("error");
        }
    return false;
    }
}


