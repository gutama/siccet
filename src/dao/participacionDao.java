package dao;

import bases.coneccion;
import java.sql.*;
import javax.swing.*;
import modelo.participacionestudiantil;

public class participacionDao {

    coneccion co;
    ResultSet re;

    public participacionDao() {
        this.co = new coneccion();
    }

    public void guardar(participacionestudiantil m) {
        String z = "INSERT INTO participacionestudiantil(calificacion,codigomatricula) values(?,?)";
        try {
            PreparedStatement sq = co.getConnection().prepareStatement(z);
            sq.setDouble(1, m.getCalificacion());
            sq.setInt(2, m.getCodigomatriculaFK());
            if (sq.executeUpdate() > 0) {
                JOptionPane.showMessageDialog(null, "guardado exitosamente", "", JOptionPane.INFORMATION_MESSAGE);
            }
            co.desconectar();
        } catch (SQLException ex) {
            System.out.println("error");
        }
    }
      
}
