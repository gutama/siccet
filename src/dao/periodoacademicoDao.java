/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import bases.coneccion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import modelo.materia;
import modelo.periodoAcademico;

/**
 *
 * @author Legion
 */
public class periodoacademicoDao {
    coneccion co;
    ResultSet re;
    PreparedStatement sq;
    public periodoacademicoDao() {
          this.co = new coneccion();
    }
     public boolean guardar(periodoAcademico pa){
       String z="INSERT INTO PERIODOACADEMICO(periodo) values(?)";
    try {
        sq =co.getConnection().prepareStatement(z);
       sq.setString(1,pa.getPeriodo());
         sq.executeUpdate();
                 
co.desconectar();
return true;
    } catch (Exception ex) {
  JOptionPane.showMessageDialog(null,ex.getMessage());
    }
        return false;  
}

                public void listarperiodos(JComboBox titulos) {// metodo para  cargar los  titulos  profecionales del docente
        String ca = "SELECT * from periodoacademico";
        Statement st;
        try {
            st = co.getConnection().createStatement();
            re = st.executeQuery(ca);
            while (re.next()) {
                titulos.addItem(re.getString("periodo"));
            }
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("error");
        }
    }
}
