package dao;

import bases.coneccion;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import modelo.persona;
import modelo.personarol;
import modelo.rol;
import modelo.usuario;

public class personaDao {

    coneccion co = null;
    ResultSet re = null;
PreparedStatement sq;
    public personaDao() {
        this.co = new coneccion();
    }

    public void guardar(persona p) {//guardardatos en la tabla persona
        try {
            String query = "INSERT INTO PERSONA(cedula,nombrecompleto,añonacimiento)VALUES(?,?,?)";
            sq = co.getConnection().prepareStatement(query);
            sq.setString(1, p.getCedula());
            sq.setString(2, p.getNombre());
            sq.setString(3, p.getAñonacimiento());
            sq.executeUpdate();
            co.desconectar();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "la persona ya se encuentra registrada");
        }
    }
// 

    public String guardarusuario(usuario u) {
        String respuesta = null;
        try {
            String query = "INSERT INTO USUARIO(cedula,clave)VALUES(?,?)";
            sq = co.getConnection().prepareStatement(query);
            sq.setString(1, u.getCedulaFk());
            sq.setString(2, u.getClave());
            sq.executeUpdate();
            JOptionPane.showMessageDialog(null, " Usuario registrado");
            co.desconectar();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "no registrado" + ex.getMessage());
        }
        return respuesta;
    }
   
 public boolean modificarrol(rol r) {
        try {
            String query = "UPDATE ROL SET cargo=? WHERE codigorol=?";
            sq = co.getConnection().prepareStatement(query);
            sq.setString(1,r.getRol());
            sq.setInt(2,r.getCodigorol());
            sq.executeUpdate();
            co.desconectar();
                return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "no registrado la nueva contraseña" + ex.getMessage());
        }
        return false;

    }
 public boolean modificargodocente(personarol pr) {
        try {
            String query = "UPDATE personarol SET codigorol=? WHERE cedula=?";
            sq = co.getConnection().prepareStatement(query);
            sq.setInt(1,pr.getCodigorolFK());
            sq.setString(2,pr.getCedulaFK());
            sq.executeUpdate();
            co.desconectar();
                return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "no registrado la nueva contraseña" + ex.getMessage());
        }
        return false;

    }
    public int guadarol(rol r) {
        int codigo = 0;        
           String query = "INSERT INTO ROL(cargo)VALUES(?)";
        try {
            sq = co.getConnection().prepareStatement(query);
            sq.setString(1,r.getRol());
            sq.executeUpdate();
            re = sq.getGeneratedKeys();
            if (re.next()) {
                codigo = re.getInt(1);
            }
            co.desconectar();
            return codigo;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "no registrado" + ex.getMessage());
        }
        return codigo;
    }

    public void guardarpersonarol(personarol pr) {
        String query = "INSERT INTO personarol (cedula,codigorol) VALUES (?,?)";
        try {
            sq = co.getConnection().prepareStatement(query);
            sq.setString(1, pr.getCedulaFK());
            sq.setInt(2, pr.getCodigorolFK());
            sq.executeUpdate();
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("error" + ex.getMessage());
        }

    }

    public boolean buscar(usuario u, rol r) { //metodo que se  usa para ingresar al sistema.    
        String a = "SELECT usuario.cedula,cargo,clave FROM persona,usuario,rol,personarol pr WHERE persona.cedula=usuario.cedula AND persona.cedula=pr.cedula and pr.codigorol=rol.codigorol and usuario.cedula=?";
        try {
           sq = co.getConnection().prepareStatement(a);
            sq.setString(1, u.getCedulaFk());
            re = sq.executeQuery();
            while (re.next()) {
                u.setClave(re.getString("clave"));
                u.setCedulaFk(re.getString("cedula"));
                r.setRol(re.getString("cargo"));
            }
            co.desconectar();
            return true;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "no registrado" + ex.getMessage());
        }
        return false;
    }

    public boolean buscarpersona(persona p) { //pendiente    
        String a = "select nombrecompleto from persona where cedula=?";
        try {
            sq = co.getConnection().prepareStatement(a);
            sq.setString(1, p.getCedula());
            re = sq.executeQuery();
            while (re.next()) {
                p.setNombre(re.getString("nombrecompleto"));
            }
            co.desconectar();
            return true;
        } catch (SQLException ex) {
        }
        return false;
    }
      public boolean Cargarroles(JComboBox role) { //pendiente    
        String a = "select *from rol";
        try {
            sq = co.getConnection().prepareStatement(a);
            re = sq.executeQuery();
            while (re.next()) {
               role.addItem(re.getString("cargo"));
            }
            co.desconectar();
            return true;
        } catch (SQLException ex) {
        }
        return false;
    }
       public boolean rolesparadocentes(JComboBox role) { //pendiente    
        String a = "select * from rol where cargo!=' ESTUDIANTE' and cargo !='ADMINISTRADOR'";
        try {
            sq = co.getConnection().prepareStatement(a);
            re = sq.executeQuery();
            while (re.next()) {
               role.addItem(re.getString("cargo"));
            }
            co.desconectar();
            return true;
        } catch (SQLException ex) {
        }
        return false;
    }
}
