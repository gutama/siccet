package dao;

import bases.coneccion;
import java.sql.*;
import javax.swing.*;
import java.net.URL;
import javax.swing.table.DefaultTableModel;
import modelo.estudiante;
import modelo.estudianteDocumento;
import modelo.familiarcercano;
import modelo.grado;
import modelo.representante;

/**
 *
 * @author Mesias
 */
public class representanteDao {

    coneccion co;
    ResultSet re;
    PreparedStatement sq;

    public representanteDao() {
        this.co = new coneccion();
    }

    public boolean guardar(representante  es) {
        try {
            String z = "INSERT INTO representante VALUES ('" + es.getCedula() + "','" + es.getNombre() + "','" + es.getApellido() + "',"
                    + "'" +es.getDireccion() + "','" + es.getCorreo() + "','" + es.getCodigofamiliarFK() + "')";
            Statement st = co.getConnection().createStatement();
            st.execute(z);
            co.desconectar();
            return true;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return false;

    }

    public boolean modificar(representante es) {
        try {
            String z = "update representante set nombre=?, apellido=?,direccion=?,email=? where  cedularepresentante=?";
            sq = co.getConnection().prepareStatement(z);
            sq.setString(1, es.getNombre());
            sq.setString(2, es.getApellido());
            sq.setString(3, es.getDireccion());
            sq.setString(4, es.getCorreo());
            sq.setString(5, es.getCedula());
            sq.executeUpdate();
            co.desconectar();
            return true;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return false;
    }
     public boolean modificarfamilar(familiarcercano fc) {
        try {
            String z = "update familiarcercano set telefono=?, direccion=? where codigofamiliar=?";
            sq = co.getConnection().prepareStatement(z);
            sq.setString(1,fc.getTelefono());
            sq.setString(2,fc.getDireccion());
            sq.setInt(3,fc.getCodigofamiliar());
            sq.executeUpdate();
            co.desconectar();
            return true;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return false;
    }

    public boolean listar(JTable tabla, DefaultTableModel model) {
        model.addColumn("CEDULA");
        model.addColumn("NOMBRES");
        model.addColumn("APELLIDOS");
        try {
            String[] x = new String[4];
            String z = "select * from representante where estado='activo'";
            PreparedStatement sq = co.getConnection().prepareStatement(z);
            re = sq.executeQuery();
            while (re.next()) {
                x[0] = re.getString("cedularepresentante");
                x[1] = re.getString("nombre");
                x[2] = re.getString("apellido");
                model.addRow(x);
            }
            tabla.setModel(model);
            co.desconectar();
            return true;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return false;
    }

 public int guardarparientefamiliar(familiarcercano f ){
      int codigo=0;
  String a="INSERT INTO familiarcercano(telefono,direccion) VALUES (?,?)";
      try {
          sq=co.getConnection().prepareStatement(a);
         sq.setString(1,f.getTelefono());
          sq.setString(2,f.getDireccion());
          sq.executeUpdate();
         re = sq.getGeneratedKeys();
         if (re.next()) 
           codigo=re.getInt(1);
 
          co.desconectar();
          return codigo;
      } catch (Exception ex) {
          System.out.println("error al registrar pariente familiar"+ex.getMessage());
      }
        return codigo;
  }

  
}
