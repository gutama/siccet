package dao;

import bases.coneccion;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import modelo.gradoRequisitos;
import modelo.materia;
import modelo.requsitosmatricula;

public class requisitoDao {

    coneccion co;
    ResultSet re;
PreparedStatement sq;
    public requisitoDao() {
        this.co = new coneccion();
    }

    public int guardarrequisito(requsitosmatricula r) {
        int codigo=0;
        String z = "INSERT INTO requisito(nombre) values(?)";
        try {
           sq = co.getConnection().prepareStatement(z);
            sq.setString(1, r.getNombre());
            sq.executeUpdate();
              re = sq.getGeneratedKeys();
            if (re.next()) 
           codigo=re.getInt(1);
            co.desconectar();
             return codigo;
        } catch (SQLException ex) {
            System.out.println("error guardar requisito"+ex.getMessage());
        }
        return codigo;
    }
      public ArrayList<requsitosmatricula> listar_requisitos(int codigogrado){
     ArrayList<requsitosmatricula> lis= new ArrayList<requsitosmatricula>();
     String sql="SELECT nombre from requisito,gradorequisito WHERE requisito.codigorequisito=gradorequisito.codigorequisito and gradorequisito.codigogrado=?"; 
      try {
          sq =co.getConnection().prepareStatement(sql);
         sq.setInt(1,codigogrado);
            re=sq.executeQuery();
          while(re.next()){                   
          requsitosmatricula r=new requsitosmatricula();
         r.setNombre(re.getString("nombre"));
          lis.add(r);
          }
          sq.close();
          sq.close();
          co.desconectar();
      } catch (SQLException ex) {
          System.out.println(""+ex.getMessage());
      }
      return lis;
  }
     public boolean guardarrequisitogrado(gradoRequisitos r) {
        String z = "INSERT INTO gradorequisito(codigogrado,codigorequisito) values(?,?)";
        try {
           sq = co.getConnection().prepareStatement(z);
            sq.setInt(1, r.getCodigogradoFK());
             sq.setInt(2, r.getCodigorequisitoFK());
            sq.executeUpdate();
            co.desconectar();
            return true;
        } catch (SQLException ex) {
            System.out.println("error");
        }
        return false;
    }
    public void cargargrados(JTable tabla) {// cargar datos para asignar  las materias  a cada docente y  cada  grado
        String da[] = new String[6];
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("CODIGO CURSO");
        modelo.addColumn("CODIGO GRADO");
        modelo.addColumn("NOMBRE");
        modelo.addColumn("TIPO");
        modelo.addColumn("JORNADA");
        modelo.addColumn("PARALELO");
        String a = "select * from grado";
        try {
            PreparedStatement sq = co.getConnection().prepareStatement(a);
            re = sq.executeQuery();
            while (re.next()) {
                da[0] = String.valueOf(re.getInt("CODIGOCURSO"));
                da[1] = String.valueOf(re.getInt("CODIGOGRADO"));
                da[2] = re.getString("NOMBRE");
                da[3] = re.getString("TIPO");
                da[4] = re.getString("JORNADA");
                da[5] = re.getString("PARALELO");
                modelo.addRow(da);
            }
            tabla.setModel(modelo);
            co.desconectar();
        } catch (Exception ex) {
            System.out.println("no vale" + ex.getMessage());
        }
    }

}
