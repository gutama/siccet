package impresiones;

/**
 *
 * @author Legion
 */
public class PlantillaCertificado {

    private String CodigoAMIE;
    private String certificado;
    private String añolectivo;
    private String Regimen;
    private String nombreinstitucion;
    private String estudiante;
    private String grado;
    private String cedula;
    private String imagen1;
    private String imagen2;
    private String promedio;
    private String Lugarfirma;

    public String getLugarfirma() {
        return Lugarfirma;
    }

    public void setLugarfirma(String Lugarfirma) {
        this.Lugarfirma = Lugarfirma;
    }

    public String getCertificado() {
        return certificado;
    }

    public void setCertificado(String certificado) {
        this.certificado = certificado;
    }
    public String getImagen1() {
        return imagen1;
    }

    public void setImagen1(String imagen1) {
        this.imagen1 = imagen1;
    }

    public String getImagen2() {
        return imagen2;
    }

    public void setImagen2(String imagen2) {
        this.imagen2 = imagen2;
    }

    public String getPromedio() {
        return promedio;
    }

    public void setPromedio(String promedio) {
        this.promedio = promedio;
    }

    public String getCodigoAMIE() {
        return CodigoAMIE;
    }

    public void setCodigoAMIE(String CodigoAMIE) {
        this.CodigoAMIE = CodigoAMIE;
    }

    public String getAñolectivo() {
        return añolectivo;
    }

    public void setAñolectivo(String añolectivo) {
        this.añolectivo = añolectivo;
    }

    public String getRegimen() {
        return Regimen;
    }

    public void setRegimen(String Regimen) {
        this.Regimen = Regimen;
    }

    public String getNombreinstitucion() {
        return nombreinstitucion;
    }

    public void setNombreinstitucion(String nombreinstitucion) {
        this.nombreinstitucion = nombreinstitucion;
    }

    public String getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(String estudiante) {
        this.estudiante = estudiante;
    }

    public String getGrado() {
        return grado;
    }

    public void setGrado(String grado) {
        this.grado = grado;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

 

}
