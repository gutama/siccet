
package impresiones;

/**
 *
 * @author Legion
 */
public class cuadrocalificaciones {
    private String area;
    private String materia;
    private String calificacioncuantitativa;
    private String calicualitativa;
    private String comportamiento;

    public String getComportamiento() {
        return comportamiento;
    }

    public void setComportamiento(String comportamiento) {
        this.comportamiento = comportamiento;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    public String getCalificacioncuantitativa() {
        return calificacioncuantitativa;
    }

    public void setCalificacioncuantitativa(String calificacioncuantitativa) {
        this.calificacioncuantitativa = calificacioncuantitativa;
    }

    public String getCalicualitativa() {
        return calicualitativa;
    }

    public void setCalicualitativa(String calicualitativa) {
        this.calicualitativa = calicualitativa;
    }
    
}
