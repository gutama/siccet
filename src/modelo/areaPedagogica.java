
package modelo;

/**
 *
 * @author Legion
 */
public class areaPedagogica {
    private int codigoareapedagogica;
    private String  nombre;

    public int getCodigoareapedagogica() {
        return codigoareapedagogica;
    }

    public void setCodigoareapedagogica(int codigoareapedagogica) {
        this.codigoareapedagogica = codigoareapedagogica;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
}
