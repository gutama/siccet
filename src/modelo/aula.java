
package modelo;

/**
 *
 * @author Legion
 */
public class aula {
    private  int   codigoaula;
    private String  numerodeaula;
    private int cupo;

    public int getCodigoaula() {
        return codigoaula;
    }

    public void setCodigoaula(int codigoaula) {
        this.codigoaula = codigoaula;
    }

    public String getNumerodeaula() {
        return numerodeaula;
    }

    public void setNumerodeaula(String numerodeaula) {
        this.numerodeaula = numerodeaula;
    }

  

    public int getCupo() {
        return cupo;
    }

    public void setCupo(int cupo) {
        this.cupo = cupo;
    }
    
}
