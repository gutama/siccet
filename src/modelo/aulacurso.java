
package modelo;

/**
 *
 * @author Legion
 */
public class aulacurso {
    private int codigoaulacurso;
    private int codigoaulafk;
    private int codigocursofk;
    private int codigoperiodofk;

    public int getCodigoperiodofk() {
        return codigoperiodofk;
    }

    public void setCodigoperiodofk(int codigoperiodofk) {
        this.codigoperiodofk = codigoperiodofk;
    }
    

    public int getCodigoaulacurso() {
        return codigoaulacurso;
    }

    public void setCodigoaulacurso(int codigoaulacurso) {
        this.codigoaulacurso = codigoaulacurso;
    }

    public int getCodigoaulafk() {
        return codigoaulafk;
    }

    public void setCodigoaulafk(int codigoaulafk) {
        this.codigoaulafk = codigoaulafk;
    }

    public int getCodigocursofk() {
        return codigocursofk;
    }

    public void setCodigocursofk(int codigocursofk) {
        this.codigocursofk = codigocursofk;
    }
    
}
