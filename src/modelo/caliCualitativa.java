
package modelo;

/**
 *
 * @author Legion
 */
public class caliCualitativa {
    private int codigonotacualitativa;
    private String equivalente;

    public int getCodigonotacualitativa() {
        return codigonotacualitativa;
    }

    public void setCodigonotacualitativa(int codigonotacualitativa) {
        this.codigonotacualitativa = codigonotacualitativa;
    }

    public String getEquivalente() {
        return equivalente;
    }

    public void setEquivalente(String equivalente) {
        this.equivalente = equivalente;
    }
       public String calificacioncualitativa( double promedio){
       String calificacion="";
       if(promedio<7){
        calificacion="No Alcanza los Aprendizajes Requerido(NAR)";   
       }else
         if(promedio>=7 && promedio<8){
            calificacion="Está próximo alcanzar los Aprendisajes Requeridos (EAR)";   
           }else
           if(promedio>=8 && promedio<9){
           calificacion="Alcanza los Aprendizajes Requerido(AA)";
           }else
               if(promedio>=9){
                  calificacion="Domina los Aprendizajes Requeridos(DA)";
               }
        return calificacion;
}
}
