package modelo;

/**
 *
 * @author Legion
 */
public class caliCuantitativa {

    private int codigonotacuantitativa;
    private double quimestre1;
    private double quimestre2;
    private double promedioanual;
    private String comportamiento;
    private int codigomatriculaFK;
    private int codigomateriaFK;
    private int codigonotacualitativaFK;

    
    public String getComportamiento() {
        return comportamiento;
    }

    public void setComportamiento(String comportamiento) {
        this.comportamiento = comportamiento;
    }

    public int getCodigomatriculaFK() {
        return codigomatriculaFK;
    }

    public void setCodigomatriculaFK(int codigomatriculaFK) {
        this.codigomatriculaFK = codigomatriculaFK;
    }

    public int getCodigomateriaFK() {
        return codigomateriaFK;
    }

    public void setCodigomateriaFK(int codigomateriaFK) {
        this.codigomateriaFK = codigomateriaFK;
    }
    

    public int getCodigonotacualitativaFK() {
        return codigonotacualitativaFK;
    }

    public void setCodigonotacualitativaFK(int codigonotacualitativaFK) {
        this.codigonotacualitativaFK = codigonotacualitativaFK;
    }


    public int getCodigonotacuantitativa() {
        return codigonotacuantitativa;
    }

    public void setCodigonotacuantitativa(int codigonotacuantitativa) {
        this.codigonotacuantitativa = codigonotacuantitativa;
    }

    public double getQuimestre1() {
        return quimestre1;
    }

    public void setQuimestre1(double quimestre1) {
        this.quimestre1 = quimestre1;
    }

    public double getQuimestre2() {
        return quimestre2;
    }

    public void setQuimestre2(double quimestre2) {
        this.quimestre2 = quimestre2;
    }

    public double getPromedioanual() {
        return promedioanual;
    }

    public void setPromedioanual(double promedioanual) {
        this.promedioanual = promedioanual;
    }
    public double promedio(){
        promedioanual=(quimestre1+quimestre2)/2;
        return promedioanual;
    
    }


}
