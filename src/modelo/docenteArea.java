
package modelo;

/**
 *
 * @author Legion
 */
public class docenteArea {
    private int docentearea;
    private String ceduladocente;
    private int codigoarea;
    private int codigoperiodo;

    public int getCodigoperiodo() {
        return codigoperiodo;
    }

    public void setCodigoperiodo(int codigoperiodo) {
        this.codigoperiodo = codigoperiodo;
    }

    public int getDocentearea() {
        return docentearea;
    }

    public void setDocentearea(int docentearea) {
        this.docentearea = docentearea;
    }

    public String getCeduladocente() {
        return ceduladocente;
    }

    public void setCeduladocente(String ceduladocente) {
        this.ceduladocente = ceduladocente;
    }

    public int getCodigoarea() {
        return codigoarea;
    }

    public void setCodigoarea(int codigoarea) {
        this.codigoarea = codigoarea;
    }
    
}
