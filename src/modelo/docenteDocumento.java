
package modelo;

/**
 *
 * @author Legion
 */
public class docenteDocumento {
   private int codigodocentedoc;
   private int codigodocumento;
   private String ceduladocente;

    public int getCodigodocentedoc() {
        return codigodocentedoc;
    }

    public void setCodigodocentedoc(int codigodocentedoc) {
        this.codigodocentedoc = codigodocentedoc;
    }

    public int getCodigodocumento() {
        return codigodocumento;
    }

    public void setCodigodocumento(int codigodocumento) {
        this.codigodocumento = codigodocumento;
    }

 

    public String getCeduladocente() {
        return ceduladocente;
    }

    public void setCeduladocente(String ceduladocente) {
        this.ceduladocente = ceduladocente;
    }
   
}
