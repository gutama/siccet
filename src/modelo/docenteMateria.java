
package modelo;

/**
 *
 * @author Legion
 */
public class docenteMateria {
    private int codigodocenteMateria;
    private String ceduladocenteFK;
    private int codigomateriaFK;
    private  int codigoperiodoFK;

    public int getCodigodocenteMateria() {
        return codigodocenteMateria;
    }

    public void setCodigodocenteMateria(int codigodocenteMateria) {
        this.codigodocenteMateria = codigodocenteMateria;
    }

    public String getCeduladocenteFK() {
        return ceduladocenteFK;
    }

    public void setCeduladocenteFK(String ceduladocenteFK) {
        this.ceduladocenteFK = ceduladocenteFK;
    }

    public int getCodigomateriaFK() {
        return codigomateriaFK;
    }

    public void setCodigomateriaFK(int codigomateriaFK) {
        this.codigomateriaFK = codigomateriaFK;
    }

    public int getCodigoperiodoFK() {
        return codigoperiodoFK;
    }

    public void setCodigoperiodoFK(int codigoperiodoFK) {
        this.codigoperiodoFK = codigoperiodoFK;
    }

  
    
}
