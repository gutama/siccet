package modelo;

/**
 *
 * @author Legion
 */
public class docentecurso {

    private int codigodocentegrado;
    private String ceduladocenteFK;
    private int codigogradoFK;
    private String tipo;
    private int codigomateriaFK;

    public int getCodigomateriaFK() {
        return codigomateriaFK;
    }

    public void setCodigomateriaFK(int codigomateriaFK) {
        this.codigomateriaFK = codigomateriaFK;
    }
    private int codigoperiodoFK;

    public int getCodigodocentegrado() {
        return codigodocentegrado;
    }

    public void setCodigodocentegrado(int codigodocentegrado) {
        this.codigodocentegrado = codigodocentegrado;
    }

    public String getCeduladocenteFK() {
        return ceduladocenteFK;
    }

    public void setCeduladocenteFK(String ceduladocenteFK) {
        this.ceduladocenteFK = ceduladocenteFK;
    }

    public int getCodigogradoFK() {
        return codigogradoFK;
    }

    public void setCodigogradoFK(int codigogradoFK) {
        this.codigogradoFK = codigogradoFK;
    }


    public int getCodigoperiodoFK() {
        return codigoperiodoFK;
    }

    public void setCodigoperiodoFK(int codigoperiodoFK) {
        this.codigoperiodoFK = codigoperiodoFK;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

  

}
