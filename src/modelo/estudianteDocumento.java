
package modelo;

/**
 *
 * @author Legion
 */
public class estudianteDocumento {
    private int codigoestudiantedoc;
    private  String cedulaestudiante;
    private int codigodocumento;

    public int getCodigoestudiantedoc() {
        return codigoestudiantedoc;
    }

    public void setCodigoestudiantedoc(int codigoestudiantedoc) {
        this.codigoestudiantedoc = codigoestudiantedoc;
    }

    public String getCedulaestudiante() {
        return cedulaestudiante;
    }

    public void setCedulaestudiante(String cedulaestudiante) {
        this.cedulaestudiante = cedulaestudiante;
    }

    public int getCodigodocumento() {
        return codigodocumento;
    }

    public void setCodigodocumento(int codigodocumento) {
        this.codigodocumento = codigodocumento;
    }

  
    
}
