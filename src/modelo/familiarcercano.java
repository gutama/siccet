
package modelo;

/**
 *
 * @author Legion
 */
public class familiarcercano {
   private int codigofamiliar;
   private String telefono;
   private String direccion;

    public int getCodigofamiliar() {
        return codigofamiliar;
    }

    public void setCodigofamiliar(int codigofamiliar) {
        this.codigofamiliar = codigofamiliar;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }


    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
   
}
