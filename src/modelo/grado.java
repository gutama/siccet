
package modelo;

/**
 *
 * @author Legion
 */
public class grado {
    private int codigogrado;
    private String nombre;
    private String tipo;

    public int getCodigogrado() {
        return codigogrado;
    }

    public void setCodigogrado(int codigogrado) {
        this.codigogrado = codigogrado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

}
