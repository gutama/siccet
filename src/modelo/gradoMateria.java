/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Legion
 */
public class gradoMateria {
    private  int codigogradomateria;
    private int codigomateria;
    private int numerohoras;
    private int codigogrado;

    public int getNumerohoras() {
        return numerohoras;
    }

    public void setNumerohoras(int numerohoras) {
        this.numerohoras = numerohoras;
    }

    public int getCodigogradomateria() {
        return codigogradomateria;
    }

    public void setCodigogradomateria(int codigogradomateria) {
        this.codigogradomateria = codigogradomateria;
    }

    public int getCodigomateria() {
        return codigomateria;
    }

    public void setCodigomateria(int codigomateria) {
        this.codigomateria = codigomateria;
    }

    public int getCodigogrado() {
        return codigogrado;
    }

    public void setCodigogrado(int codigogrado) {
        this.codigogrado = codigogrado;
    }
    
}
