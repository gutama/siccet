/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Legion
 */
public class gradoRequisitos {
    private int codigogradorequisito;
    private int codigogradoFK;
    private int codigorequisitoFK;

    public int getCodigogradorequisito() {
        return codigogradorequisito;
    }

    public void setCodigogradorequisito(int codigogradorequisito) {
        this.codigogradorequisito = codigogradorequisito;
    }

    public int getCodigogradoFK() {
        return codigogradoFK;
    }

    public void setCodigogradoFK(int codigogradoFK) {
        this.codigogradoFK = codigogradoFK;
    }

    public int getCodigorequisitoFK() {
        return codigorequisitoFK;
    }

    public void setCodigorequisitoFK(int codigorequisitoFK) {
        this.codigorequisitoFK = codigorequisitoFK;
    }
    
}
