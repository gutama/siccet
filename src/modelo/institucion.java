
package modelo;

/**
 *
 * @author Mesias
 */
public class institucion {
 private int codigoinstitucion;
 private String codigo;
 private String nombre;
 private String sector;
 private String telefono;
 private String correo;
 private String regimen;
    public String getNombre() {
        return nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getSector() {
        return sector;
    }

    public String getTelefono() {
        return telefono;
    }

    

    public String getCorreo() {
        return correo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public int getCodigoinstitucion() {
        return codigoinstitucion;
    }

    public void setCodigoinstitucion(int codigoinstitucion) {
        this.codigoinstitucion = codigoinstitucion;
    }

    public String getRegimen() {
        return regimen;
    }

    public void setRegimen(String regimen) {
        this.regimen = regimen;
    }

   
}
