
package modelo;

/**
 *
 * @author Legion
 */
public class inventario {
    private int codigoinventario;
    private String identificador;
    private String serie;
    private String codigoactual;
    private String descripcion;
    private String fechaingreso;
    private int cantidad;
    private String origeningreso;
    private String tiporespaldo;
    private double costo;
    private String modelo;
    private String marca;
    private String color;
    private String material;
    private String dimenciones;
    private String estado;
    private int codigoareaFK;

    public int getCodigoinventario() {
        return codigoinventario;
    }

    public void setCodigoinventario(int codigoinventario) {
        this.codigoinventario = codigoinventario;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getCodigoactual() {
        return codigoactual;
    }

    public void setCodigoactual(String codigoactual) {
        this.codigoactual = codigoactual;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechaingreso() {
        return fechaingreso;
    }

    public void setFechaingreso(String fechaingreso) {
        this.fechaingreso = fechaingreso;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getOrigeningreso() {
        return origeningreso;
    }

    public void setOrigeningreso(String origeningreso) {
        this.origeningreso = origeningreso;
    }

    public String getTiporespaldo() {
        return tiporespaldo;
    }

    public void setTiporespaldo(String tiporespaldo) {
        this.tiporespaldo = tiporespaldo;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getCodigoareaFK() {
        return codigoareaFK;
    }

    public void setCodigoareaFK(int codigoareaFK) {
        this.codigoareaFK = codigoareaFK;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getDimenciones() {
        return dimenciones;
    }

    public void setDimenciones(String dimenciones) {
        this.dimenciones = dimenciones;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    
    
}
