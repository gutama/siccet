
package modelo;

/**
 *
 * @author Mesias
 */
public class materia extends areaPedagogica{
    private int codigomateria;
    private String nombre;

    public int getCodigomateria() {
        return codigomateria;
    }

    public void setCodigomateria(int codigomateria) {
        this.codigomateria = codigomateria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

 


    
   
    public String calificacioncualitativa( double promedio){
       String calificacion="";
       if(promedio<=7){
        calificacion="No Alcanza los Aprendizajes Requerido(NAR)";   
       }else
         if(promedio<=7){
            calificacion="Está próximo alcanzar los Aprendisajes Requeridos (EAR)";   
           }else
           if(promedio>=7 && promedio<9){
           calificacion="Alcanza los Aprendizajes Requerido(AA)";
           }else
               if(promedio>=9){
                  calificacion="Domina los Aprendizajes Requeridos(DA)";
               }
        return calificacion;
}   
}
