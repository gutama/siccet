
package modelo;

public class matricula { 
    private int codigomatricula; 
     private int codigocursoFk;
     private int codigoperiodoFK;
     private String cedulaestudianteFK;
     private String fecha;

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCedulaestudianteFK() {
        return cedulaestudianteFK;
    }

    public void setCedulaestudianteFK(String cedulaestudianteFK) {
        this.cedulaestudianteFK = cedulaestudianteFK;
    }
    
    public int getCodigomatricula() {
        return codigomatricula;
    }

    public void setCodigomatricula(int codigomatricula) {
        this.codigomatricula = codigomatricula;
    }

    public int getCodigocursoFk() {
        return codigocursoFk;
    }

    public void setCodigocursoFk(int codigocursoFk) {
        this.codigocursoFk = codigocursoFk;
    }


    public int getCodigoperiodoFK() {
        return codigoperiodoFK;
    }

    public void setCodigoperiodoFK(int codigoperiodoFK) {
        this.codigoperiodoFK = codigoperiodoFK;
    }


}
