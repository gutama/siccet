
package modelo;

/**
 *
 * @author Legion
 */
public class paralelo {
   private int codigoparalelo;
   private String paralelo;

    public int getCodigoparalelo() {
        return codigoparalelo;
    }

    public void setCodigoparalelo(int codigoparalelo) {
        this.codigoparalelo = codigoparalelo;
    }

    public String getParalelo() {
        return paralelo;
    }

    public void setParalelo(String paralelo) {
        this.paralelo = paralelo;
    }
   
}
