
package modelo;

/**
 *
 * @author Legion
 */
public class participacionestudiantil {
   private int codigoparticipacion;
   private double calificacion;
   private int codigomatriculaFK;

    public int getCodigoparticipacion() {
        return codigoparticipacion;
    }

    public void setCodigoparticipacion(int codigoparticipacion) {
        this.codigoparticipacion = codigoparticipacion;
    }

    public double getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(double calificacion) {
        this.calificacion = calificacion;
    }

    public int getCodigomatriculaFK() {
        return codigomatriculaFK;
    }

    public void setCodigomatriculaFK(int codigomatriculaFK) {
        this.codigomatriculaFK = codigomatriculaFK;
    }
   
   
}
