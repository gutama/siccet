
package modelo;

public class persona { 
private  String cedula;
private String nombre;
private String apellido;
private String telefono; 
   private String correo;
   private String añonacimiento;

    public String getAñonacimiento() {
        return añonacimiento;
    }

    public void setAñonacimiento(String añonacimiento) {
        this.añonacimiento = añonacimiento;
    }

    public persona() {
        this.cedula ="";
        this.nombre = "";
        this.apellido = "";
        this.telefono = "";
        this.correo ="";
        this.añonacimiento ="";
    }
   

    public String getCedula() {
        return cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
       public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
}
