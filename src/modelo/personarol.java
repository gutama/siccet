
package modelo;

/**
 *
 * @author Legion
 */
public class personarol {
    private int codigopersonarol;
    private int codigorolFK;
    private String cedulaFK;

    public int getCodigopersonarol() {
        return codigopersonarol;
    }

    public void setCodigopersonarol(int codigopersonarol) {
        this.codigopersonarol = codigopersonarol;
    }

    public int getCodigorolFK() {
        return codigorolFK;
    }

    public void setCodigorolFK(int codigorolFK) {
        this.codigorolFK = codigorolFK;
    }

    public String getCedulaFK() {
        return cedulaFK;
    }

    public void setCedulaFK(String cedulaFK) {
        this.cedulaFK = cedulaFK;
    }
    
    
}
