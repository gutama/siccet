
package modelo;

/**
 *
 * @author Legion
 */
public class requsitosmatricula {
    private int codigorequisito;
    private String nombre;

    public int getCodigorequisito() {
        return codigorequisito;
    }

    public void setCodigorequisito(int codigorequisito) {
        this.codigorequisito = codigorequisito;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
