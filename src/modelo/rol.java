
package modelo;

public class rol {
 private int codigorol;
 private String rol;


    public int getCodigorol() {
        return codigorol;
    }

    public void setCodigorol(int codigorol) {
        this.codigorol = codigorol;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }
 
}
