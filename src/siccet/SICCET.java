package siccet;

import bases.incrementocodigo;
import bases.mimagenes;
import controlador.adminstradorcontroller;
import controlador.iniciarsesioncontroller;
import vista.viniciosecion;
import vista.vregistroadministrador;

/**
 *
 * @author Legion
 */
public class SICCET {

    public static void main(String[] args) {  
        mimagenes i = new mimagenes();
        incrementocodigo in = new incrementocodigo();
        String fondo = "/imagenes/fondo 3.jpg";
        int numeroregistros = Integer.parseInt(in.devolverundato("select count(cedula) from persona"));
        if (numeroregistros == 0) {
            vregistroadministrador v = new vregistroadministrador(new javax.swing.JFrame(), true);
            adminstradorcontroller controlador = new adminstradorcontroller(v);
            controlador.iniciarvista(fondo, i);
        } else {
            viniciosecion v = new viniciosecion();
            iniciarsesioncontroller controlador = new iniciarsesioncontroller(v);
            controlador.iniciarvista(fondo, i);
        }

    }

}
