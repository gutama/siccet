/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

/**
 *
 * @author Legion
 */
public class vasignar extends javax.swing.JDialog {

    /**
     * Creates new form vasignar
     */
    public vasignar(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_agregados = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl_docentes = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbl_grados = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jc_listmaterias = new javax.swing.JComboBox<>();
        jc_cargo = new javax.swing.JComboBox<>();
        btn_agregar = new javax.swing.JButton();
        btn_quitar = new javax.swing.JButton();
        btn_grabar = new javax.swing.JButton();
        fondo = new javax.swing.JLabel();
        txt_periodo = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Asignar  Grados  y Materias");
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(349, 10, 268, -1));

        tbl_agregados.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        tbl_agregados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbl_agregados.getTableHeader().setResizingAllowed(false);
        tbl_agregados.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tbl_agregados);

        jPanel2.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 307, 1130, 350));

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel2.setText("Grados");
        jPanel2.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(299, 64, -1, -1));

        tbl_docentes.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        tbl_docentes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbl_docentes.getTableHeader().setResizingAllowed(false);
        tbl_docentes.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(tbl_docentes);

        jPanel2.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 90, 400, 143));

        jLabel3.setText("Materias");
        jPanel2.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(27, 262, -1, -1));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel4.setText("Docentes");
        jPanel2.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 70, 100, -1));

        tbl_grados.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        tbl_grados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbl_grados.getTableHeader().setReorderingAllowed(false);
        jScrollPane3.setViewportView(tbl_grados);

        jPanel2.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(27, 91, 670, 148));

        jLabel5.setText("Periodo Academico");
        jPanel2.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 262, -1, -1));

        jLabel6.setText("Cargo");
        jPanel2.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(679, 262, -1, -1));

        jc_listmaterias.setBackground(new java.awt.Color(0, 255, 255));
        jPanel2.add(jc_listmaterias, new org.netbeans.lib.awtextra.AbsoluteConstraints(84, 258, 206, -1));

        jc_cargo.setBackground(new java.awt.Color(0, 255, 255));
        jc_cargo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "DOCENTE", "TUTOR" }));
        jPanel2.add(jc_cargo, new org.netbeans.lib.awtextra.AbsoluteConstraints(724, 259, 206, -1));

        btn_agregar.setBackground(new java.awt.Color(0, 0, 255));
        btn_agregar.setForeground(new java.awt.Color(255, 255, 255));
        btn_agregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/agregar.png"))); // NOI18N
        btn_agregar.setText("Agregar");
        btn_agregar.setBorder(null);
        jPanel2.add(btn_agregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 260, 91, -1));

        btn_quitar.setBackground(new java.awt.Color(0, 0, 255));
        btn_quitar.setForeground(new java.awt.Color(255, 255, 255));
        btn_quitar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/eliminar.png"))); // NOI18N
        btn_quitar.setText("Quitar");
        btn_quitar.setBorder(null);
        jPanel2.add(btn_quitar, new org.netbeans.lib.awtextra.AbsoluteConstraints(1050, 260, 90, 30));

        btn_grabar.setBackground(new java.awt.Color(0, 0, 255));
        btn_grabar.setForeground(new java.awt.Color(255, 255, 255));
        btn_grabar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/guardar.png"))); // NOI18N
        btn_grabar.setText("Guardar");
        btn_grabar.setBorder(null);
        jPanel2.add(btn_grabar, new org.netbeans.lib.awtextra.AbsoluteConstraints(1030, 670, 100, -1));
        jPanel2.add(fondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1150, 710));

        txt_periodo.setEditable(false);
        txt_periodo.setBackground(new java.awt.Color(153, 255, 255));
        jPanel2.add(txt_periodo, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 260, 170, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(vasignar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(vasignar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(vasignar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(vasignar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                vasignar dialog = new vasignar(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btn_agregar;
    public javax.swing.JButton btn_grabar;
    public javax.swing.JButton btn_quitar;
    public javax.swing.JLabel fondo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    public javax.swing.JComboBox<String> jc_cargo;
    public javax.swing.JComboBox<String> jc_listmaterias;
    public javax.swing.JTable tbl_agregados;
    public javax.swing.JTable tbl_docentes;
    public javax.swing.JTable tbl_grados;
    public javax.swing.JTextField txt_periodo;
    // End of variables declaration//GEN-END:variables
}
